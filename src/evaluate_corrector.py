import tensorflow as tf
import datetime
import argparse
import os
import pickle
from dt_corrector import CorrectorModel
import logging
import Utils
import numpy as np
import math
from collections import defaultdict
import treetaggerwrapper


# logging.basicConfig(filename='dt_Corrector_test.log', level=logging.INFO)

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

fh = logging.FileHandler('dt_Corrector_evaluate.log', mode='a')
fh.setLevel(logging.INFO)

logger.addHandler(fh)

tagger = treetaggerwrapper.TreeTagger(TAGLANG='nl')


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--data_dir', type=str, default='/home/yakke/Bureaublad/preprocessed_data/',
                        help='data directory containing input.txt')
    parser.add_argument('--model_dir', type=str, default='/home/yakke/Bureaublad/preprocessed_data/',
                        help='data directory containing important model parameters')
    # parser.add_argument('--save_dir', type=str, default='/home/yakke/Bureaublad/preprocessed_data/save/',
    #                     help='directory to store checkpointed models')
    parser.add_argument('--batch_size', type=int, default=100,
                        help='minibatch size')
    parser.add_argument('--no_max_evaluation', action='store_false', default=True,
                        help='bool deciding if maximum lengths are used for evaluation')
    parser.add_argument('--max_length_verb', type=int, default=20,
                        help='maximum length of verbs taken into account')
    parser.add_argument('--max_length_context', type=int, default=25,
                        help='maximum length of left or right part taken into account')
    parser.add_argument('--number_difficult_sentences', type=int, default=10,
                        help='minimum number of sentences with highest loss need to be printed')
    args = parser.parse_args()
    evaluate(args)


def evaluate(args):
    logger.info(" ")
    logger.info("----------START EVALUATION----------")
    logger.info(" ")
    start_preprocessing = datetime.datetime.now()
    # TODO parameters
    data_loader = Utils.EvaluatePreProcessor(args.data_dir, args.model_dir, args.batch_size, args.no_max_evaluation,
                                             args.max_length_verb, args.max_length_context)
    stop_preprocessing = datetime.datetime.now()
    print("Time preprocessing = {}".format(stop_preprocessing - start_preprocessing))
    logger.info("Time preprocessing = {}".format(stop_preprocessing - start_preprocessing))

    # check if all necessary files exist
    assert os.path.isdir(args.data_dir), " %s must be a a path" % args.data_dir
    assert os.path.isfile(
        os.path.join(args.model_dir, "config.pkl")), "config.pkl file does not exist in path %s" % args.model_dir
    assert os.path.isfile(os.path.join(args.model_dir,
                                       "vocab_words.pkl")), "vocab_words.pkl file does not exist in path %s" \
                                                            % args.model_dir
    assert os.path.isfile(os.path.join(args.model_dir,
                                       "vocab_chars.pkl")), "vocab_chars.pkl file does not exist in path %s" \
                                                            % args.model_dir
    ckpt = tf.train.get_checkpoint_state(args.model_dir)
    assert ckpt, "No checkpoint found"
    assert ckpt.model_checkpoint_path, "No model path found in checkpoint"

    learned_embedding = data_loader.get_embedding_model_repr()
    start_model_evaluation_time = datetime.datetime.now()

    with open(os.path.join(args.model_dir, 'config.pkl'), 'rb') as f:
        saved_args = pickle.load(f)

    model_args = vars(saved_args)
    # model = CorrectorModel(data_loader.word_vocab_size, data_loader.char_vocab_size, model_args['embedding_size'],
    #                        model_args['batch_size'], model_args['rnn_size'], model_args['num_layers'],
    #                        model_args['max_length_context'], model_args['max_length_verb'])
    model = CorrectorModel(data_loader.word_vocab_size, data_loader.char_vocab_size, model_args['embedding_size'],
                           args.batch_size, model_args['rnn_size'], model_args['num_layers'],
                           data_loader.max_context_sequence_length, data_loader.max_verb_sequence_length)

    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        sess.run(tf.assign(model.embedding_words, learned_embedding))
        saver = tf.train.Saver(tf.global_variables())
        ckpt = tf.train.get_checkpoint_state(args.model_dir)
        if ckpt and ckpt.model_checkpoint_path:
            saver.restore(sess, ckpt.model_checkpoint_path)

        verb_sentence_generator = Utils.read_sentences_generator_numbers(data_loader.test_verb_tensor_file)
        left_context_sentence_generator = \
            Utils.read_sentences_generator_numbers(data_loader.test_left_context_tensor_file)
        right_context_sentence_generator = \
            Utils.read_sentences_generator_numbers(data_loader.test_right_context_tensor_file)
        answers_sentence_generator = Utils.read_sentences_generator_numbers(data_loader.test_answers_tensor_file)
        line_number_sentence_generator = Utils.read_sentences_generator_number(data_loader.test_line_number_file)
        verbs_generator = Utils.read_words_generator_text(data_loader.test_verb_file)
        verb_time_sentence_generator = Utils.read_sentences_generator_number(data_loader.test_verb_times_file)

        sum_evaluation_costs = 0
        nb_most_probable_predictions = 0
        nb_correct_predictions = 0
        nb_total_cases = 0
        nb_correct_change_predictions = 0
        nb_false_change_predictions = 0

        nb_cases_past_dde = 0
        nb_most_probable_predictions_past_dde = 0
        nb_correct_predictions_past_dde = 0
        nb_correct_change_predictions_past_dde = 0
        nb_false_change_predictions_past_dde = 0

        nb_cases_past_de = 0
        nb_most_probable_predictions_past_de = 0
        nb_correct_predictions_past_de = 0
        nb_correct_change_predictions_past_de = 0
        nb_false_change_predictions_past_de = 0

        nb_cases_past_dden = 0
        nb_most_probable_predictions_past_dden = 0
        nb_correct_predictions_past_dden = 0
        nb_correct_change_predictions_past_dden = 0
        nb_false_change_predictions_past_dden = 0

        nb_cases_past_den = 0
        nb_most_probable_predictions_past_den = 0
        nb_correct_predictions_past_den = 0
        nb_correct_change_predictions_past_den = 0
        nb_false_change_predictions_past_den = 0

        nb_cases_past_tte = 0
        nb_most_probable_predictions_past_tte = 0
        nb_correct_predictions_past_tte = 0
        nb_correct_change_predictions_past_tte = 0
        nb_false_change_predictions_past_tte = 0

        nb_cases_past_te = 0
        nb_most_probable_predictions_past_te = 0
        nb_correct_predictions_past_te = 0
        nb_correct_change_predictions_past_te = 0
        nb_false_change_predictions_past_te = 0

        nb_cases_past_tten = 0
        nb_most_probable_predictions_past_tten = 0
        nb_correct_predictions_past_tten = 0
        nb_correct_change_predictions_past_tten = 0
        nb_false_change_predictions_past_tten = 0

        nb_cases_past_ten = 0
        nb_most_probable_predictions_past_ten = 0
        nb_correct_predictions_past_ten = 0
        nb_correct_change_predictions_past_ten = 0
        nb_false_change_predictions_past_ten = 0

        nb_cases_present_d = 0
        nb_most_probable_predictions_present_d = 0
        nb_correct_predictions_present_d = 0
        nb_correct_change_predictions_present_d = 0
        nb_false_change_predictions_present_d = 0

        nb_cases_present_dt = 0
        nb_most_probable_predictions_present_dt = 0
        nb_correct_predictions_present_dt = 0
        nb_correct_change_predictions_present_dt = 0
        nb_false_change_predictions_present_dt = 0

        nb_cases_present_t = 0
        nb_most_probable_predictions_present_t = 0
        nb_correct_predictions_present_t = 0
        nb_correct_change_predictions_present_t = 0
        nb_false_change_predictions_present_t = 0

        nb_cases_past_participle_d = 0
        nb_most_probable_predictions_past_participle_d = 0
        nb_correct_predictions_past_participle_d = 0
        nb_correct_change_predictions_past_participle_d = 0
        nb_false_change_predictions_past_participle_d = 0

        nb_cases_past_participle_t = 0
        nb_most_probable_predictions_past_participle_t = 0
        nb_correct_predictions_past_participle_t = 0
        nb_correct_change_predictions_past_participle_t = 0
        nb_false_change_predictions_past_participle_t = 0

        # weird_sentence_numbers = []
        # weird_sentence_line_number_rule_output = defaultdict(list)


        highest_losses = defaultdict(list)
        highest_losses[-1].append(-1)
        # highest_losses = {-1: [-1]}
        number_highest_losses = 1
        pair_sentence_line_number_rule_line_number = defaultdict(list)
        pair_sentence_line_number_rule_line_number[-1].append(-1)
        # pair_sentence_number_rule_number = {-1: -1}
        pair_sentence_line_number_rule_output = defaultdict(list)
        pair_sentence_line_number_rule_output[-1].append(-1)
        start_testing = datetime.datetime.now()
        for b in range(data_loader.num_batches_test):
            text_verbs_numeric, text_verb_seq_lens, \
                text_left_context_data, text_left_context_seq_lengths, \
                text_right_context_data, text_right_context_seq_lengths, \
                text_targets, test_line_numbers, test_verbs, test_verb_times = \
                data_loader.generate_batch(verb_sentence_generator, left_context_sentence_generator,
                                           right_context_sentence_generator, answers_sentence_generator,
                                           line_number_sentence_generator, verbs_generator, 
                                           verb_time_sentence_generator)

            feed = {model.input_data_verb: text_verbs_numeric,
                    model.seq_length_verb: text_verb_seq_lens,
                    model.input_data_left_context: text_left_context_data,
                    model.seq_length_left_context: text_left_context_seq_lengths,
                    model.input_data_right_context: text_right_context_data,
                    model.seq_length_right_context: text_right_context_seq_lengths,
                    model.target: text_targets}

            # print(text_verbs_numeric)
            # print(text_verb_seq_lens)
            # print(text_left_context_data)
            # print(text_left_context_seq_lengths)
            # print(text_right_context_data)
            # print(text_right_context_seq_lengths)
            # print(text_targets)

            evaluate_cost, evaluation_predictions, evaluate_losses = \
                sess.run([model.cost, model.prediction, model.loss], feed)
            sum_evaluation_costs += evaluate_cost
            predicted_rules = np.argmax(evaluation_predictions, axis=1)
            true_rules = np.argmax(text_targets, axis=1)
            # print(len(predicted_rules))
            # print(len(text_targets))
            # print(test_line_numbers)
            # print(test_verbs)

            for i in range(len(evaluate_losses)):
                evaluate_loss = evaluate_losses[i]
                sentence_number_original_file = test_line_numbers[i]
                
                # print(sentence_number_original_file)
                predicted_rule = predicted_rules[i]
                true_rule = true_rules[i]
                # print(highest_losses)
                least_worst_sentence_loss = math.inf
                for l in highest_losses:
                    # print(l)
                    temp_min = min(highest_losses[l])
                    if least_worst_sentence_loss > temp_min:
                        least_worst_sentence_loss = temp_min
                        least_worst_sentence = l
                # least_worst_sentence_losses = (highest_losses.get(least_worst_sentence))
                # least_worst_sentence_loss = min(least_worst_sentence_losses)
                least_worst_loss_index = highest_losses[least_worst_sentence].index(least_worst_sentence_loss)
                if evaluate_loss > least_worst_sentence_loss:
                    # Check if their are already the max number of highest loss examples
                    if number_highest_losses == args.number_difficult_sentences:
                        del highest_losses[least_worst_sentence][least_worst_loss_index]
                        del pair_sentence_line_number_rule_line_number[least_worst_sentence][least_worst_loss_index]
                        del pair_sentence_line_number_rule_output[least_worst_sentence][least_worst_loss_index]
                        # print(highest_losses)
                        if len(highest_losses[least_worst_sentence]) == 0:
                            # print('Check')
                            del highest_losses[least_worst_sentence]
                            del pair_sentence_line_number_rule_line_number[least_worst_sentence]
                            del pair_sentence_line_number_rule_output[least_worst_sentence]
                        number_highest_losses -= 1

                    # Add new example to list
                    highest_losses[sentence_number_original_file].append(evaluate_loss)
                    number_highest_losses += 1
                    pair_sentence_line_number_rule_line_number[sentence_number_original_file].append(b * data_loader.batch_size + i)
                    pair_sentence_line_number_rule_output[sentence_number_original_file].append([predicted_rule, true_rule])
                elif evaluate_loss == least_worst_sentence_loss:
                    if number_highest_losses < args.number_difficult_sentences:
                        highest_losses[sentence_number_original_file].append(evaluate_loss)
                        number_highest_losses += 1
                        pair_sentence_line_number_rule_line_number[sentence_number_original_file].append(b * data_loader.batch_size + i)
                        pair_sentence_line_number_rule_output[sentence_number_original_file].append(
                            [predicted_rule, true_rule])

            # for prediction, correct_rule in zip(predicted_rules, true_rules):
            #     nb_total_cases += 1
            #     if correct_rule == Utils.AdaptRule.DO_NOTHING.value:
            #         nb_most_probable_predictions += 1
            #     if prediction == correct_rule:
            #         nb_correct_predictions += 1
            #     if prediction == correct_rule and not correct_rule == Utils.AdaptRule.DO_NOTHING.value:
            #         nb_correct_change_predictions += 1
            #     if not prediction == correct_rule and not prediction == Utils.AdaptRule.DO_NOTHING.value:
            #         nb_false_change_predictions += 1

            for prediction, correct_rule, original_verb, verb_time, i in \
                    zip(predicted_rules, true_rules, test_verbs, test_verb_times, range(len(predicted_rules))):
                nb_total_cases += 1
                if correct_rule == Utils.AdaptRule.DO_NOTHING.value:
                    nb_most_probable_predictions += 1
                if prediction == correct_rule:
                    nb_correct_predictions += 1
                if prediction == correct_rule and not correct_rule == Utils.AdaptRule.DO_NOTHING.value:
                    nb_correct_change_predictions += 1
                if not prediction == correct_rule and not prediction == Utils.AdaptRule.DO_NOTHING.value:
                    nb_false_change_predictions += 1
                
                if verb_time == Utils.VerbTime.PAST.value:
                    # print(original_verb)
                    if original_verb.endswith('dde'):
                        nb_cases_past_dde += 1
                        if correct_rule == Utils.AdaptRule.DO_NOTHING.value:
                            nb_most_probable_predictions_past_dde += 1
                        if prediction == correct_rule:
                            nb_correct_predictions_past_dde += 1
                        if prediction == correct_rule and not correct_rule == Utils.AdaptRule.DO_NOTHING.value:
                            nb_correct_change_predictions_past_dde += 1
                        if not prediction == correct_rule and not prediction == Utils.AdaptRule.DO_NOTHING.value:
                            nb_false_change_predictions_past_dde += 1
                        # if not correct_rule == Utils.AdaptRule.DO_NOTHING.value:
                        #     sentence_nb_original_file = test_line_numbers[i]
                        #     weird_sentence_numbers.append(sentence_nb_original_file)
                        #     weird_sentence_line_number_rule_output[sentence_nb_original_file].append(
                        #         [prediction, correct_rule])
                        #     print(original_verb)
                    elif original_verb.endswith('de'):
                        nb_cases_past_de += 1
                        if correct_rule == Utils.AdaptRule.DO_NOTHING.value:
                            nb_most_probable_predictions_past_de += 1
                        if prediction == correct_rule:
                            nb_correct_predictions_past_de += 1
                        if prediction == correct_rule and not correct_rule == Utils.AdaptRule.DO_NOTHING.value:
                            nb_correct_change_predictions_past_de += 1
                        if not prediction == correct_rule and not prediction == Utils.AdaptRule.DO_NOTHING.value:
                            nb_false_change_predictions_past_de += 1
                    elif original_verb.endswith('dden'):
                        nb_cases_past_dden += 1
                        if correct_rule == Utils.AdaptRule.DO_NOTHING.value:
                            nb_most_probable_predictions_past_dden += 1
                        if prediction == correct_rule:
                            nb_correct_predictions_past_dden += 1
                        if prediction == correct_rule and not correct_rule == Utils.AdaptRule.DO_NOTHING.value:
                            nb_correct_change_predictions_past_dden += 1
                        if not prediction == correct_rule and not prediction == Utils.AdaptRule.DO_NOTHING.value:
                            nb_false_change_predictions_past_dden += 1
                    elif original_verb.endswith('den'):
                        nb_cases_past_den += 1
                        if correct_rule == Utils.AdaptRule.DO_NOTHING.value:
                            nb_most_probable_predictions_past_den += 1
                        if prediction == correct_rule:
                            nb_correct_predictions_past_den += 1
                        if prediction == correct_rule and not correct_rule == Utils.AdaptRule.DO_NOTHING.value:
                            nb_correct_change_predictions_past_den += 1
                        if not prediction == correct_rule and not prediction == Utils.AdaptRule.DO_NOTHING.value:
                            nb_false_change_predictions_past_den += 1
                    elif original_verb.endswith('tte'):
                        nb_cases_past_tte += 1
                        if correct_rule == Utils.AdaptRule.DO_NOTHING.value:
                            nb_most_probable_predictions_past_tte += 1
                        if prediction == correct_rule:
                            nb_correct_predictions_past_tte += 1
                        if prediction == correct_rule and not correct_rule == Utils.AdaptRule.DO_NOTHING.value:
                            nb_correct_change_predictions_past_tte += 1
                        if not prediction == correct_rule and not prediction == Utils.AdaptRule.DO_NOTHING.value:
                            nb_false_change_predictions_past_tte += 1
                    elif original_verb.endswith('te'):
                        nb_cases_past_te += 1
                        if correct_rule == Utils.AdaptRule.DO_NOTHING.value:
                            nb_most_probable_predictions_past_te += 1
                        if prediction == correct_rule:
                            nb_correct_predictions_past_te += 1
                        if prediction == correct_rule and not correct_rule == Utils.AdaptRule.DO_NOTHING.value:
                            nb_correct_change_predictions_past_te += 1
                        if not prediction == correct_rule and not prediction == Utils.AdaptRule.DO_NOTHING.value:
                            nb_false_change_predictions_past_te += 1
                    elif original_verb.endswith('tten'):
                        nb_cases_past_tten += 1
                        if correct_rule == Utils.AdaptRule.DO_NOTHING.value:
                            nb_most_probable_predictions_past_tten += 1
                        if prediction == correct_rule:
                            nb_correct_predictions_past_tten += 1
                        if prediction == correct_rule and not correct_rule == Utils.AdaptRule.DO_NOTHING.value:
                            nb_correct_change_predictions_past_tten += 1
                        if not prediction == correct_rule and not prediction == Utils.AdaptRule.DO_NOTHING.value:
                            nb_false_change_predictions_past_tten += 1
                    elif original_verb.endswith('ten'):
                        nb_cases_past_ten += 1
                        if correct_rule == Utils.AdaptRule.DO_NOTHING.value:
                            nb_most_probable_predictions_past_ten += 1
                        if prediction == correct_rule:
                            nb_correct_predictions_past_ten += 1
                        if prediction == correct_rule and not correct_rule == Utils.AdaptRule.DO_NOTHING.value:
                            nb_correct_change_predictions_past_ten += 1
                        if not prediction == correct_rule and not prediction == Utils.AdaptRule.DO_NOTHING.value:
                            nb_false_change_predictions_past_ten += 1
                    
                elif verb_time == Utils.VerbTime.PRESENT.value:
                    if original_verb.endswith('d'):
                        nb_cases_present_d += 1
                        if correct_rule == Utils.AdaptRule.DO_NOTHING.value:
                            nb_most_probable_predictions_present_d += 1
                        if prediction == correct_rule:
                            nb_correct_predictions_present_d += 1
                        if prediction == correct_rule and not correct_rule == Utils.AdaptRule.DO_NOTHING.value:
                            nb_correct_change_predictions_present_d += 1
                        if not prediction == correct_rule and not prediction == Utils.AdaptRule.DO_NOTHING.value:
                            nb_false_change_predictions_present_d += 1
                    elif original_verb.endswith('dt'):
                        nb_cases_present_dt += 1
                        if correct_rule == Utils.AdaptRule.DO_NOTHING.value:
                            nb_most_probable_predictions_present_dt += 1
                        if prediction == correct_rule:
                            nb_correct_predictions_present_dt += 1
                        if prediction == correct_rule and not correct_rule == Utils.AdaptRule.DO_NOTHING.value:
                            nb_correct_change_predictions_present_dt += 1
                        if not prediction == correct_rule and not prediction == Utils.AdaptRule.DO_NOTHING.value:
                            nb_false_change_predictions_present_dt += 1
                    elif original_verb.endswith('t') and not original_verb.endswith('dt'):
                        nb_cases_present_t += 1
                        if correct_rule == Utils.AdaptRule.DO_NOTHING.value:
                            nb_most_probable_predictions_present_t += 1
                        if prediction == correct_rule:
                            nb_correct_predictions_present_t += 1
                        if prediction == correct_rule and not correct_rule == Utils.AdaptRule.DO_NOTHING.value:
                            nb_correct_change_predictions_present_t += 1
                        if not prediction == correct_rule and not prediction == Utils.AdaptRule.DO_NOTHING.value:
                            nb_false_change_predictions_present_t += 1
                        # if not correct_rule == Utils.AdaptRule.DO_NOTHING.value:
                        #     sentence_nb_original_file = test_line_numbers[i]
                        #     weird_sentence_numbers.append(sentence_nb_original_file)
                        #     weird_sentence_line_number_rule_output[sentence_nb_original_file].append(
                        #         [prediction, correct_rule])
                        #     print(original_verb)
                
                elif verb_time == Utils.VerbTime.PAST_PARTICIPLE.value:
                    if original_verb.endswith('d'):
                        nb_cases_past_participle_d += 1
                        if correct_rule == Utils.AdaptRule.DO_NOTHING.value:
                            nb_most_probable_predictions_past_participle_d += 1
                        if prediction == correct_rule:
                            nb_correct_predictions_past_participle_d += 1
                        if prediction == correct_rule and not correct_rule == Utils.AdaptRule.DO_NOTHING.value:
                            nb_correct_change_predictions_past_participle_d += 1
                        if not prediction == correct_rule and not prediction == Utils.AdaptRule.DO_NOTHING.value:
                            nb_false_change_predictions_past_participle_d += 1
                        # if not correct_rule == Utils.AdaptRule.DO_NOTHING.value:
                        #     sentence_nb_original_file = test_line_numbers[i]
                        #     weird_sentence_numbers.append(sentence_nb_original_file)
                    elif original_verb.endswith('t') and not original_verb.endswith('dt'):
                        nb_cases_past_participle_t += 1
                        if correct_rule == Utils.AdaptRule.DO_NOTHING.value:
                            nb_most_probable_predictions_past_participle_t += 1
                        if prediction == correct_rule:
                            nb_correct_predictions_past_participle_t += 1
                        if prediction == correct_rule and not correct_rule == Utils.AdaptRule.DO_NOTHING.value:
                            nb_correct_change_predictions_past_participle_t += 1
                        if not prediction == correct_rule and not prediction == Utils.AdaptRule.DO_NOTHING.value:
                            nb_false_change_predictions_past_participle_t += 1
                        # if not correct_rule == Utils.AdaptRule.DO_NOTHING.value:
                            # sentence_nb_original_file = test_line_numbers[i]
                            # weird_sentence_numbers.append(sentence_nb_original_file)
                            # weird_sentence_line_number_rule_output[sentence_nb_original_file].append(
                            #     [prediction, correct_rule])
                            # print(original_verb)
                # print(original_verb)

        print("Testing: " + args.data_dir)
        print("Training: " + args.model_dir)
        logger.info("Testing: " + args.data_dir)
        logger.info("Training: " + args.model_dir)

        print(highest_losses)
        logger.info(highest_losses)
        with open(data_loader.test_original_file) as original_f, open(data_loader.test_file) as edited_f, \
                open(data_loader.test_answers_tensor_file) as out_f:
            # print('OK')
            output_lines = out_f.readlines()
            # print('OK')
            for (i, line,), (_, line2) in zip(enumerate(original_f), enumerate(edited_f)):
                # print(i)
                # print('OK')
                if i in highest_losses:
                    print(i)
                    logger.info(i)
                    print(line + line2 + str(highest_losses[i]))
                    logger.info(line + line2 + str(highest_losses[i]))
                    for j in pair_sentence_line_number_rule_line_number[i]:
                        print(output_lines[j].strip('\n'))
                        logger.info(output_lines[j].strip('\n'))
                    for j in pair_sentence_line_number_rule_output[i]:
                        print("Prediction: " + str(j[0]) + "\nCorrect rule: " + str(j[1]))
                        logger.info("Prediction: " + str(j[0]) + "\nCorrect rule: " + str(j[1]))
                    print(" ")
                    print(" ")
                    logger.info(" ")
                    logger.info(" ")
                # if i in weird_sentence_numbers:
                #     print("Bad1")
                #     print(line + line2)
                #     for j in weird_sentence_line_number_rule_output[i]:
                #         print("Prediction: " + str(j[0]) + "\nCorrect rule: " + str(j[1]))
                #     print(tag_input_sentence(line))
                #     print(output_lines[i].strip('\n'))
                #     print("Bad2")

        average_text_loss = sum_evaluation_costs / data_loader.num_batches_test
        stop_testing = datetime.datetime.now()
        print("Average test_loss = {:.5f}".format(average_text_loss))
        logger.info("Average test_loss = {:.5f}".format(average_text_loss))

        print(" ")
        print("Evaluation past dde")
        logger.info(" ")
        logger.info("Evaluation past dde")
        if nb_cases_past_dde == 0:
            print("No such cases")
            logger.info("No such cases")
        else:
            print("Total cases past dde = {}".format(nb_cases_past_dde))
            logger.info("Total cases past dde = {}".format(nb_cases_past_dde))
            print("Number most probable predictions past dde = {}".format(nb_most_probable_predictions_past_dde))
            logger.info("Number most probable predictions past dde = {}".format(nb_most_probable_predictions_past_dde))
            print("Number correct predictions past dde = {}".format(nb_correct_predictions_past_dde))
            logger.info("Number correct predictions past dde = {}".format(nb_correct_predictions_past_dde))
            print("Number correct change predictions past dde = {}".format(nb_correct_change_predictions_past_dde))
            logger.info("Number correct change predictions past dde = {}"
                        .format(nb_correct_change_predictions_past_dde))
            print("Number correct no change predictions past dde = {}"
                  .format(nb_correct_predictions_past_dde - nb_correct_change_predictions_past_dde))
            logger.info("Number correct no change predictions past dde = {}"
                        .format(nb_correct_predictions_past_dde - nb_correct_change_predictions_past_dde))
            print("Number false predictions past dde = {}".format(nb_cases_past_dde - nb_correct_predictions_past_dde))
            logger.info("Number false predictions past dde = {}"
                        .format(nb_cases_past_dde - nb_correct_predictions_past_dde))
            print("Number false change predictions past dde = {}".format(nb_false_change_predictions_past_dde))
            logger.info("Number false change predictions past dde = {}".format(nb_false_change_predictions_past_dde))
            print("Number false no change predictions past dde = {}"
                  .format(nb_cases_past_dde - nb_correct_predictions_past_dde - nb_false_change_predictions_past_dde))
            logger.info("Number false no change predictions past dde = {}".format(
                nb_cases_past_dde - nb_correct_predictions_past_dde - nb_false_change_predictions_past_dde))
            accuracy_majority_past_dde = nb_most_probable_predictions_past_dde / nb_cases_past_dde
            print("Accuracy majority class past dde = {:.5f}".format(accuracy_majority_past_dde))
            logger.info("Accuracy majority class past dde = {:.5f}".format(accuracy_majority_past_dde))
            accuracy_model_past_dde = nb_correct_predictions_past_dde / nb_cases_past_dde
            print("Accuracy trained model past dde = {:.5f}".format(accuracy_model_past_dde))
            logger.info("Accuracy trained model past dde = {:.5f}".format(accuracy_model_past_dde))

        print(" ")
        print("Evaluation past de")
        logger.info(" ")
        logger.info("Evaluation past de")
        if nb_cases_past_de == 0:
            print("No such cases")
            logger.info("No such cases")
        else:
            print("Total cases past de = {}".format(nb_cases_past_de))
            logger.info("Total cases past de = {}".format(nb_cases_past_de))
            print("Number most probable predictions past de = {}".format(nb_most_probable_predictions_past_de))
            logger.info("Number most probable predictions past de = {}".format(nb_most_probable_predictions_past_de))
            print("Number correct predictions past de = {}".format(nb_correct_predictions_past_de))
            logger.info("Number correct predictions past de = {}".format(nb_correct_predictions_past_de))
            print("Number correct change predictions past de = {}".format(nb_correct_change_predictions_past_de))
            logger.info("Number correct change predictions past de = {}".format(nb_correct_change_predictions_past_de))
            print("Number correct no change predictions past de = {}"
                  .format(nb_correct_predictions_past_de - nb_correct_change_predictions_past_de))
            logger.info("Number correct no change predictions past de = {}"
                        .format(nb_correct_predictions_past_de - nb_correct_change_predictions_past_de))
            print("Number false predictions past de = {}".format(nb_cases_past_de - nb_correct_predictions_past_de))
            logger.info("Number false predictions past de = {}"
                        .format(nb_cases_past_de - nb_correct_predictions_past_de))
            print("Number false change predictions past de = {}".format(nb_false_change_predictions_past_de))
            logger.info("Number false change predictions past de = {}".format(nb_false_change_predictions_past_de))
            print("Number false no change predictions past de = {}"
                  .format(nb_cases_past_de - nb_correct_predictions_past_de - nb_false_change_predictions_past_de))
            logger.info("Number false no change predictions past de = {}".format(
                nb_cases_past_de - nb_correct_predictions_past_de - nb_false_change_predictions_past_de))
            accuracy_majority_past_de = nb_most_probable_predictions_past_de / nb_cases_past_de
            print("Accuracy majority class past de = {:.5f}".format(accuracy_majority_past_de))
            logger.info("Accuracy majority class past de = {:.5f}".format(accuracy_majority_past_de))
            accuracy_model_past_de = nb_correct_predictions_past_de / nb_cases_past_de
            print("Accuracy trained model past de = {:.5f}".format(accuracy_model_past_de))
            logger.info("Accuracy trained model past de = {:.5f}".format(accuracy_model_past_de))

        print(" ")
        print("Evaluation past dden")
        logger.info(" ")
        logger.info("Evaluation past dden")
        if nb_cases_past_dden == 0:
            print("No such cases")
            logger.info("No such cases")
        else:
            print("Total cases past dden = {}".format(nb_cases_past_dden))
            logger.info("Total cases past dden = {}".format(nb_cases_past_dden))
            print("Number most probable predictions past dden = {}".format(nb_most_probable_predictions_past_dden))
            logger.info("Number most probable predictions past dden = {}".format(nb_most_probable_predictions_past_dden))
            print("Number correct predictions past dden = {}".format(nb_correct_predictions_past_dden))
            logger.info("Number correct predictions past dden = {}".format(nb_correct_predictions_past_dden))
            print("Number correct change predictions past dden = {}".format(nb_correct_change_predictions_past_dden))
            logger.info("Number correct change predictions past dden = {}"
                        .format(nb_correct_change_predictions_past_dden))
            print("Number correct no change predictions past dden = {}"
                  .format(nb_correct_predictions_past_dden - nb_correct_change_predictions_past_dden))
            logger.info(
                "Number correct no change predictions past dden = {}".format(
                    nb_correct_predictions_past_dden - nb_correct_change_predictions_past_dden))
            print("Number false predictions past dden = {}".format(nb_cases_past_dden - nb_correct_predictions_past_dden))
            logger.info(
                "Number false predictions past dden = {}".format(nb_cases_past_dden - nb_correct_predictions_past_dden))
            print("Number false change predictions past dden = {}".format(nb_false_change_predictions_past_dden))
            logger.info("Number false change predictions past dden = {}".format(nb_false_change_predictions_past_dden))
            print("Number false no change predictions past dden = {}".format(
                nb_cases_past_dden - nb_correct_predictions_past_dden - nb_false_change_predictions_past_dden))
            logger.info("Number false no change predictions past dden = {}".format(
                nb_cases_past_dden - nb_correct_predictions_past_dden - nb_false_change_predictions_past_dden))
            accuracy_majority_past_dden = nb_most_probable_predictions_past_dden / nb_cases_past_dden
            print("Accuracy majority class past dden = {:.5f}".format(accuracy_majority_past_dden))
            logger.info("Accuracy majority class past dden = {:.5f}".format(accuracy_majority_past_dden))
            accuracy_model_past_dden = nb_correct_predictions_past_dden / nb_cases_past_dden
            print("Accuracy trained model past dden = {:.5f}".format(accuracy_model_past_dden))
            logger.info("Accuracy trained model past dden = {:.5f}".format(accuracy_model_past_dden))

        print(" ")
        print("Evaluation past den")
        logger.info(" ")
        logger.info("Evaluation past den")
        if nb_cases_past_den == 0:
            print("No such cases")
            logger.info("No such cases")
        else:
            print("Total cases past den = {}".format(nb_cases_past_den))
            logger.info("Total cases past den = {}".format(nb_cases_past_den))
            print("Number most probable predictions past den = {}".format(nb_most_probable_predictions_past_den))
            logger.info("Number most probable predictions past den = {}".format(nb_most_probable_predictions_past_den))
            print("Number correct predictions past den = {}".format(nb_correct_predictions_past_den))
            logger.info("Number correct predictions past den = {}".format(nb_correct_predictions_past_den))
            print("Number correct change predictions past den = {}".format(nb_correct_change_predictions_past_den))
            logger.info("Number correct change predictions past den = {}".format(nb_correct_change_predictions_past_den))
            print(
                "Number correct no change predictions past den = {}".format(
                    nb_correct_predictions_past_den - nb_correct_change_predictions_past_den))
            logger.info(
                "Number correct no change predictions past den = {}".format(
                    nb_correct_predictions_past_den - nb_correct_change_predictions_past_den))
            print("Number false predictions past den = {}".format(nb_cases_past_den - nb_correct_predictions_past_den))
            logger.info(
                "Number false predictions past den = {}".format(nb_cases_past_den - nb_correct_predictions_past_den))
            print("Number false change predictions past den = {}".format(nb_false_change_predictions_past_den))
            logger.info("Number false change predictions past den = {}".format(nb_false_change_predictions_past_den))
            print("Number false no change predictions past den = {}".format(
                nb_cases_past_den - nb_correct_predictions_past_den - nb_false_change_predictions_past_den))
            logger.info("Number false no change predictions past den = {}".format(
                nb_cases_past_den - nb_correct_predictions_past_den - nb_false_change_predictions_past_den))
            accuracy_majority_past_den = nb_most_probable_predictions_past_den / nb_cases_past_den
            print("Accuracy majority class past den = {:.5f}".format(accuracy_majority_past_den))
            logger.info("Accuracy majority class past den = {:.5f}".format(accuracy_majority_past_den))
            accuracy_model_past_den = nb_correct_predictions_past_den / nb_cases_past_den
            print("Accuracy trained model past den = {:.5f}".format(accuracy_model_past_den))
            logger.info("Accuracy trained model past den = {:.5f}".format(accuracy_model_past_den))

        print(" ")
        print("Evaluation past tte")
        logger.info(" ")
        logger.info("Evaluation past tte")
        if nb_cases_past_tte == 0:
            print("No such cases")
            logger.info("No such cases")
        else:
            print("Total cases past tte = {}".format(nb_cases_past_tte))
            logger.info("Total cases past tte = {}".format(nb_cases_past_tte))
            print("Number most probable predictions past tte = {}".format(nb_most_probable_predictions_past_tte))
            logger.info("Number most probable predictions past tte = {}".format(nb_most_probable_predictions_past_tte))
            print("Number correct predictions past tte = {}".format(nb_correct_predictions_past_tte))
            logger.info("Number correct predictions past tte = {}".format(nb_correct_predictions_past_tte))
            print("Number correct change predictions past tte = {}".format(nb_correct_change_predictions_past_tte))
            logger.info("Number correct change predictions past tte = {}".format(nb_correct_change_predictions_past_tte))
            print(
                "Number correct no change predictions past tte = {}".format(
                    nb_correct_predictions_past_tte - nb_correct_change_predictions_past_tte))
            logger.info(
                "Number correct no change predictions past tte = {}".format(
                    nb_correct_predictions_past_tte - nb_correct_change_predictions_past_tte))
            print("Number false predictions past tte = {}".format(nb_cases_past_tte - nb_correct_predictions_past_tte))
            logger.info(
                "Number false predictions past tte = {}".format(nb_cases_past_tte - nb_correct_predictions_past_tte))
            print("Number false change predictions past tte = {}".format(nb_false_change_predictions_past_tte))
            logger.info("Number false change predictions past tte = {}".format(nb_false_change_predictions_past_tte))
            print("Number false no change predictions past tte = {}".format(
                nb_cases_past_tte - nb_correct_predictions_past_tte - nb_false_change_predictions_past_tte))
            logger.info("Number false no change predictions past tte = {}".format(
                nb_cases_past_tte - nb_correct_predictions_past_tte - nb_false_change_predictions_past_tte))
            accuracy_majority_past_tte = nb_most_probable_predictions_past_tte / nb_cases_past_tte
            print("Accuracy majority class past tte = {:.5f}".format(accuracy_majority_past_tte))
            logger.info("Accuracy majority class past tte = {:.5f}".format(accuracy_majority_past_tte))
            accuracy_model_past_tte = nb_correct_predictions_past_tte / nb_cases_past_tte
            print("Accuracy trained model past tte = {:.5f}".format(accuracy_model_past_tte))
            logger.info("Accuracy trained model past tte = {:.5f}".format(accuracy_model_past_tte))

        print(" ")
        print("Evaluation past te")
        logger.info(" ")
        logger.info("Evaluation past te")
        if nb_cases_past_te == 0:
            print("No such cases")
            logger.info("No such cases")
        else:
            print("Total cases past te = {}".format(nb_cases_past_te))
            logger.info("Total cases past te = {}".format(nb_cases_past_te))
            print("Number most probable predictions past te = {}".format(nb_most_probable_predictions_past_te))
            logger.info("Number most probable predictions past te = {}".format(nb_most_probable_predictions_past_te))
            print("Number correct predictions past te = {}".format(nb_correct_predictions_past_te))
            logger.info("Number correct predictions past te = {}".format(nb_correct_predictions_past_te))
            print("Number correct change predictions past te = {}".format(nb_correct_change_predictions_past_te))
            logger.info("Number correct change predictions past te = {}".format(nb_correct_change_predictions_past_te))
            print(
                "Number correct no change predictions past te = {}".format(
                    nb_correct_predictions_past_te - nb_correct_change_predictions_past_te))
            logger.info(
                "Number correct no change predictions past te = {}".format(
                    nb_correct_predictions_past_te - nb_correct_change_predictions_past_te))
            print("Number false predictions past te = {}".format(nb_cases_past_te - nb_correct_predictions_past_te))
            logger.info(
                "Number false predictions past te = {}".format(nb_cases_past_te - nb_correct_predictions_past_te))
            print("Number false change predictions past te = {}".format(nb_false_change_predictions_past_te))
            logger.info("Number false change predictions past te = {}".format(nb_false_change_predictions_past_te))
            print("Number false no change predictions past te = {}".format(
                nb_cases_past_te - nb_correct_predictions_past_te - nb_false_change_predictions_past_te))
            logger.info("Number false no change predictions past te = {}".format(
                nb_cases_past_te - nb_correct_predictions_past_te - nb_false_change_predictions_past_te))
            accuracy_majority_past_te = nb_most_probable_predictions_past_te / nb_cases_past_te
            print("Accuracy majority class past te = {:.5f}".format(accuracy_majority_past_te))
            logger.info("Accuracy majority class past te = {:.5f}".format(accuracy_majority_past_te))
            accuracy_model_past_te = nb_correct_predictions_past_te / nb_cases_past_te
            print("Accuracy trained model past te = {:.5f}".format(accuracy_model_past_te))
            logger.info("Accuracy trained model past te = {:.5f}".format(accuracy_model_past_te))

        print(" ")
        print("Evaluation past tten")
        logger.info(" ")
        logger.info("Evaluation past tten")
        if nb_cases_past_tten == 0:
            print("No such cases")
            logger.info("No such cases")
        else:
            print("Total cases past tten = {}".format(nb_cases_past_tten))
            logger.info("Total cases past tten = {}".format(nb_cases_past_tten))
            print("Number most probable predictions past tten = {}".format(nb_most_probable_predictions_past_tten))
            logger.info("Number most probable predictions past tten = {}".format(nb_most_probable_predictions_past_tten))
            print("Number correct predictions past tten = {}".format(nb_correct_predictions_past_tten))
            logger.info("Number correct predictions past tten = {}".format(nb_correct_predictions_past_tten))
            print("Number correct change predictions past tten = {}".format(nb_correct_change_predictions_past_tten))
            logger.info("Number correct change predictions past tten = {}".format(nb_correct_change_predictions_past_tten))
            print(
                "Number correct no change predictions past tten = {}".format(
                    nb_correct_predictions_past_tten - nb_correct_change_predictions_past_tten))
            logger.info(
                "Number correct no change predictions past tten = {}".format(
                    nb_correct_predictions_past_tten - nb_correct_change_predictions_past_tten))
            print("Number false predictions past tten = {}".format(nb_cases_past_tten - nb_correct_predictions_past_tten))
            logger.info(
                "Number false predictions past tten = {}".format(nb_cases_past_tten - nb_correct_predictions_past_tten))
            print("Number false change predictions past tten = {}".format(nb_false_change_predictions_past_tten))
            logger.info("Number false change predictions past tten = {}".format(nb_false_change_predictions_past_tten))
            print("Number false no change predictions past tten = {}".format(
                nb_cases_past_tten - nb_correct_predictions_past_tten - nb_false_change_predictions_past_tten))
            logger.info("Number false no change predictions past tten = {}".format(
                nb_cases_past_tten - nb_correct_predictions_past_tten - nb_false_change_predictions_past_tten))
            accuracy_majority_past_tten = nb_most_probable_predictions_past_tten / nb_cases_past_tten
            print("Accuracy majority class past tten = {:.5f}".format(accuracy_majority_past_tten))
            logger.info("Accuracy majority class past tten = {:.5f}".format(accuracy_majority_past_tten))
            accuracy_model_past_tten = nb_correct_predictions_past_tten / nb_cases_past_tten
            print("Accuracy trained model past tten = {:.5f}".format(accuracy_model_past_tten))
            logger.info("Accuracy trained model past tten = {:.5f}".format(accuracy_model_past_tten))

        print(" ")
        print("Evaluation past ten")
        logger.info(" ")
        logger.info("Evaluation past ten")
        if nb_cases_past_ten == 0:
            print("No such cases")
            logger.info("No such cases")
        else:
            print("Total cases past ten = {}".format(nb_cases_past_ten))
            logger.info("Total cases past ten = {}".format(nb_cases_past_ten))
            print("Number most probable predictions past ten = {}".format(nb_most_probable_predictions_past_ten))
            logger.info("Number most probable predictions past ten = {}".format(nb_most_probable_predictions_past_ten))
            print("Number correct predictions past ten = {}".format(nb_correct_predictions_past_ten))
            logger.info("Number correct predictions past ten = {}".format(nb_correct_predictions_past_ten))
            print("Number correct change predictions past ten = {}".format(nb_correct_change_predictions_past_ten))
            logger.info("Number correct change predictions past ten = {}".format(nb_correct_change_predictions_past_ten))
            print(
                "Number correct no change predictions past ten = {}".format(
                    nb_correct_predictions_past_ten - nb_correct_change_predictions_past_ten))
            logger.info(
                "Number correct no change predictions past ten = {}".format(
                    nb_correct_predictions_past_ten - nb_correct_change_predictions_past_ten))
            print("Number false predictions past ten = {}".format(nb_cases_past_ten - nb_correct_predictions_past_ten))
            logger.info(
                "Number false predictions past ten = {}".format(nb_cases_past_ten - nb_correct_predictions_past_ten))
            print("Number false change predictions past ten = {}".format(nb_false_change_predictions_past_ten))
            logger.info("Number false change predictions past ten = {}".format(nb_false_change_predictions_past_ten))
            print("Number false no change predictions past ten = {}".format(
                nb_cases_past_ten - nb_correct_predictions_past_ten - nb_false_change_predictions_past_ten))
            logger.info("Number false no change predictions past ten = {}".format(
                nb_cases_past_ten - nb_correct_predictions_past_ten - nb_false_change_predictions_past_ten))
            accuracy_majority_past_ten = nb_most_probable_predictions_past_ten / nb_cases_past_ten
            print("Accuracy majority class past ten = {:.5f}".format(accuracy_majority_past_ten))
            logger.info("Accuracy majority class past ten = {:.5f}".format(accuracy_majority_past_ten))
            accuracy_model_past_ten = nb_correct_predictions_past_ten / nb_cases_past_ten
            print("Accuracy trained model past ten = {:.5f}".format(accuracy_model_past_ten))
            logger.info("Accuracy trained model past ten = {:.5f}".format(accuracy_model_past_ten))

        print(" ")
        print("Evaluation present d")
        logger.info(" ")
        logger.info("Evaluation present d")
        if nb_cases_present_d == 0:
            print("No such cases")
            logger.info("No such cases")
        else:
            print("Total cases present d = {}".format(nb_cases_present_d))
            logger.info("Total cases present d = {}".format(nb_cases_present_d))
            print("Number most probable predictions present d = {}".format(nb_most_probable_predictions_present_d))
            logger.info("Number most probable predictions present d = {}".format(nb_most_probable_predictions_present_d))
            print("Number correct predictions present d = {}".format(nb_correct_predictions_present_d))
            logger.info("Number correct predictions present d = {}".format(nb_correct_predictions_present_d))
            print("Number correct change predictions present d = {}".format(nb_correct_change_predictions_present_d))
            logger.info("Number correct change predictions present d = {}".format(nb_correct_change_predictions_present_d))
            print(
                "Number correct no change predictions present d = {}".format(
                    nb_correct_predictions_present_d - nb_correct_change_predictions_present_d))
            logger.info(
                "Number correct no change predictions present d = {}".format(
                    nb_correct_predictions_present_d - nb_correct_change_predictions_present_d))
            print("Number false predictions present d = {}".format(nb_cases_present_d - nb_correct_predictions_present_d))
            logger.info(
                "Number false predictions present d = {}".format(nb_cases_present_d - nb_correct_predictions_present_d))
            print("Number false change predictions present d = {}".format(nb_false_change_predictions_present_d))
            logger.info("Number false change predictions present d = {}".format(nb_false_change_predictions_present_d))
            print("Number false no change predictions present d = {}".format(
                nb_cases_present_d - nb_correct_predictions_present_d - nb_false_change_predictions_present_d))
            logger.info("Number false no change predictions present d = {}".format(
                nb_cases_present_d - nb_correct_predictions_present_d - nb_false_change_predictions_present_d))
            accuracy_majority_present_d = nb_most_probable_predictions_present_d / nb_cases_present_d
            print("Accuracy majority class present d = {:.5f}".format(accuracy_majority_present_d))
            logger.info("Accuracy majority class present d = {:.5f}".format(accuracy_majority_present_d))
            accuracy_model_present_d = nb_correct_predictions_present_d / nb_cases_present_d
            print("Accuracy trained model present d = {:.5f}".format(accuracy_model_present_d))
            logger.info("Accuracy trained model present d = {:.5f}".format(accuracy_model_present_d))

        print(" ")
        print("Evaluation present dt")
        logger.info(" ")
        logger.info("Evaluation present dt")
        if nb_cases_present_dt == 0:
            print("No such cases")
            logger.info("No such cases")
        else:
            print("Total cases present dt = {}".format(nb_cases_present_dt))
            logger.info("Total cases present dt = {}".format(nb_cases_present_dt))
            print("Number most probable predictions present dt = {}".format(nb_most_probable_predictions_present_dt))
            logger.info("Number most probable predictions present dt = {}".format(nb_most_probable_predictions_present_dt))
            print("Number correct predictions present dt = {}".format(nb_correct_predictions_present_dt))
            logger.info("Number correct predictions present dt = {}".format(nb_correct_predictions_present_dt))
            print("Number correct change predictions present dt = {}".format(nb_correct_change_predictions_present_dt))
            logger.info("Number correct change predictions present dt = {}".format(nb_correct_change_predictions_present_dt))
            print(
                "Number correct no change predictions present dt = {}".format(
                    nb_correct_predictions_present_dt - nb_correct_change_predictions_present_dt))
            logger.info(
                "Number correct no change predictions present dt = {}".format(
                    nb_correct_predictions_present_dt - nb_correct_change_predictions_present_dt))
            print("Number false predictions present dt = {}".format(nb_cases_present_dt - nb_correct_predictions_present_dt))
            logger.info(
                "Number false predictions present dt = {}".format(nb_cases_present_dt - nb_correct_predictions_present_dt))
            print("Number false change predictions present dt = {}".format(nb_false_change_predictions_present_dt))
            logger.info("Number false change predictions present dt = {}".format(nb_false_change_predictions_present_dt))
            print("Number false no change predictions present dt = {}".format(
                nb_cases_present_dt - nb_correct_predictions_present_dt - nb_false_change_predictions_present_dt))
            logger.info("Number false no change predictions present dt = {}".format(
                nb_cases_present_dt - nb_correct_predictions_present_dt - nb_false_change_predictions_present_dt))
            accuracy_majority_present_dt = nb_most_probable_predictions_present_dt / nb_cases_present_dt
            print("Accuracy majority class present dt = {:.5f}".format(accuracy_majority_present_dt))
            logger.info("Accuracy majority class present dt = {:.5f}".format(accuracy_majority_present_dt))
            accuracy_model_present_dt = nb_correct_predictions_present_dt / nb_cases_present_dt
            print("Accuracy trained model present dt = {:.5f}".format(accuracy_model_present_dt))
            logger.info("Accuracy trained model present dt = {:.5f}".format(accuracy_model_present_dt))

        print(" ")
        print("Evaluation present t")
        logger.info(" ")
        logger.info("Evaluation present t")
        if nb_cases_present_t == 0:
            print("No such cases")
            logger.info("No such cases")
        else:
            print("Total cases present t = {}".format(nb_cases_present_t))
            logger.info("Total cases present t = {}".format(nb_cases_present_t))
            print("Number most probable predictions present t = {}".format(nb_most_probable_predictions_present_t))
            logger.info("Number most probable predictions present t = {}".format(nb_most_probable_predictions_present_t))
            print("Number correct predictions present t = {}".format(nb_correct_predictions_present_t))
            logger.info("Number correct predictions present t = {}".format(nb_correct_predictions_present_t))
            print("Number correct change predictions present t = {}".format(nb_correct_change_predictions_present_t))
            logger.info("Number correct change predictions present t = {}".format(nb_correct_change_predictions_present_t))
            print(
                "Number correct no change predictions present t = {}".format(
                    nb_correct_predictions_present_t - nb_correct_change_predictions_present_t))
            logger.info(
                "Number correct no change predictions present t = {}".format(
                    nb_correct_predictions_present_t - nb_correct_change_predictions_present_t))
            print("Number false predictions present t = {}".format(nb_cases_present_t - nb_correct_predictions_present_t))
            logger.info(
                "Number false predictions present t = {}".format(nb_cases_present_t - nb_correct_predictions_present_t))
            print("Number false change predictions present t = {}".format(nb_false_change_predictions_present_t))
            logger.info("Number false change predictions present t = {}".format(nb_false_change_predictions_present_t))
            print("Number false no change predictions present t = {}".format(
                nb_cases_present_t - nb_correct_predictions_present_t - nb_false_change_predictions_present_t))
            logger.info("Number false no change predictions present t = {}".format(
                nb_cases_present_t - nb_correct_predictions_present_t - nb_false_change_predictions_present_t))
            accuracy_majority_present_t = nb_most_probable_predictions_present_t / nb_cases_present_t
            print("Accuracy majority class present t = {:.5f}".format(accuracy_majority_present_t))
            logger.info("Accuracy majority class present t = {:.5f}".format(accuracy_majority_present_t))
            accuracy_model_present_t = nb_correct_predictions_present_t / nb_cases_present_t
            print("Accuracy trained model present t = {:.5f}".format(accuracy_model_present_t))
            logger.info("Accuracy trained model present t = {:.5f}".format(accuracy_model_present_t))

        print(" ")
        print("Evaluation past participle d")
        logger.info(" ")
        logger.info("Evaluation past participle d")
        if nb_cases_past_participle_d == 0:
            print("No such cases")
            logger.info("No such cases")
        else:
            print("Total cases past participle d = {}".format(nb_cases_past_participle_d))
            logger.info("Total cases past participle d = {}".format(nb_cases_past_participle_d))
            print("Number most probable predictions past participle d = {}".format(nb_most_probable_predictions_past_participle_d))
            logger.info("Number most probable predictions past participle d = {}".format(nb_most_probable_predictions_past_participle_d))
            print("Number correct predictions past participle d = {}".format(nb_correct_predictions_past_participle_d))
            logger.info("Number correct predictions past participle d = {}".format(nb_correct_predictions_past_participle_d))
            print("Number correct change predictions past participle d = {}".format(nb_correct_change_predictions_past_participle_d))
            logger.info("Number correct change predictions past participle d = {}".format(nb_correct_change_predictions_past_participle_d))
            print(
                "Number correct no change predictions past participle d = {}".format(
                    nb_correct_predictions_past_participle_d - nb_correct_change_predictions_past_participle_d))
            logger.info(
                "Number correct no change predictions past participle d = {}".format(
                    nb_correct_predictions_past_participle_d - nb_correct_change_predictions_past_participle_d))
            print("Number false predictions past participle d = {}".format(nb_cases_past_participle_d - nb_correct_predictions_past_participle_d))
            logger.info(
                "Number false predictions past participle d = {}".format(nb_cases_past_participle_d - nb_correct_predictions_past_participle_d))
            print("Number false change predictions past participle d = {}".format(nb_false_change_predictions_past_participle_d))
            logger.info("Number false change predictions past participle d = {}".format(nb_false_change_predictions_past_participle_d))
            print("Number false no change predictions past participle d = {}".format(
                nb_cases_past_participle_d - nb_correct_predictions_past_participle_d - nb_false_change_predictions_past_participle_d))
            logger.info("Number false no change predictions past participle d = {}".format(
                nb_cases_past_participle_d - nb_correct_predictions_past_participle_d - nb_false_change_predictions_past_participle_d))
            accuracy_majority_past_participle_d = nb_most_probable_predictions_past_participle_d / nb_cases_past_participle_d
            print("Accuracy majority class past participle d = {:.5f}".format(accuracy_majority_past_participle_d))
            logger.info("Accuracy majority class past participle d = {:.5f}".format(accuracy_majority_past_participle_d))
            accuracy_model_past_participle_d = nb_correct_predictions_past_participle_d / nb_cases_past_participle_d
            print("Accuracy trained model past participle d = {:.5f}".format(accuracy_model_past_participle_d))
            logger.info("Accuracy trained model past participle d = {:.5f}".format(accuracy_model_past_participle_d))

        print(" ")
        print("Evaluation past participle t")
        logger.info(" ")
        logger.info("Evaluation past participle t")
        if nb_cases_past_participle_t == 0:
            print("No such cases")
            logger.info("No such cases")
        else:
            print("Total cases past participle t = {}".format(nb_cases_past_participle_t))
            logger.info("Total cases past participle t = {}".format(nb_cases_past_participle_t))
            print("Number most probable predictions past participle t = {}".format(nb_most_probable_predictions_past_participle_t))
            logger.info("Number most probable predictions past participle t = {}".format(nb_most_probable_predictions_past_participle_t))
            print("Number correct predictions past participle t = {}".format(nb_correct_predictions_past_participle_t))
            logger.info("Number correct predictions past participle t = {}".format(nb_correct_predictions_past_participle_t))
            print("Number correct change predictions past participle t = {}".format(nb_correct_change_predictions_past_participle_t))
            logger.info("Number correct change predictions past participle t = {}".format(nb_correct_change_predictions_past_participle_t))
            print(
                "Number correct no change predictions past participle t = {}".format(
                    nb_correct_predictions_past_participle_t - nb_correct_change_predictions_past_participle_t))
            logger.info(
                "Number correct no change predictions past participle t = {}".format(
                    nb_correct_predictions_past_participle_t - nb_correct_change_predictions_past_participle_t))
            print("Number false predictions past participle t = {}".format(nb_cases_past_participle_t - nb_correct_predictions_past_participle_t))
            logger.info(
                "Number false predictions past participle t = {}".format(nb_cases_past_participle_t - nb_correct_predictions_past_participle_t))
            print("Number false change predictions past participle t = {}".format(nb_false_change_predictions_past_participle_t))
            logger.info("Number false change predictions past participle t = {}".format(nb_false_change_predictions_past_participle_t))
            print("Number false no change predictions past participle t = {}".format(
                nb_cases_past_participle_t - nb_correct_predictions_past_participle_t - nb_false_change_predictions_past_participle_t))
            logger.info("Number false no change predictions past participle t = {}".format(
                nb_cases_past_participle_t - nb_correct_predictions_past_participle_t - nb_false_change_predictions_past_participle_t))
            accuracy_majority_past_participle_t = nb_most_probable_predictions_past_participle_t / nb_cases_past_participle_t
            print("Accuracy majority class past participle t = {:.5f}".format(accuracy_majority_past_participle_t))
            logger.info("Accuracy majority class past participle t = {:.5f}".format(accuracy_majority_past_participle_t))
            accuracy_model_past_participle_t = nb_correct_predictions_past_participle_t / nb_cases_past_participle_t
            print("Accuracy trained model past participle t = {:.5f}".format(accuracy_model_past_participle_t))
            logger.info("Accuracy trained model past participle t = {:.5f}".format(accuracy_model_past_participle_t))

        print(" ")
        print("Overview total")
        logger.info(" ")
        logger.info("Overview total")

        print("Total cases = {}".format(nb_total_cases))
        logger.info("Total cases = {}".format(nb_total_cases))

        print("Number most probable predictions = {}".format(nb_most_probable_predictions))
        logger.info("Number most probable predictions = {}".format(nb_most_probable_predictions))

        print("Number correct predictions = {}".format(nb_correct_predictions))
        logger.info("Number correct predictions = {}".format(nb_correct_predictions))

        print("Number correct change predictions = {}".format(nb_correct_change_predictions))
        logger.info("Number correct change predictions = {}".format(nb_correct_change_predictions))

        print("Number correct no change predictions = {}".format(nb_correct_predictions - nb_correct_change_predictions))
        logger.info("Number correct no change predictions = {}".format(nb_correct_predictions - nb_correct_change_predictions))

        print("Number false predictions = {}".format(nb_total_cases - nb_correct_predictions))
        logger.info("Number false predictions = {}".format(nb_total_cases - nb_correct_predictions))

        print("Number false change predictions = {}".format(nb_false_change_predictions))
        logger.info("Number false change predictions = {}".format(nb_false_change_predictions))

        print("Number false no change predictions = {}".format(nb_total_cases - nb_correct_predictions - nb_false_change_predictions))
        logger.info("Number false no change predictions = {}".format(nb_total_cases - nb_correct_predictions - nb_false_change_predictions))

        accuracy_majority = nb_most_probable_predictions/nb_total_cases
        print("Accuracy majority class = {:.5f}".format(accuracy_majority))
        logger.info("Accuracy majority class = {:.5f}".format(accuracy_majority))

        accuracy_model = nb_correct_predictions/nb_total_cases
        print("Accuracy trained model = {:.5f}".format(accuracy_model))
        logger.info("Accuracy trained model = {:.5f}".format(accuracy_model))

        print("Time testing = {}".format(stop_testing - start_testing))
        logger.info("Time testing = {}".format(stop_testing - start_testing))

    stop_model_evaluation_time = datetime.datetime.now()
    print("Time evaluating model = {}".format(stop_model_evaluation_time - start_model_evaluation_time))
    logger.info("Time evaluating model = {}".format(stop_model_evaluation_time - start_model_evaluation_time))

    logger.info(" ")
    logger.info("----------STOP EVALUATION----------")
    logger.info(" ")

if __name__ == '__main__':
    main()