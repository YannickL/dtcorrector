import tensorflow as tf
import Utils
flags = tf.flags

flags.DEFINE_string(
    "model", "small",
    "Een model type. Mogelijkheden zijn: small, big.")
flags.DEFINE_string("data_path", None,
                    "Where the training/test data is stored.")
flags.DEFINE_string("save_path", None,
                    "Model output directory.")

FLAGS = flags.FLAGS

# class CorrectorInput(object):
#   """The input data."""
#
#   def __init__(self, config, data, answers, name=None):
#       # The size of the batch used
#       self.batch_size = batch_size = config.batch_size
#       # The number of unrolled steps of the LSTM
#       self.num_steps = num_steps = config.num_steps
#       # An epoch is a measure of the number of times all of the training vectors are used once to update the weights.
#       # For batch training all of the training samples pass through the learning algorithm simultaneously
#       # in one epoch before weights are updated
#       # Number of iterations
#       self.epoch_size = ((len(data) // batch_size) - 1) // num_steps
#       self.input_data, self.targets = input_reader.gen_batch(data, answers, batch_size, num_steps, name=name)


class CorrectorModel(object):
    # feature size =
    # output size = aantal edit regels
    def __init__(self, vocab_size_words, vocab_size_characters, embedding_size, batch_size, hidden_size,
                 number_of_layers, max_length_context, max_length_verbs):
        # self._max_timestep = max_timestep
        # self._feature_size = feature_size
        self._vocab_size_words = vocab_size_words
        self._vocab_size_characters = vocab_size_characters
        self._output_size = len(Utils.AdaptRule)
        self._hidden_size = hidden_size
        self._number_of_layers = number_of_layers
        self._batch_size = batch_size
        self._embedding_size = embedding_size
        # self._max_epoch = 100
        self._max_grad_norm = 5
        # self._model_path = "corrector_model.ckpt"

        self.max_length_context = max_length_context
        self.max_length_verb = max_length_verbs


        # [timestep, mini-batch, feature dims]

        # GEERT: tweede argument mag niet None zijn: moet de maximale lengte van de sequentie zijn
        self.input_data_verb = tf.placeholder(tf.float32, [None, self.max_length_verb, vocab_size_characters])
        self.input_data_left_context = tf.placeholder(tf.int32, [None, self.max_length_context])
        self.input_data_right_context = tf.placeholder(tf.int32, [None, self.max_length_context])

        # self._input_data_verb = tf.placeholder(tf.int32, [self.batch_size, self.seq_length_verb])
        # self._input_data_left_context = tf.placeholder(tf.int32, [self.batch_size, self.seq_length_context])
        # self._input_data_right_context = tf.placeholder(tf.int32, [self.batch_size, self.seq_length_context])

        self.target = tf.placeholder(tf.int32, [None, self._output_size])

        # GEERT: seq_length_left_context klopt, je zal hetzelfde voor right_context en verb moeten doen
        self.seq_length_verb = tf.placeholder(tf.int32, [None])
        self.seq_length_left_context = tf.placeholder(tf.int32, [None])
        self.seq_length_right_context = tf.placeholder(tf.int32, [None])

        # print("Control")

        with tf.device("/cpu:0"):
            #TODO vocab size? Alle mogelijke werkwoordsvormen?
            # vocab_size is aantal mogelijke woorden
            # size is de grootte van de embeddingsvector

            # GEERT: de split/squeeze operaties zijn overbodig:
            # left_inputs = tf.nn.embedding_lookup(embedding_words, self._input_data_left_context) volstaat
            # Deze hidden size moet niet dezelfde grootte hebben als hidden size van lstm cell

            embedding_words = tf.Variable(tf.random_uniform(shape=[self._vocab_size_words, self._embedding_size],
                                                            minval=-1.0, maxval=1.0), trainable=False, name="W")
            self.embedding_words = embedding_words

            # self.embedding_words = tf.get_variable("embedding", [self._vocab_size_words, self._embedding_size])

            # [batch_size x time x self._hidden_size]
            left_inputs = tf.nn.embedding_lookup(self.embedding_words, self.input_data_left_context)
            right_inputs = tf.nn.embedding_lookup(self.embedding_words, self.input_data_right_context)

        # print("Control")

        # with tf.variable_scope("left"):
        lstm_left_cell = tf.contrib.rnn.BasicLSTMCell(self._hidden_size)
        # with tf.variable_scope("multi_left"):
        multi_lstm_left_cell = tf.contrib.rnn.MultiRNNCell([lstm_left_cell] * self._number_of_layers,
                                                           state_is_tuple=True)
        # with tf.variable_scope("right"):
        lstm_right_cell = tf.contrib.rnn.BasicLSTMCell(self._hidden_size)
        # with tf.variable_scope("multi_right"):
        multi_lstm_right_cell = tf.contrib.rnn.MultiRNNCell([lstm_right_cell] * self._number_of_layers,
                                                            state_is_tuple=True)
        # with tf.variable_scope("verb"):
        lstm_verb_cell = tf.contrib.rnn.BasicLSTMCell(self._hidden_size)
        # with tf.variable_scope("multi_verb"):
        multi_lstm_verb_cell = tf.contrib.rnn.MultiRNNCell([lstm_verb_cell] * self._number_of_layers,
                                                            state_is_tuple=True)

        # GEERT: gebruik self._seq_length_context enz.
        # GEERT: wanneer je hierboven left_inputs = tf.nn.embedding_lookup(embedding_words, self._input_data_left_context) enz. gebruikt => time_major=False

        # [batch_size, max_time, num_cells (momenteel = self._hidden_size)]
        with tf.variable_scope("verb"):
            outputs_verb, verb_context_states = \
                tf.nn.dynamic_rnn(multi_lstm_verb_cell, self.input_data_verb, sequence_length=self.seq_length_verb,
                                  dtype=tf.float32, time_major=False)
        with tf.variable_scope("left"):
            outputs_left_context, left_context_states = \
                tf.nn.dynamic_rnn(multi_lstm_left_cell, left_inputs, sequence_length=self.seq_length_left_context,
                                  dtype=tf.float32, time_major=False)
        with tf.variable_scope("right"):
            outputs_right_context, right_context_states = \
                tf.nn.dynamic_rnn(multi_lstm_right_cell, right_inputs, sequence_length=self.seq_length_right_context,
                                  dtype=tf.float32, time_major=False)

        # print("Control")

        #  Voor outputs te gebruiken -> Eruit halen wat de laatste output state is (geen padding symbool)
        outputs_verb_shape = tf.shape(outputs_verb)
        # print(outputs_verb)
        batch_size = outputs_verb_shape[0]
        max_len_verbs = outputs_verb_shape[1]
        num_cells = outputs_verb_shape[2]
        outputs_verb = tf.reshape(outputs_verb, [-1, num_cells])
        indices_verb = self.seq_length_verb - 1 + tf.range(batch_size) * max_len_verbs
        last_output_verbs = tf.nn.embedding_lookup(outputs_verb, indices_verb)

        outputs_left_context_shape = tf.shape(outputs_left_context)
        batch_size = outputs_left_context_shape[0]
        max_len_context = outputs_left_context_shape[1]
        num_cells = outputs_left_context_shape[2]
        outputs_left_context = tf.reshape(outputs_left_context, [-1, num_cells])
        indices_left_context = self.seq_length_left_context - 1 + tf.range(batch_size) * max_len_context

        # indices_left_context = tf.Print(indices_left_context, [self.seq_length_left_context, batch_size, max_len_context, indices_left_context], summarize=1000)

        last_output_left_context = tf.nn.embedding_lookup(outputs_left_context, indices_left_context)

        outputs_right_context_shape = tf.shape(outputs_right_context)
        batch_size = outputs_right_context_shape[0]
        max_len_context = outputs_right_context_shape[1]
        num_cells = outputs_right_context_shape[2]
        outputs_right_context = tf.reshape(outputs_right_context, [-1, num_cells])
        indices_right_context = self.seq_length_right_context - 1 + tf.range(batch_size) * max_len_context
        last_output_right_context = tf.nn.embedding_lookup(outputs_right_context, indices_right_context)

        output = tf.concat([last_output_verbs, last_output_left_context, last_output_right_context], 1)

        # Gebruik van states
        # [batch_size, 3 x num_cells ]
        # output = tf.concat(2, [verb_context_states[-1], left_context_states[-1], right_context_states[-1]])

        softmax_weight = tf.get_variable("softmax_w", [3*self._hidden_size, self._output_size])
        softmax_bias = tf.get_variable("softmax_b", [self._output_size])
        self.prediction = tf.matmul(output, softmax_weight) + softmax_bias

        #  loss runne zodak ik kan zien of loss naar benede gaat (tijdens traine, tijdens teste kan niet want dan heb ik geen target)
        self.loss = tf.nn.softmax_cross_entropy_with_logits(logits=self.prediction, labels=self.target)
        self.cost = tf.reduce_sum(self.loss) / self._batch_size

        # self.lr = tf.Variable(0.001, trainable=False)
        tvars = tf.trainable_variables()
        grads, _ = tf.clip_by_global_norm(tf.gradients(self.loss, tvars), self._max_grad_norm)
        optimizer = tf.train.AdamOptimizer()
        # train_op runnen (hier gebeurt echte training) (tijdens traine, niet tijdens teste)
        self.train_op = optimizer.apply_gradients(zip(grads, tvars))
