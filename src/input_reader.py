import os
import pprint
import treetaggerwrapper
import nltk.data
from nltk.tokenize import word_tokenize
import tensorflow as tf
import collections
import re
import numpy as np
from enum import Enum
import random
from six.moves import cPickle
import math
import datetime
import gensim
import logging

# logging.basicConfig(filename='dt_Corrector.log', level=logging.DEBUG)
# logging = logging.getlogging('dt_Corrector')
# hdlr = logging.FileHandler('home/yakke/Dropbox/Masterproef/dtCorrector/dt_Corrector.log')
# formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
# hdlr.setFormatter(formatter)
# logging.addHandler(hdlr)


tokenizer = nltk.data.load('tokenizers/punkt/dutch.pickle')
tagger = treetaggerwrapper.TreeTagger(TAGLANG='nl')

unknown_token = "unknown"


class TrainingTextLoader:
    def __init__(self, data_path, batch_size, max_verb_sequence_length, max_context_sequence_length, data_size,
                 min_appearances_word, embedding_size=100):
        self.data_path = data_path
        self.batch_size = batch_size
        self.max_verb_sequence_length = max_verb_sequence_length
        self.max_context_sequence_length = max_context_sequence_length
        self.embedding_size = embedding_size
        # self.word_to_id = None
        # self.character_to_id = None
        # self.words = None
        # self.chars = None

        # # Converts the input files to the right input format and converted to file-id's
        # def corrector_raw_data(data_path=None):
        #     train_path = os.path.join(data_path, "corrector.train.txt")
        #     train_answers_path = os.path.join(data_path, "corrector.train_answers.txt")
        #     valid_path = os.path.join(data_path, "corrector.valid.txt")
        #     valid_answers_path = os.path.join(data_path, "corrector.valid_answers.txt")
        #     test_path = os.path.join(data_path, "corrector.test.txt")
        #     test_answers_path = os.path.join(data_path, "corrector.test_answers.txt")
        #
        #     word_to_id = _build_word_vocab(train_path, train_answers_path)
        #     character_to_id = _build_character_vocab(train_path, train_answers_path)
        #
        #     train_data = convert_input_file_to_ids(train_path, word_to_id, character_to_id)
        #     train_answers_data = convert_target_file_to_probability_output(train_answers_path)
        #     valid_data = convert_input_file_to_ids(valid_path, word_to_id, character_to_id)
        #     valid_answers_data = convert_target_file_to_probability_output(valid_answers_path)
        #     test_data = convert_input_file_to_ids(test_path, word_to_id, character_to_id)
        #     test_answers_data = convert_target_file_to_probability_output(test_answers_path)
        #
        #     return train_data, train_answers_data, valid_data, valid_answers_data, test_data, test_answers_data


        # train_path = os.path.join(data_path, "corrector.train.txt")
        # train_answers_path = os.path.join(data_path, "corrector.train_answers.txt")
        # valid_path = os.path.join(data_path, "corrector.valid.txt")
        # valid_answers_path = os.path.join(data_path, "corrector.valid_answers.txt")
        # test_path = os.path.join(data_path, "corrector.test.txt")
        # test_answers_path = os.path.join(data_path, "corrector.test_answers.txt")

        input_file = os.path.join(data_path, "train_original.txt")
        edited_input_file = os.path.join(data_path, "train_edited.txt")
        target_file = os.path.join(data_path, "train_answers.txt")
        word_vocab_file = os.path.join(data_path, "vocab_words2.pkl")
        char_vocab_file = os.path.join(data_path, "vocab_chars2.pkl")
        embedding_file = os.path.join(data_path, "embedding2")
        input_tensor_file = os.path.join(data_path, "input.npy")
        output_tensor_file = os.path.join(data_path, "output.npy")

        # input_file = os.path.join(data_path, "corrector.train_original.txt")
        # edited_input_file = os.path.join(data_path, "corrector.train_edited.txt")
        # target_file = os.path.join(data_path, "corrector.train_answers.txt")
        # word_vocab_file = os.path.join(data_path, "vocab_words.pkl")
        # char_vocab_file = os.path.join(data_path, "vocab_chars.pkl")
        # embedding_file = os.path.join(data_path, "embedding")
        # input_tensor_file = os.path.join(data_path, "input.npy")
        # output_tensor_file = os.path.join(data_path, "output.npy")

        if not os.path.exists(edited_input_file):
            start = datetime.datetime.now()
            print("Edit input file")
            logging.info("Edit input file")
            _replace_infrequent_words_text(input_file, edited_input_file, min_appearances_word)
            end = datetime.datetime.now()
            duration = end - start
            print("Time duration: {}".format(duration))
            logging.info("Time duration: {}".format(duration))

        if not (os.path.exists(embedding_file)):
            print("Building embedding")
            logging.info("Building embedding")
            start = datetime.datetime.now()
            self._embedding = _build_embedding_from_file(edited_input_file, embedding_file, size=embedding_size,
                                                         min_count=min_appearances_word)
            end = datetime.datetime.now()
            duration = end - start
            print("Time duration: {}".format(duration))
            logging.info("Time duration: {}".format(duration))
        else:
            print("Loading embedding")
            logging.info("Loading embedding")
            start = datetime.datetime.now()
            self._embedding = _load_embedding(embedding_file)
            end = datetime.datetime.now()
            duration = end - start
            print("Time duration: {}".format(duration))
            logging.info("Time duration: {}".format(duration))

        if not (os.path.exists(word_vocab_file) and os.path.exists(char_vocab_file)):
            print("Building vocabs")
            logging.info("Building vocabs")
            start = datetime.datetime.now()
            self._build_vocabs(edited_input_file, word_vocab_file, char_vocab_file)
            end = datetime.datetime.now()
            duration = end - start
            print("Time duration: {}".format(duration))
            logging.info("Time duration: {}".format(duration))
            print("Preprocessing files")
            logging.info("Preprocessing files")
            start = datetime.datetime.now()
            self._preprocess(edited_input_file, target_file, word_vocab_file, char_vocab_file, input_tensor_file,
                             output_tensor_file, data_size)
            end = datetime.datetime.now()
            duration = end - start
            print("Time duration: {}".format(duration))
            logging.info("Time duration: {}".format(duration))
        elif not (os.path.exists(input_tensor_file) and os.path.exists(output_tensor_file)):
            print("Preprocessing files")
            logging.info("Preprocessing files")
            start = datetime.datetime.now()
            self._preprocess(input_file, target_file, word_vocab_file, char_vocab_file, input_tensor_file,
                             output_tensor_file, data_size)
            end = datetime.datetime.now()
            duration = end - start
            print("Time duration: {}".format(duration))
            logging.info("Time duration: {}".format(duration))
        else:
            print("Loading preprocessed files")
            logging.info("Loading preprocessed files")
            start = datetime.datetime.now()
            self._load_preprocessed(word_vocab_file, char_vocab_file, input_tensor_file, output_tensor_file)
            end = datetime.datetime.now()
            duration = end - start
            print("Time duration: {}".format(duration))
            logging.info("Time duration: {}".format(duration))

        # self.verb_data_batches_training, self.verb_seq_length_batches_training, \
        # self.left_context_data_batches_training, self.left_context_seq_length_batches_training, \
        # self.right_context_data_batches_training, self.right_context_seq_length_batches_training, \
        # self.target_batches_training = self._create_batches_2(train_data)
        #
        # self.verb_data_batches_validation, self.verb_seq_length_batches_validation, \
        # self.left_context_data_batches_validation, self.left_context_seq_length_batches_validation, \
        # self.right_context_data_batches_validation, self.right_context_seq_length_batches_validation, \
        # self.target_batches_validation = self._create_batches_2(validation_data)
        #
        # self.verb_data_batches_test, self.verb_seq_length_batches_test, \
        # self.left_context_data_batches_test, self.left_context_seq_length_batches_test, \
        # self.right_context_data_batches_test, self.right_context_seq_length_batches_test = \
        #     self._create_batches_2(test_data)

        self._create_batches()
        self.reset_batch_pointer()


    def _build_vocabs(self, input_file, word_vocab_file, char_vocab_file):
        """Function to build the vocabs (word vocab and character vocab).

        Gets the words vocab and builds the character vocab.
        The words, characters and size of the vocabs are stored in object variables.
        The words and characters are also save in files.

        Args:
            input_file (str): The file used to build the vocabs.
            word_vocab_file (str): The file where the words are stored.
            word_vocab_file (str): The file where the characters are stored.

        Returns:
            None
        """
        self.word_to_id = self._get_embedding_sorted_word_to_id()
        self.character_to_id = _build_character_vocab(input_file)
        self.words, _ = zip(*self.word_to_id.items())
        self.word_vocab_size = len(self.words)
        self.chars, _ = zip(*self.character_to_id.items())
        self.char_vocab_size = len(self.chars)
        with open(word_vocab_file, 'wb') as f:
            cPickle.dump(self.words, f)
        with open(char_vocab_file, 'wb') as f:
            cPickle.dump(self.chars, f)

    def _get_embedding_sorted_vocab(self):
        """Function to get the sorted list of words in the vocab used in the embedding.

        Gets the sorted list of words in the vocab, from most used to less used, from the embedding.
        The result depends on the vocab used in the embedding, so also on the parameters used to build the embedding.

        Returns:
            Sorted list of words.
        """
        return self._embedding.wv.index2word

    # TODO USED
    def _get_embedding_sorted_word_to_id(self):
        words = self._embedding.wv.index2word
        word_to_id = dict(zip(words, range(1, len(words) + 1)))
        return word_to_id

    # TODO USED
    def get_embedding_model_repr(self):
        return self._embedding.wv.syn0

    # TODO USED
    def _preprocess(self, input_file, target_file, word_vocab_file, char_vocab_file, input_tensor_file,
                    output_tensor_file, data_size):
        with open(word_vocab_file, 'rb') as f:
            self.words = cPickle.load(f)
        self.word_vocab_size = len(self.words)
        self.word_to_id = dict(zip(self.words, range(len(self.words))))
        with open(char_vocab_file, 'rb') as f:
            self.chars = cPickle.load(f)
        self.char_vocab_size = len(self.chars)
        self.character_to_id = dict(zip(self.chars, range(len(self.chars))))
        # print(self.character_to_id)

        with open(input_file) as in_f:
            input_lines = in_f.read().splitlines()
            # input_lines = [x.lower() for x in input_lines]
        with open(target_file) as out_f:
            target_lines = out_f.read().splitlines()
            # target_lines = [x.lower() for x in target_lines]
        # Delete empty strings
        input_lines = list(filter(None, input_lines))
        target_lines = list(filter(None, target_lines))

        # Shuffle both lists (relation between input and target is kept)
        input_lines, target_lines = shuffle_two_lists(input_lines, target_lines)
        # Only take the amount of data specified
        input_lines = input_lines[:data_size]
        target_lines = target_lines[:data_size]

        train_data, train_answers_data = convert_input_and_target(input_lines, target_lines, self.word_to_id,
                                                                  self.character_to_id,
                                                                  self.max_context_sequence_length,
                                                                  self.max_verb_sequence_length)
        # print(train_data)
        # print(train_answers_data)

        np.save(input_tensor_file, train_data)
        np.save(output_tensor_file, train_answers_data)
        self.input_tensor = train_data
        self.output_tensor = train_answers_data

    # TODO USED
    def _load_preprocessed(self, word_vocab_file, char_vocab_file, input_tensor_file, output_tensor_file):
        with open(word_vocab_file, 'rb') as f:
            self.words = cPickle.load(f)
        self.word_vocab_size = len(self.words)
        self.word_to_id = dict(zip(self.words, range(len(self.words))))
        with open(char_vocab_file, 'rb') as f:
            self.chars = cPickle.load(f)
        self.char_vocab_size = len(self.chars)
        self.character_to_id = dict(zip(self.chars, range(len(self.chars))))
        self.input_tensor = np.load(input_tensor_file)
        self.output_tensor = np.load(output_tensor_file)

    # Nodig input_tensor, batch_size, output_tensor
    # Maken van 3 pointers, 1 die bijhoudt waar we zitten met batches van validatietekst, 1 die bijhoudt waar we zitten
    # met batches van testtekst, 1 die bijhoudt waar we zitten met batches van trainingstekst
    def _create_batches_2(self, input_data, input_data_answers):
        data_len = len(input_data)
        num_batches = math.ceil(data_len / self.batch_size)

        verb_input = []
        verb_seq_length = []
        left_input = []
        left_seq_length = []
        right_input = []
        right_seq_length = []
        target_input = []
        # print(self.input_tensor)
        for inp in input_data:
            verb_input.append(inp[0])
            verb_seq_length.append(len(inp[0]))
            left_input.append(inp[1])
            left_seq_length.append(len(inp[1]))
            right_input.append(inp[2])
            right_seq_length.append(len(inp[2]))
        for tar in input_data_answers:
            target_input.append(tar)

        # 0-padding
        for verb in verb_input:
            verb.extend([convert_number_to_one_hot_vector(0, self.char_vocab_size)] * (
                self.max_verb_sequence_length - len(verb)))
        for left_word in left_input:
            left_word.extend([0] * (self.max_context_sequence_length - len(left_word)))
        for right_word in right_input:
            right_word.extend([0] * (self.max_context_sequence_length - len(right_word)))

        verb_data = np.array(verb_input)
        verb_data_seq_length = np.array(verb_seq_length)
        left_context_data = np.array(left_input)
        left_context_data_seq_length = np.array(left_seq_length)
        right_context_data = np.array(right_input)
        right_context_data_seq_length = np.array(right_seq_length)
        target_data = np.array(target_input)

        verb_data_batches = np.array_split(verb_data, num_batches)
        verb_seq_length_batches = np.array_split(verb_data_seq_length, num_batches)
        left_context_data_batches = np.array_split(left_context_data, num_batches)
        left_context_seq_length_batches = np.array_split(left_context_data_seq_length, num_batches)
        right_context_data_batches = np.array_split(right_context_data, num_batches)
        right_context_seq_length_batches = np.array_split(right_context_data_seq_length, num_batches)
        target_batches = np.array_split(target_data, num_batches)

        return verb_data_batches, verb_seq_length_batches, left_context_data_batches, left_context_seq_length_batches, \
            right_context_data_batches, right_context_seq_length_batches, target_batches

    # TODO USED
    def _create_batches(self):
        data_len = len(self.input_tensor)
        self.num_batches = math.ceil(data_len / self.batch_size)
        # print(data_len)
        # print(self.batch_size)
        # print(len(self.output_tensor))
        # print(math.ceil(len(self.output_tensor) / self.batch_size))

        # When the data (tensor) is too small, let's give them a better error message
        if self.num_batches == 0:
            assert False, "Not enough data. Make batch_size small."

        # Vanaf hier tot en met 0 padding mss nog in een preprocess stap doen?
        verb_input = []
        verb_seq_length = []
        left_input = []
        left_seq_length = []
        right_input = []
        right_seq_length = []
        target_input = []
        # print(self.input_tensor)
        for inp in self.input_tensor:
            verb_input.append(inp[0])
            verb_seq_length.append(len(inp[0]))
            left_input.append(inp[1])
            left_seq_length.append(len(inp[1]))
            right_input.append(inp[2])
            right_seq_length.append(len(inp[2]))
        for tar in self.output_tensor:
            target_input.append(tar)
        # print(verb_input)

        # 0-padding
        for verb in verb_input:
            # verb.append([convert_number_to_one_hot_vector(0, self.char_vocab_size)] * (self.max_verb_sequence_length - len(verb)))
            verb.extend([convert_number_to_one_hot_vector(0, self.char_vocab_size)] * (
                self.max_verb_sequence_length - len(verb)))
        for left_word in left_input:
            left_word.extend([0] * (self.max_context_sequence_length - len(left_word)))
        for right_word in right_input:
            right_word.extend([0] * (self.max_context_sequence_length - len(right_word)))

        verb_data = np.array(verb_input)
        verb_data_seq_length = np.array(verb_seq_length)
        left_context_data = np.array(left_input)
        left_context_data_seq_length = np.array(left_seq_length)
        right_context_data = np.array(right_input)
        right_context_data_seq_length = np.array(right_seq_length)
        target_data = np.array(target_input)

        self.verb_data_batches = np.array_split(verb_data, self.num_batches)
        self.verb_seq_length_batches = np.array_split(verb_data_seq_length, self.num_batches)
        self.left_context_data_batches = np.array_split(left_context_data, self.num_batches)
        self.left_context_seq_length_batches = np.array_split(left_context_data_seq_length, self.num_batches)
        self.right_context_data_batches = np.array_split(right_context_data, self.num_batches)
        self.right_context_seq_length_batches = np.array_split(right_context_data_seq_length, self.num_batches)
        self.target_batches = np.array_split(target_data, self.num_batches)

        # print(self.verb_data_batches)
        # print(self.verb_seq_length_batches)
        # print(self.left_context_data_batches)
        # print(self.left_context_seq_length_batches)
        # print(self.right_context_data_batches)
        # print(self.right_context_seq_length_batches)
        # print(self.target_batches)

    # TODO USED
    def next_batch(self):
        verb_data_batch = self.verb_data_batches[self.pointer]
        verb_seq_len_batch = self.verb_seq_length_batches[self.pointer]
        left_context_data_batch = self.left_context_data_batches[self.pointer]
        left_context_seq_length_batch = self.left_context_seq_length_batches[self.pointer]
        right_context_data_batch = self.right_context_data_batches[self.pointer]
        right_context_seq_length_batch = self.right_context_seq_length_batches[self.pointer]
        target_batch = self.target_batches[self.pointer]
        self.pointer += 1

        return verb_data_batch, verb_seq_len_batch, left_context_data_batch, left_context_seq_length_batch, \
            right_context_data_batch, right_context_seq_length_batch, target_batch

    # TODO USED
    def reset_batch_pointer(self):
        self.pointer = 0


# TODO
class CheckerTextLoader:
    pass
    # def __init__(self, data_path, batch_size, max_verb_sequence_length, max_context_sequence_length):
    #     self.data_path = data_path
    #     self.batch_size = batch_size
    #     self.max_verb_sequence_length = max_verb_sequence_length
    #     self.max_context_sequence_length = max_context_sequence_length
    #
    #     # train_path = os.path.join(data_path, "corrector.train.txt")
    #     # train_answers_path = os.path.join(data_path, "corrector.train_answers.txt")
    #     # valid_path = os.path.join(data_path, "corrector.valid.txt")
    #     # valid_answers_path = os.path.join(data_path, "corrector.valid_answers.txt")
    #     # test_path = os.path.join(data_path, "corrector.test.txt")
    #     # test_answers_path = os.path.join(data_path, "corrector.test_answers.txt")
    #
    #     # TODO vocab file meegeve
    #     input_file = os.path.join(data_path, "corrector.test.txt")
    #     word_vocab_file = os.path.join(data_path, "vocab_words.pkl")
    #     char_vocab_file = os.path.join(data_path, "vocab_chars.pkl")
    #     # input_tensor_file = os.path.join(data_path, "input.npy")
    #     # output_tensor_file = os.path.join(data_path, "output.npy")
    #     # if not (os.path.exists(word_vocab_file) and os.path.exists(char_vocab_file)):
    #     #     print("building vocabs")
    #     #     self.build_vocabs(input_file, target_file, word_vocab_file, char_vocab_file)
    #     #     print("preprocessing files")
    #     #     self.preprocess(input_file, target_file, word_vocab_file, char_vocab_file, input_tensor_file,
    #     #                     output_tensor_file)
    #
    #     print("Preprocessing files")
    #     logging.info("Preprocessing files")
    #     self.preprocess(input_file, word_vocab_file, char_vocab_file)
    #     # else:
    #     #     print("loading preprocessed files")
    #     #     self.load_preprocessed(word_vocab_file, char_vocab_file, input_tensor_file, output_tensor_file)
    #
    #     # print(len(input_data))
    #     # print("-----")
    #     # print(len(target_data))
    #     self.create_batches()
    #     self.reset_batch_pointer()
    #
    # def preprocess(self, input_file, word_vocab_file, char_vocab_file):
    #     with open(word_vocab_file, 'rb') as f:
    #         self.words = cPickle.load(f)
    #     self.word_vocab_size = len(self.words)
    #     self.word_to_id = dict(zip(self.words, range(len(self.words))))
    #     with open(char_vocab_file, 'rb') as f:
    #         self.chars = cPickle.load(f)
    #     self.char_vocab_size = len(self.chars)
    #     self.character_to_id = dict(zip(self.chars, range(len(self.chars))))
    #     # print(self.character_to_id)
    #
    #     with open(input_file) as in_f:
    #         input_lines = in_f.read().splitlines()
    #
    #     train_data, verbs = convert_input_file_to_ids(input_lines, self.word_to_id, self.character_to_id,
    #                                                   self.max_context_sequence_length, self.max_verb_sequence_length)
    #
    #     self.input_tensor = train_data
    #     self.input_verbs = verbs
    #
    # def create_batches(self):
    #     data_len = len(self.input_tensor)
    #     self.num_batches = data_len // self.batch_size
    #
    #     # When the data (tensor) is too small, let's give them a better error message
    #     if self.num_batches == 0:
    #         assert False, "Not enough data. Make batch_size small."
    #
    #     # Vanaf hier tot en met 0 padding mss nog in een preprocess stap doen?
    #     verb_input = []
    #     verb_seq_length = []
    #     left_input = []
    #     left_seq_length = []
    #     right_input = []
    #     right_seq_length = []
    #     for inp in self.input_tensor:
    #         verb_input.append(inp[0])
    #         verb_seq_length.append(len(inp[0]))
    #         left_input.append(inp[1])
    #         left_seq_length.append(len(inp[1]))
    #         right_input.append(inp[2])
    #         right_seq_length.append(len(inp[2]))
    #
    #     # 0-padding
    #     for verb in verb_input:
    #         # print(len(verb))
    #         # verb.append([convert_number_to_one_hot_vector(0, self.char_vocab_size)] * (self.max_verb_sequence_length - len(verb)))
    #         verb.extend([convert_number_to_one_hot_vector(0, self.char_vocab_size)] * (
    #             self.max_verb_sequence_length - len(verb)))
    #     for left_word in left_input:
    #         left_word.extend([0] * (self.max_context_sequence_length - len(left_word)))
    #     for right_word in right_input:
    #         right_word.extend([0] * (self.max_context_sequence_length - len(right_word)))
    #
    #     verb_data = np.array(verb_input)
    #     verb_data_seq_length = np.array(verb_seq_length)
    #     left_context_data = np.array(left_input)
    #     left_context_data_seq_length = np.array(left_seq_length)
    #     right_context_data = np.array(right_input)
    #     right_context_data_seq_length = np.array(right_seq_length)
    #     verbs = np.array(self.input_verbs)
    #
    #     self.verb_data_batches = np.array_split(verb_data, self.num_batches)
    #     self.verb_seq_length_batches = np.array_split(verb_data_seq_length, self.num_batches)
    #     self.left_context_data_batches = np.array_split(left_context_data, self.num_batches)
    #     self.left_context_seq_length_batches = np.array_split(left_context_data_seq_length, self.num_batches)
    #     self.right_context_data_batches = np.array_split(right_context_data, self.num_batches)
    #     self.right_context_seq_length_batches = np.array_split(right_context_data_seq_length, self.num_batches)
    #     self.input_verbs = np.array_split(verbs, self.num_batches)
    #
    # def next_batch(self):
    #     verb_data_batch = self.verb_data_batches[self.pointer]
    #     verb_seq_len_batch = self.verb_seq_length_batches[self.pointer]
    #     left_context_data_batch = self.left_context_data_batches[self.pointer]
    #     left_context_seq_length_batch = self.left_context_seq_length_batches[self.pointer]
    #     right_context_data_batch = self.right_context_data_batches[self.pointer]
    #     right_context_seq_length_batch = self.right_context_seq_length_batches[self.pointer]
    #     verbs_batch = self.input_verbs[self.pointer]
    #     self.pointer += 1
    #
    #     return verb_data_batch, verb_seq_len_batch, left_context_data_batch, left_context_seq_length_batch, \
    #         right_context_data_batch, right_context_seq_length_batch, verbs_batch
    #
    # def reset_batch_pointer(self):
    #     self.pointer = 0


# TODO USED
def convert_input_and_target(input_lines, target_lines, word_to_id, character_to_id,
                             max_context_sequence_length, max_verb_sequence_length):

    print("Converting input")
    logging.info("Converting input")
    input_data_correct_format, verbs, targets = reformat_input(input_lines, target_lines, max_context_sequence_length,
                                                               max_verb_sequence_length)
    train_data, _ = convert_input_data_to_ids(input_data_correct_format, verbs, word_to_id, character_to_id,
                                              max_context_sequence_length, max_verb_sequence_length)

    print("Converting input targets")
    logging.info("Converting input targets")
    train_answers_data = convert_target_data_to_probability_output(targets)
    # train_answers_data = convert_target_file_to_probability_output(target_lines)
    # print(len(train_answers_data))

    return train_data, train_answers_data


# TODO USED
def shuffle_two_lists(list1, list2):
    combined_list = list(zip(list1, list2))
    random.shuffle(combined_list)
    list1, list2 = zip(*combined_list)
    return list1, list2


# TODO USED
# A memory-friendly iterator of a text with sentences used for creating the embedding
class MySentences(object):
    def __init__(self, data_path):
        self.data_path = data_path

    def __iter__(self):
        for line in open(self.data_path):
            yield line.lower().split()


# def _build_embedding_from_list(sentences, embedding_file, size=100, min_count=1, workers=4, sg=1):
#     model = gensim.models.Word2Vec(sentences, size=size, min_count=min_count, workers=workers, sg=sg)
#     model.save(embedding_file)
#     return model


# TODO USED
def _build_embedding_from_file(filename, embedding_file, size=100, min_count=1, workers=4, sg=1):
    sentences = MySentences(filename)  # a memory-friendly iterator
    model = gensim.models.Word2Vec(sentences, size=size, min_count=min_count, workers=workers, sg=sg)
    model.save(embedding_file)
    return model


# TODO USED
def _load_embedding(embedding_file):
    model = gensim.models.Word2Vec.load(embedding_file)
    return model


# def _build_embedding_vocab(filename, embedding_filename):
#
#     words = embedding_repr.get_embedding_sorted_vocab()
#     word_to_id = dict(zip(words, range(1, len(words) + 1)))
#     print(words)
#     return word_to_id


# TODO USED
def _replace_infrequent_words_text(filename, converted_filename, min_count):
    data = []
    with open(filename) as f1:
        input_lines = f1.read().splitlines()
        # input_lines = [x.lower() for x in input_lines]
        for line_number in range(len(input_lines)):
            tags_sentence = tag_input_sentence(input_lines[line_number])
            for w in range(len(tags_sentence)):
                data.append(tags_sentence[w].word.lower())

    counter = collections.Counter(data)
    d = dict(counter)
    nb_not_frequent_items = 0
    # TODO occurence boundary relatief maken tot grootte van tekst?
    # min_count = 2
    for key, value in d.items():
        if value < min_count:
            nb_not_frequent_items += 1
    d = {k: v for k, v in d.items() if v >= min_count}
    d[unknown_token] = nb_not_frequent_items
    count_pairs = sorted(d.items(), key=lambda x: (-x[1], x[0]))

    words, _ = list(zip(*count_pairs))
    word_to_id = dict(zip(words, range(1, len(words) + 1)))

    with open(converted_filename, "w") as out_f:
        for line_number in range(len(input_lines)):
            new_line = replace_unknown_words_sentence(input_lines[line_number], unknown_token, word_to_id)
            # input_lines[line_number] = new_line
            out_f.write(new_line + '\n')

    return input_lines


# TODO USED
def replace_unknown_words_sentence(sentence, replacement, vocab):
    word_list = sentence.split()
    for i in range(len(word_list)):
        if word_list[i].lower() not in vocab:
            word_list[i] = replacement
    sentence = ' '.join(word_list)
    return sentence


# TODO USED
# filename1 is een tekst, filename2 is een lijst met de juiste werkwoorden
def _build_character_vocab(filename1):
    # data = _read_words(filename)
    text = _read_file(filename1)
    sentences = tokenizer.tokenize(text)
    data = []
    for sentence in sentences:
        # TODO Not with word tokenize, but with treetagger words
        new_words = word_tokenize(sentence, 'dutch')
        for w in new_words:
            data.append(w)
    for i in range(len(data)):
        data[i] = data[i].lower()
    data2 = "".join(data)

    new_data = list(data2)

    counter = collections.Counter(new_data)
    count_pairs = sorted(counter.items(), key=lambda x: (-x[1], x[0]))

    characters, _ = list(zip(*count_pairs))
    character_to_id = dict(zip(characters, range(len(characters))))

    return character_to_id


# TODO USED
def _read_file(filename):
    with tf.gfile.GFile(filename, "r") as f:
        return f.read().decode("utf-8")


# TODO USED
def convert_input_data_to_ids(raw_inputs_train_data, verbs, word_to_id, character_to_id, max_context_sequence_length,
                              max_verb_sequence_length):
    print("Convert input file to ids")
    logging.info("Convert input file to ids")
    correct_input = []
    number_characters = len(character_to_id)
    # print("len raw_inputs_train_data)")
    # print(len(raw_inputs_train_data))
    count = 0
    for converted_input in raw_inputs_train_data:
        count += 1
        new_correct_input = []
        verb_input = []
        verb = converted_input[0]

        for char_i in range(0, len(verb)):
            one_hot_verb_char = convert_number_to_one_hot_vector(character_to_id[verb[char_i].lower()],
                                                                 number_characters)
            verb_input.append(one_hot_verb_char)
        new_correct_input.append(verb_input)

        for context_i in range(1, len(converted_input)):
            context = converted_input[context_i]
            context_input = []
            for word_index in range(0, len(context)):
                context_input.append(get_voc_index_word(word_to_id, context[word_index]))
                # context_input.append(word_to_id[context[word_index]])
            new_correct_input.append(context_input)
        correct_input.append(new_correct_input)
        if count % 10000 == 0:
            print(count)
            logging.info(count)
    return correct_input, verbs


# TODO USED
def get_voc_index_word(word_to_id, word):
    word = word.lower()
    if word in word_to_id:
        return word_to_id[word]
    else:
        return word_to_id[unknown_token]


# def convert_to_input_format(input_lines, target_lines, max_context_sequence_length, max_verb_sequence_length):
#     inputs = []
#     verbs = []
#     targets = []
#     for target_line in target_lines:
#         for target_couple in range(len(target_line)):
#
#     return inputs, verbs, targets

# def convert_target_file_to_ids(filename, word_to_id, char_to_id):
#     correct_verbs = _read_words(filename)
#     targets = []
#     for verb in correct_verbs:
#         new_target = verb.lower()
#         target_index = get_voc_index_word(word_to_id, new_target)
#         chars_target = list("".join(new_target))
#         chars_index_target = []
#         for char in chars_target:
#             chars_index_target.append(char_to_id[char])
#         targets.append([target_index, chars_index_target])
#     return targets


# TODO
class AdaptRule(Enum):
    DO_NOTHING = 1
    REM_T = 2
    REM_T_ADD_DT = 3
    REM_T_ADD_D = 4
    ADD_T = 5
    REM_DDE_ADD_DE = 6
    REM_DDEN_ADD_DEN = 7
    REM_DE_ADD_DDE = 8
    REM_DEN_ADD_DDEN = 9
    REM_TTE_ADD_TE = 10
    REM_TTEN_ADD_TEN = 11
    REM_TE_ADD_TTE = 12
    REM_TEN_ADD_TTEN = 13


# TODO USED
def convert_number_to_one_hot_vector(n, max):
    lst = []
    if n == 0:
        lst = [0] * max
    elif n > 0:
        lst = [0] * (n - 1) + [1] + [0] * (max - n)
    # print(lst)
    return lst


# TODO USED
def convert_target_data_to_probability_output(target_lines):
    # with open(filename) as f:
    #     lines = f.read().splitlines()
    # print(lines)
    # TODO hier is lengte van total cases aantal regels van dt-fouten die we beschouwen
    # total_cases = len(AdaptRule)
    total_cases = 5
    targets = []
    # print(target_lines)
    for line in target_lines:
        # print(line)
        # temp = line.split()
        # print(temp)
        for i in range(0, len(line), 2):
            wrong_verb = line[i]
            correct_verb = line[i + 1]
            adapt_rule = convert_wrong_and_correct_verb_to_adapt_rule(wrong_verb, correct_verb)
            target = convert_number_to_one_hot_vector(adapt_rule.value, total_cases)
            targets.append(target)
            # print(targets)
    return targets


# TODO USED
def convert_wrong_and_correct_verb_to_adapt_rule(wrong_verb, correct_verb):
    adapt_rule = None
    if wrong_verb == correct_verb:
        adapt_rule = AdaptRule.DO_NOTHING
    elif wrong_verb.endswith("dt") and correct_verb.endswith("d"):
        adapt_rule = AdaptRule.REM_T
    elif wrong_verb.endswith("t") and correct_verb.endswith("dt"):
        adapt_rule = AdaptRule.REM_T_ADD_DT
    elif wrong_verb.endswith("t") and correct_verb.endswith("d"):
        adapt_rule = AdaptRule.REM_T_ADD_D
    elif wrong_verb.endswith("d") and correct_verb.endswith("dt"):
        adapt_rule = AdaptRule.ADD_T
    elif wrong_verb.endswith("dde") and correct_verb.endswith("de"):
        adapt_rule = AdaptRule.REM_DDE_ADD_DE
    elif wrong_verb.endswith("dden") and correct_verb.endswith("den"):
        adapt_rule = AdaptRule.REM_DDEN_ADD_DEN
    elif wrong_verb.endswith("de") and correct_verb.endswith("dde"):
        adapt_rule = AdaptRule.REM_DE_ADD_DDE
    elif wrong_verb.endswith("den") and correct_verb.endswith("dden"):
        adapt_rule = AdaptRule.REM_DEN_ADD_DDEN
    elif wrong_verb.endswith("tte") and correct_verb.endswith("te"):
        adapt_rule = AdaptRule.REM_TTE_ADD_TE
    elif wrong_verb.endswith("tten") and correct_verb.endswith("ten"):
        adapt_rule = AdaptRule.REM_TTEN_ADD_TEN
    elif wrong_verb.endswith("te") and correct_verb.endswith("tte"):
        adapt_rule = AdaptRule.REM_TE_ADD_TTE
    elif wrong_verb.endswith("ten") and correct_verb.endswith("tten"):
        adapt_rule = AdaptRule.REM_TEN_ADD_TTEN
    return adapt_rule


# TODO used
def apply_predicted_rule(prediction, verb):
    adapt_rule = AdaptRule(prediction).name
    new_verb = None
    if adapt_rule == AdaptRule.DO_NOTHING.name:
        new_verb = verb
    elif adapt_rule == AdaptRule.REM_T.name:
        new_verb = verb[:-1]
    elif adapt_rule == AdaptRule.REM_T_ADD_DT.name:
        new_verb = verb[:-1] + 'dt'
    elif adapt_rule == AdaptRule.REM_T_ADD_D.name:
        new_verb = verb[:-1] + 'd'
    elif adapt_rule == AdaptRule.ADD_T.name:
        new_verb = verb + 't'
    elif adapt_rule == AdaptRule.REM_DDE_ADD_DE.name:
        new_verb = verb[:-3] + 'de'
    elif adapt_rule == AdaptRule.REM_DDEN_ADD_DEN.name:
        new_verb = verb[:-4] + 'den'
    elif adapt_rule == AdaptRule.REM_DE_ADD_DDE.name:
        new_verb = verb[:-2] + 'dde'
    elif adapt_rule == AdaptRule.REM_DEN_ADD_DDEN.name:
        new_verb = verb[:-3] + 'dden'
    elif adapt_rule == AdaptRule.REM_TTE_ADD_TE.name:
        new_verb = verb[:-3] + 'te'
    elif adapt_rule == AdaptRule.REM_TTEN_ADD_TEN.name:
        new_verb = verb[:-4] + 'ten'
    elif adapt_rule == AdaptRule.REM_TE_ADD_TTE.name:
        new_verb = verb[:-2] + 'tte'
    elif adapt_rule == AdaptRule.REM_TEN_ADD_TTEN.name:
        new_verb = verb[:-3] + 'tten'
    return new_verb


# TODO USED
# Tag one sentence
def tag_input_sentence(sentence):
    tags = tagger.tag_text(sentence)
    tags2 = treetaggerwrapper.make_tags(tags, exclude_nottags=True)
    return tags2


# TODO USED
def reformat_input(list_input_lines, list_target_lines, max_context_sequence_length, max_verb_sequence_length):
    """Function to reformat the given input.

    Transforms each sentence into a predefined format.
    The format is

    Args:
        list_input_lines (list): List with all the input lines.
        list_target_lines (list): List with the same order as the list_input_lines with for every input line all its
            target pairs (a target pair is the used verb in the sentence and the correct verb).
        max_context_sequence_length (int): The maximum length of words a sentence can have at the left or right of verb
            (this is considered as the context)
        max_verb_sequence_length (int): The maximum length of characters a verb can have.

    Returns:
        reformatted_inputs (list): List of sentences with every sentence in the correct format.
        verbs (list): List of verbs.
        targets (list): List of targets.

    Example:
        Input:
            list_input_lines = ["Het wordt mooi weer .", "Ik antwoord correct ."]
            list_target_lines = ["wordt wordt", "antwoord antwoord"]
            max_context_sequence_length = 20
            max_verb_sequence_length = 20
        Output:
            ([[['w', 'o', 'r', 'd', 't'], ['Het'], ['.', 'weer', 'mooi']],
              [['a', 'n', 't', 'w', 'o', 'o', 'r', 'd'], ['Ik'], ['.', 'correct']]],
             ['wordt', 'antwoord'],
             [['wordt', 'wordt'], ['antwoord', 'antwoord']])
    """
    print("Create raw inputs")
    logging.info("Create raw inputs")
    inputs = []
    verbs = []
    targets = []
    # print("len list_input_lines)")
    # print(len(list_input_lines))
    count = 0
    for sentence in range(len(list_input_lines)):
        # print(list_input_lines[sentence])
        count += 1
        # print(list_target_lines[sentence])
        new_inputs, new_verbs, new_targets = convert_sentence_to_input_format(list_input_lines[sentence], list_target_lines[sentence], max_context_sequence_length, max_verb_sequence_length)
        for i in range(len(new_verbs)):
            inputs.append(new_inputs[i])
            verbs.append(new_verbs[i])
            targets.append(new_targets[i])
        if count % 10000 == 0:
            print(count)
            logging.info(count)
    print("End raw inputs")
    logging.info("End raw inputs")
    # print(inputs)
    return inputs, verbs, targets




# Converteert één zin in de juiste volgorde.
#
#    Args:
#       sentence: de zin die geconverteerd moet worden
#       max_context_sequence_length: de maximale lengte van een context in woorden (links of rechts)
#       max_verb_length: de maximale werkwoordslengte in karakters
#
#   Returns:
#       Lijst met een element voor elk gevonden werkwoord in de zin.
#       Elk element is zelf een lijst met 3 elementen
#           werkwoord: lijst met de karakters van het werkwoord
#           linkse context: lijst met de woorden voor het werkwoord in de zin van links naar rechts
#           rechtse context: lijst met de woorden achter het werkwoord in de zin van rechts naar links.
#
#   Voorbeeld:
#       Input: Dit is een zin.
#       Returns: [["i", "s"], ["dit"], [".", "zin", "een"]]
def convert_sentence_to_inputs(sentence, max_context_sequence_length, max_verb_length):
    # TODO lower
    sentence = sentence.lower()
    tags_sentence = tag_input_sentence(sentence)
    created_inputs = []
    verbs = []
    sentence_length = len(tags_sentence)

    for i in range(sentence_length):
        if not type(tags_sentence[i]).__name__ == "NotTag":
            # ...\t2__\... betekent dat getagde woord een werkwoordsvorm is
            # Als we werkwoord op characterniveau gaan behandelen
            if tags_sentence[i].pos.startswith('2'):
                verb = tags_sentence[i].word
                left_context_len = i
                right_context_len = len(tags_sentence) - (i + 1)
                if len(verb) <= max_verb_length and left_context_len <= max_context_sequence_length \
                        and right_context_len <= max_context_sequence_length:
                    if verb.endswith('d') or verb.endswith('dt'):
                        verbs.append(verb)
                        converted_verb = list(verb)
                        left_context = []
                        right_context = []
                        for j in range(i):
                            left_context.append(tags_sentence[j].word)
                        for k in range(len(tags_sentence) - 1, i, -1):
                            right_context.append(tags_sentence[k].word)
                        result = [converted_verb, left_context, right_context]
                        created_inputs.append(result)

    return created_inputs, verbs


# TODO USED
def convert_sentence_to_input_format(sentence, list_targets, max_context_sequence_length, max_verb_length):
    """
    Function to reformat a sentence into the predefined input format.

    Transforms a sentence into a predefined format.
    The format is [list_character_verbs, list_words_left_context, list_words_right_context_reversed_order]
    The transformation of a sentence in the correct format can result in multiple results. Every verb detected is
    considered, and if it is a possible d/t-fault, a result for this verb is formed.

    Args:
        sentence (str):
        list_targets ():
        max_context_sequence_length (int): The maximum length of words a sentence can have at the left or right of verb
            (this is considered as the context)
        max_verb_length (int): The maximum length of characters a verb can have.

    Returns:
        reformatted_sentence (list<list<list<str>>>): List with lists of the sentence. A list of a sentence contains
            three lists. The first one with the characters of the verb concentrated on, the second one with the words
            of the left context, the third with the words of the right context.
        verbs (list): List of verbs used.
        targets (list<list<str>>): List with lists of target pairs. A target pair is the wrong (verb in sentence) and
            the correct verb

    Example:
        Input:
            list_input_lines = "Het wordt mooi weer ."
            list_target_lines = ["wordt wordt"]
            max_context_sequence_length = 20
            max_verb_sequence_length = 20
        Output:
            ([[['w', 'o', 'r', 'd', 't'], ['Het'], ['.', 'weer', 'mooi']]],
             ['wordt'],
             [['wordt', 'wordt']])
    """
    sentence = sentence
    tags_sentence = tag_input_sentence(sentence)
    list_targets = list_targets.split()
    created_inputs = []
    verbs = []
    sentence_length = len(tags_sentence)
    number_of_verbs = 0
    created_targets = []
    # print(sentence)
    # pprint.pprint(tags_sentence)
    # print(list_targets)
    for i in range(sentence_length):
        if not type(tags_sentence[i]).__name__ == "NotTag":
            # ...\t2__\... means the tagged word is a verb
            if hasattr(tags_sentence[i], 'pos'):
                pos_tag = tags_sentence[i].pos
                # print(tags_sentence[i].word)
                if pos_tag.startswith('2') and len(pos_tag) >= 3:
                    test_present_time = 4 <= int(pos_tag[1]) <= 8 and 1 <= int(pos_tag[2]) <= 3
                    test_past_time = 0 <= int(pos_tag[1]) <= 3 and 6 <= int(pos_tag[2]) <= 9
                    if test_present_time or test_past_time:
                        verb = tags_sentence[i].word
                        left_context_len = i
                        right_context_len = len(tags_sentence) - (i + 1)
                        if verb.endswith('d') or verb.endswith('dt'):
                            if verb in list_targets[number_of_verbs::2]:
                                if len(verb) <= max_verb_length and left_context_len <= max_context_sequence_length \
                                        and right_context_len <= max_context_sequence_length:
                                    verbs.append(verb)
                                    converted_verb = list(verb)
                                    left_context = []
                                    right_context = []
                                    for j in range(i):
                                        left_context.append(tags_sentence[j].word)
                                    for k in range(len(tags_sentence) - 1, i, -1):
                                        right_context.append(tags_sentence[k].word)
                                    result = [converted_verb, left_context, right_context]
                                    created_inputs.append(result)
                                    # print(list_targets)
                                    # print(number_of_verbs)
                                    target = [list_targets[number_of_verbs * 2], list_targets[number_of_verbs * 2 + 1]]
                                    created_targets.append(target)
                                number_of_verbs += 1
    return created_inputs, verbs, created_targets





# Not used methods
    # In TrainingsTextloader
    # def build_vocabs(self, input_file, target_file, word_vocab_file, char_vocab_file):
    #     self.word_to_id = _build_word_vocab(input_file, target_file)
    #     self.character_to_id = _build_character_vocab(input_file, target_file)
    #     self.words, _ = zip(*self.word_to_id.items())
    #     self.word_vocab_size = len(self.words)
    #     self.chars, _ = zip(*self.character_to_id.items())
    #     self.char_vocab_size = len(self.chars)
    #     with open(word_vocab_file, 'wb') as f:
    #         cPickle.dump(self.words, f)
    #     with open(char_vocab_file, 'wb') as f:
    #         cPickle.dump(self.chars, f)

# def adapt_text_to_voc(input_file, converted_input_file, word_vocab_file):
#     with open(input_file) as in_f, open(converted_input_file, "w") as out_f:
#         input_lines = in_f.read().splitlines()
#         input_lines = [x.lower() for x in input_lines]
#         for line in input_lines:
#             new_line = ""
#             tags_sentence = tag_input_sentence(line)
#             for i in range(len(tags_sentence)):
#                 if type(tags_sentence[i]).__name__ == "NotTag":
#                     word = "<unk>"
#                 elif not tags_sentence[i].word in word_vocab_file:
#                     word = "<unk>"
#                 else:
#                     word = tags_sentence[i].word
#                 new_line += word + " "
#             out_f.write(new_line + '\n')
#
#
# # filename1 is een tekst, filename2 is een lijst met de juiste werkwoorden
# def _build_word_vocab(filename1, filename2):
#     # data = _read_words(filename)
#
#
#     # text = _read_file(filename1)
#     # text = text.lower()
#     # sentences = tokenizer.tokenize(text)
#     data = []
#     with open(filename1) as f1:
#         input_lines = f1.read().splitlines()
#         input_lines = [x.lower() for x in input_lines]
#         for line in input_lines:
#             # for sentence in sentences:
#             #         regex = re.compile(r"(.*?)<(.*?)>(.*?)")
#             #         replacement = "\\1" + "\\2" + "\\3"
#             # sentence = regex.sub(replacement, sentence)
#             # line = regex.sub(replacement, line)
#             # new_words = word_tokenize(sentence, 'dutch')
#             tags_sentence = tag_input_sentence(line)
#             for w in range(len(tags_sentence)):
#                 data.append(tags_sentence[w].word)
#
#                 # for w in new_words:
#                 #     data.append(w)
#     # verbs = _read_words(filename2)
#     # for verb in verbs:
#     #     new_verb = verb.lower()
#     #     data.append(new_verb)
#     counter = collections.Counter(data)
#     # print("a")
#     # print(counter)
#     d = dict(counter)
#     nb_not_frequent_items = 0
#     # TODO occurence boundary relatief maken tot grootte van tekst?
#     occurence_boundary = 1
#     for key, value in d.items():
#         if value < occurence_boundary:
#             nb_not_frequent_items += 1
#     d = {k: v for k, v in d.items() if v >= occurence_boundary}
#     d['<unk>'] = nb_not_frequent_items
#     count_pairs = sorted(d.items(), key=lambda x: (-x[1], x[0]))
#     # print("b")
#     # print(count_pairs)
#
#     words, _ = list(zip(*count_pairs))
#     # print("c")
#     # print(words)
#     word_to_id = dict(zip(words, range(1, len(words) + 1)))
#     # print("d")
#     # print(word_to_id)
#
#     return word_to_id

# def _read_sentences(filename):
#     # Geeft misschien problemen met leestekens
#     with tf.gfile.GFile(filename, "r") as f:
#         return f.read().decode("utf-8").replace(".\n", ".").replace("\n", "")
#
#
# def _read_words(filename):
#     with tf.gfile.GFile(filename, "r") as f:
#         # return f.read().decode("utf-8").replace("\n", "<eos>").split()
#         # Geeft misschien problemen met afkortingen, en andere leestekens
#         return f.read().decode("utf-8").replace(". ", " . ").replace(".\n", " . ").split()
#
#
# def convert_input_file_to_ids(list_input_lines, word_to_id, character_to_id, max_context_sequence_length,
#                               max_verb_sequence_length):
#     print("Convert input file to ids")
#     logging.info("Convert input file to ids")
#     raw_inputs_train_data, verbs = create_raw_inputs(list_input_lines, max_context_sequence_length,
#                                                      max_verb_sequence_length)
#     correct_input = []
#     number_characters = len(character_to_id)
#     # print("len raw_inputs_train_data)")
#     # print(len(raw_inputs_train_data))
#     count = 0
#     for converted_input in raw_inputs_train_data:
#         count += 1
#         new_correct_input = []
#         verb_input = []
#         verb = converted_input[0]
#
#         for char_i in range(0, len(verb)):
#             one_hot_verb_char = convert_number_to_one_hot_vector(character_to_id[verb[char_i]], number_characters)
#             verb_input.append(one_hot_verb_char)
#         new_correct_input.append(verb_input)
#
#         for context_i in range(1, len(converted_input)):
#             context = converted_input[context_i]
#             context_input = []
#             for word_index in range(0, len(context)):
#                 context_input.append(get_voc_index_word(word_to_id, context[word_index]))
#                 # context_input.append(word_to_id[context[word_index]])
#             new_correct_input.append(context_input)
#         correct_input.append(new_correct_input)
#         if count % 10000 == 0:
#             print(count)
#             logging.info(count)
#     return correct_input, verbs
#
# def convert_target_file_to_probability_output(target_lines):
#     # with open(filename) as f:
#     #     lines = f.read().splitlines()
#     # print(lines)
#     # TODO hier is lengte van total cases aantal regels van dt-fouten die we beschouwen
#     # total_cases = len(AdaptRule)
#     total_cases = 5
#     targets = []
#     # print(target_lines)
#     for line in target_lines:
#         # print(line)
#         temp = line.split()
#         # print(temp)
#         for i in range(0, len(temp), 2):
#             wrong_verb = temp[i]
#             correct_verb = temp[i + 1]
#             adapt_rule = convert_wrong_and_correct_verb_to_adapt_rule(wrong_verb, correct_verb)
#             target = convert_number_to_one_hot_vector(adapt_rule.value, total_cases)
#             targets.append(target)
#             # print(targets)
#     return targets
#
#
# # Misschien niet ideaal om dat hier al te doen, want nu worden er enorm grote lijsten doorgegeven.
# def convert_verb_index_to_one_hot_vector(character_to_id, character):
#     vocab_size = len(character_to_id)
#     id = character_to_id[character]
#     vector = [0] * vocab_size
#     vector[id] = 1
#     return vector
#
#
# def _file_to_word_ids(filename, word_to_id):
#     # data = _read_words(filename)
#     text = _read_file(filename)
#     sentences = tokenizer.tokenize(text)
#     data = []
#     for sentence in sentences:
#         try:
#             correct_words = re.search(">(.*)<", sentence).group(1)
#         except AttributeError:
#             correct_words = " "
#         new_words = word_tokenize(sentence, 'dutch')
#         new_words.append(correct_words)
#         for w in new_words:
#             data.append(w)
#
#     return [word_to_id[word] for word in data if word in word_to_id]
#
#
# # Tag a whole file
# def tag_input_text(datapath):
#     tags = tagger.tag_file(datapath)
#     tags2 = treetaggerwrapper.make_tags(tags)
#     return tags2


# # Converteert de zinnen van de tekst in de juiste inputvolgorde.
# #
# #   Args:
# #     Sentence: de zin die geconverteerd moet worden
# #
# #   Returns:
# #     Lijst met een element voor elk gevonden werkwoord in de zin.
# #     Elk element is zelf een lijst met 3 elzementen (werkwoord, linkse context, rechtse context)
# #     linkse context: lijst met de woorden voor het werkwoord in de zin van links naar rechts
# #     rechtse context: lijst met de woorden achter het werkwoord in de zin van rechts naar links.
# def create_raw_inputs_with_dt_faults(input_file, target_file, max_context_sequence_length, max_verb_sequence_length):
#     # text = _read_sentences(data_path)
#     text = _read_file(input_file)
#     text = text.lower()
#     sentences = tokenizer.tokenize(text)
#     inputs = []
#     max_sentence_length = 2*max_context_sequence_length + 1
#     with open(target_file, 'w') as output_answer_file:
#         for sentence in sentences:
#
#             # # TODO Filteren zodat enkel ww met t of d op einde? (ovt gaat dan wel niet)
#             new_inputs, verbs = convert_sentence_to_inputs_with_creating_dt_fault(sentence, max_sentence_length, max_verb_sequence_length)
#             for i in range(len(verbs)):
#                 output_answer_file.write(verbs[i][0] + " " + verbs[i][1] + '\n')
#                 inputs.append(new_inputs[i])
#
#
#     return inputs


# # Converteert de zinnen van een tekst naar de juiste inputvolgorde.
# #
# #   Args:
# #       input_file: de input file met een tekst
# #       max_context_sequence_length: de maximale lengte van een context in woorden (links of rechts)
# #       max_verb_length: de maximale werkwoordslengte in karakters
# #
# #   Returns:
# #       Lijst met een element voor elk gevonden werkwoord in een zin voor alle zinnen in de input file.
# #       Elk element is zelf een lijst met 3 elementen
# #           werkwoord: lijst met de karakters van het werkwoord
# #           linkse context: lijst met de woorden voor het werkwoord in de zin van links naar rechts
# #           rechtse context: lijst met de woorden achter het werkwoord in de zin van rechts naar links.
# #
# #   Example:
# #       Input: Dit is een zin. En dit is ook een zin.
# #       Returns: [[["i", "s"], ["dit"], [".", "zin", "een"]], [["i", "s"], ["en", dit"], [".", "zin", "een", "ook"]]]
# def create_raw_inputs(list_input_lines, max_context_sequence_length, max_verb_sequence_length):
#     print("Create raw inputs")
#     logging.info("Create raw inputs")
#     # text = _read_sentences(data_path)
#     # text = _read_file(list_input_lines)
#     # text = text.lower()
#     # sentences = tokenizer.tokenize(text)sentences
#     inputs = []
#     verbs = []
#     # print("len list_input_lines)")
#     # print(len(list_input_lines))
#     count = 0
#     for sentence in list_input_lines:
#         # print(sentence)
#         count += 1
#         new_inputs, new_verbs = convert_sentence_to_inputs(sentence, max_context_sequence_length,
#                                                            max_verb_sequence_length)
#         for i in range(len(new_verbs)):
#             inputs.append(new_inputs[i])
#             verbs.append(new_verbs[i])
#         if count % 10000 == 0:
#             print(count)
#             logging.info(count)
#     print("End raw inputs")
#     logging.info("End raw inputs")
#     # print(inputs)
#     return inputs, verbs



# # Converteert één zin in de juiste volgorde.
# # Voorbeeld: Input: Ik ben een student.
# #            Returns: [[b, e, n][Ik][., student, een]
# #
# #    Args:
# #     Sentence: de zin die geconverteerd moet worden
# #
# #   Returns:
# #     Lijst met een element voor elk gevonden werwoord in de zin.
# #     Elk element is zelf een lijst met 3 elementen (werkwoord, linkse context, rechtse context)
# #     linkse context: lijst met de woorden voor het werkwoord in de zin van links naar rechts
# #     rechtse context: lijst met de woorden achter het werkwoord in de zin van rechts naar links.
# def convert_sentence_to_inputs_with_creating_dt_fault(sentence, max_sentence_length, max_verb_length):
#     tags_sentence = tag_input_sentence(sentence)
#     created_inputs = []
#     verbs = []
#     words = word_tokenize(sentence, 'dutch')
#     # print("tags1")
#     # print(tags_sentence)
#     # print("tags2")
#     sentence_length = len(tags_sentence)
#
#     # Zorg ervoor dat zin niet langer is dan max_context_length * 2 + 1
#     if sentence_length <= max_sentence_length:
#
#         for i in range(sentence_length):
#             # print(tags_sentence[i])
#             if not type(tags_sentence[i]).__name__ == "NotTag":
#                 if tags_sentence[i].pos.startswith('2'):
#                     # ...\t2__\... betekent dat getagde woord een werkwoordsvorm is
#                     # Als we werkwoord op charcterniveau gaan behandelen
#                     # verb = words[i]
#                     # print("words " + verb)
#                     verb = tags_sentence[i].word
#                     # print("tagger " + verb)
#                     # Als we werkwoord niet op characternieveau gaan bekijken
#                     # verb = words[i]
#
#                     # Zorg ervoor dat zin niet langer is dan max_context_length * 2 + 1
#                     if len(verb) <= max_verb_length:
#
#                         if verb.endswith('d') or verb.endswith('dt'):
#                             # new_verb = create_possible_dt_fault(verb)
#                             verbs.append([new_verb, verb])
#                             converted_verb = list(new_verb)
#                             left_context = []
#                             right_context = []
#                             for j in range(i):
#                                 # left_context.append(words[j])
#                                 left_context.append(tags_sentence[j].word)
#                             # for k in range(len(words)-1, i, -1):
#                             #     right_context.append(words[k])
#                             for k in range(len(tags_sentence) - 1, i, -1):
#                                 right_context.append(tags_sentence[k].word)
#                             result = [converted_verb, left_context, right_context]
#                             created_inputs.append(result)
#
#     return created_inputs, verbs


# def create_possible_dt_fault(verb):
#     bad_verb = ''
#     rand_perc = random.random()
#     if verb.endswith('d'):
#         if rand_perc <= (1 / 3):
#             bad_verb = verb + 't'
#         elif (1 / 3) < rand_perc <= (2 / 3):
#             bad_verb = verb[:-1] + 't'
#         else:
#             bad_verb = verb
#     elif verb.endswith('dt'):
#         if rand_perc <= (1 / 3):
#             bad_verb = verb[:-1]
#         elif (1 / 3) < rand_perc <= (2 / 3):
#             bad_verb = verb[:-2] + 't'
#         else:
#             bad_verb = verb
#     return bad_verb


# print(_read_sentences("/home/yakke/Bureaublad/corrector.train.txt"))
#
# a = _build_word_vocab("/home/yakke/Bureaublad/corrector.train.txt", "/home/yakke/Bureaublad/corrector.train_answers.txt")
# print(_file_to_word_ids("/home/yakke/Bureaublad/corrector.train.txt", a))
#
# b = _build_character_vocab("/home/yakke/Bureaublad/corrector.train.txt")
#
# print((convert_target_file_to_probability_output("/home/yakke/Bureaublad/preprocessed_data/Test/targetfiletest.txt")))
#
# print(convert_sentence_to_inputs("Dit wordt geantwoord ."))
# input_file = os.path.join("/home/yakke/Bureaublad/preprocessed_data/Test/", "corrector.train.txt")
# target_file = os.path.join("/home/yakke/Bureaublad/preprocessed_data/Test/", "corrector.train_answers.txt")
# print("----")
# print(create_raw_inputs(input_file, target_file))
#
# with open("/home/yakke/Bureaublad/preprocessed_data/Test/vocab_words.pkl", 'rb') as f:
#     words = cPickle.load(f)
# word_vocab_size = len(words)
# word_to_id = dict(zip(words, range(len(words))))
#
# TrainingTextLoader("/home/yakke/Bureaublad/preprocessed_data/Test", 10, 20, 30, 1000, 2)
#
# print(reformat_input(["Het wordt mooi weer .", "Ik antwoord word correct ."], ["wordt wordt", "antwoord antwoord word word"], 20, 20))







