import tensorflow as tf
import datetime
import argparse
import os
import pickle
from dt_corrector import CorrectorModel
import logging
import Utils
import math
import numpy as np

# logging.basicConfig(filename='dt_Corrector_train.log', filemode="w", level=logging.INFO)

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

fh = logging.FileHandler('dt_Corrector_train.log', mode='a')
fh.setLevel(logging.INFO)

logger.addHandler(fh)


def main():
    logger.info(" ")
    logger.info("----------START TRAINING----------")
    logger.info(" ")
    parser = argparse.ArgumentParser()
    parser.add_argument('--data_dir', type=str, default='/home/yakke/Bureaublad/preprocessed_data/',
                        help='data directory containing input.txt')
    # parser.add_argument('--save_dir', type=str, default='/home/yakke/Bureaublad/preprocessed_data/save/',
    #                     help='directory to store checkpointed models')
    parser.add_argument('--rnn_size', type=int, default=128,
                        help='size of RNN hidden state')
    parser.add_argument('--num_layers', type=int, default=2,
                        help='number of layers in the RNN')
    parser.add_argument('--batch_size', type=int, default=100,
                        help='minibatch size')
    # TODO number of epochs = 10 zal normaal volstaan -> Kijken met validatieset wnr fout niet meer naar beneden gaat
    parser.add_argument('--num_epochs', type=int, default=10,
                        help='number of epochs')
    parser.add_argument('--min_count_words', type=int, default=30,
                        help='replace words by unknown token if less frequent')
    parser.add_argument('--embedding_size', type=int, default=100,
                        help='size of embedding')
    parser.add_argument('--no_max_evaluation', action='store_false', default=True,
                        help='bool deciding if maximum lengths are used for evaluation')
    parser.add_argument('--max_length_verb', type=int, default=20,
                        help='maximum length of verbs taken into account')
    parser.add_argument('--max_length_context', type=int, default=25,
                        help='maximum length of left or right part taken into account')
    parser.add_argument('--init_from', type=str, default=None,
                        help="""continue training from saved model at this path. Path must contain files saved by previous training process:
                            'config.pkl'        : configuration;
                            'vocab_chars.pkl'   : vocabulary definitions;
                            'vocab_words.pkl'   : vocabulary definitions;
                            'checkpoint'        : paths to model file(s) (created by tf).
                                                  Note: this file contains absolute paths, be careful when moving files around;
                            'model.ckpt-*'      : file(s) with model definition (created by tf)
                        """)
    args = parser.parse_args()
    train(args)


def train(args):
    save_dir = os.path.join(args.data_dir, 'save/')
    if not os.path.isdir(save_dir):
        os.makedirs(save_dir)
    start_preprocessing = datetime.datetime.now()
    data_loader = Utils.TrainingPreProcessor(args.data_dir, save_dir, args.batch_size, args.no_max_evaluation,
                                             args.max_length_verb, args.max_length_context, args.min_count_words,
                                             embedding_size=args.embedding_size)
    stop_preprocessing = datetime.datetime.now()
    print("Time preprocessing = {}".format(stop_preprocessing - start_preprocessing))
    logger.info("Time preprocessing = {}".format(stop_preprocessing - start_preprocessing))
    # data_loader2 = input_reader.TrainingTextLoader(args.data_dir, args.batch_size, args.max_length_verb,
    #                                                args.max_length_context, args.data_size, args.min_count_words,
    #                                                embedding_size=args.embedding_size)

    # Gebruiken ik niet. Ik ga model nooit verder trainen. Anders moet ik ook bijhouden wel stuk van trainingstekst wel
    # al gebruikt is en moet ik dus pointer ook onthouden.
    # check compatibility if training is continued from previously saved model
    if args.init_from is not None:
        # check if all necessary files exist
        assert os.path.isdir(args.init_from), " %s must be a a path" % args.init_from
        assert os.path.isfile(
            os.path.join(args.init_from, "config.pkl")), "config.pkl file does not exist in path %s" % args.init_from
        assert os.path.isfile(os.path.join(args.init_from,
                                           "vocab_words.pkl")), "vocab_words.pkl file does not exist in path %s" \
                                                                % args.init_from
        assert os.path.isfile(os.path.join(args.init_from,
                                           "vocab_chars.pkl")), "vocab_chars.pkl file does not exist in path %s" \
                                                                % args.init_from
        ckpt = tf.train.get_checkpoint_state(args.init_from)
        assert ckpt, "No checkpoint found"
        assert ckpt.model_checkpoint_path, "No model path found in checkpoint"

        # open old config and check if models are compatible
        with open(os.path.join(args.init_from, 'config.pkl'), 'rb') as f:
            saved_model_args = pickle.load(f)
        need_be_same = ["model", "rnn_size", "num_layers", "batch_size", "min_count_words", "data_size",
                        "max_length_verb", "max_length_context"]
        for checkme in need_be_same:
            assert vars(saved_model_args)[checkme] == vars(args)[
                checkme], "Command line argument and saved model disagree on '%s' " % checkme

        # open saved vocab/dict and check if vocabs/dicts are compatible
        with open(os.path.join(args.init_from, 'vocab_chars.pkl'), 'rb') as f:
            saved_chars, saved_chars_vocab = pickle.load(f)
        with open(os.path.join(args.init_from, 'vocab_words.pkl'), 'rb') as f:
            saved_words, saved_words_vocab = pickle.load(f)
        # assert saved_chars == data_loader.chars, "Data and loaded model disagree on character set!"
        assert saved_chars_vocab == data_loader.character_to_id, "Data and loaded model disagree on " \
                                                                 "dictionary mappings characters!"
        # assert saved_words == data_loader.words, "Data and loaded model disagree on word set!"
        assert saved_words_vocab == data_loader.word_to_id, "Data and loaded model disagree on " \
                                                            "dictionary mappings words!"

    with open(os.path.join(save_dir, 'config.pkl'), 'wb') as f:
        pickle.dump(args, f)
    # with open(os.path.join(save_dir, 'chars_vocab.pkl'), 'wb') as f:
    #     pickle.dump((data_loader.chars, data_loader.character_to_id), f)
    # with open(os.path.join(save_dir, 'words_vocab.pkl'), 'wb') as f:
    #     pickle.dump((data_loader.words, data_loader.word_to_id), f)

    model = CorrectorModel(data_loader.word_vocab_size, data_loader.char_vocab_size, args.embedding_size,
                           args.batch_size, args.rnn_size, args.num_layers, data_loader.max_context_sequence_length,
                           data_loader.max_verb_sequence_length)
    learned_embedding = data_loader.get_embedding_model_repr()
    start_model_training_time = datetime.datetime.now()

    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        print("Variables initialized")
        logger.info("Variables initialized")
        sess.run(tf.assign(model.embedding_words, learned_embedding))
        print("Embedding initialized")
        logger.info("Embedding initialized")
        # Currently only one model will be kept, so when saving the previous saved model will be deleted.
        saver = tf.train.Saver(tf.global_variables(), max_to_keep=1)
        checkpoint_path = os.path.join(save_dir, 'model.ckpt')
        previous_average_valid_loss = math.inf
        overfitting = False
        # restore model
        if args.init_from is not None:
            saver.restore(sess, ckpt.model_checkpoint_path)
        print("Start training")
        logger.info("Start training")
        start_training = datetime.datetime.now()
        for e in range(args.num_epochs):
            if overfitting:
                break
            else:

                train_verb_sentence_generator = \
                    Utils.read_sentences_generator_numbers(data_loader.train_verb_tensor_file)
                train_left_context_sentence_generator = \
                    Utils.read_sentences_generator_numbers(data_loader.train_left_context_tensor_file)
                train_right_context_sentence_generator = \
                    Utils.read_sentences_generator_numbers(data_loader.train_right_context_tensor_file)
                train_answers_sentence_generator = \
                    Utils.read_sentences_generator_numbers(data_loader.train_answers_tensor_file)
                validation_verb_sentence_generator = \
                    Utils.read_sentences_generator_numbers(data_loader.validation_verb_tensor_file)
                validation_left_context_sentence_generator = \
                    Utils.read_sentences_generator_numbers(data_loader.validation_left_context_tensor_file)
                validation_right_context_sentence_generator = \
                    Utils.read_sentences_generator_numbers(data_loader.validation_right_context_tensor_file)
                validation_answers_sentence_generator = \
                    Utils.read_sentences_generator_numbers(data_loader.validation_answers_tensor_file)

                sum_train_losses = 0
                start_training_epoch = datetime.datetime.now()
                for b in range(data_loader.num_batches_train):
                    # print("Batch " + str(b))
                    verb_data_batch, verb_seq_len_batch, \
                        left_context_data_batch, left_context_seq_length_batch, \
                        right_context_data_batch, right_context_seq_length_batch, \
                        target_batch = data_loader.generate_batch(train_verb_sentence_generator,
                                                                  train_left_context_sentence_generator,
                                                                  train_right_context_sentence_generator,
                                                                  train_answers_sentence_generator)

                    # print("Batch generated")
                    feed = {model.input_data_verb: verb_data_batch,
                            model.seq_length_verb: verb_seq_len_batch,
                            model.input_data_left_context: left_context_data_batch,
                            model.seq_length_left_context: left_context_seq_length_batch,
                            model.input_data_right_context: right_context_data_batch,
                            model.seq_length_right_context: right_context_seq_length_batch,
                            model.target: target_batch}

                    # print("Run")
                    train_loss, _ = sess.run([model.cost, model.train_op], feed)
                    # print("Run OK")
                    # print("{}/{} (epoch {}), train_loss = {:.3f}, time/batch = {:.3f}"
                    #       .format(e * data_loader.num_batches_train + b + 1,
                    #               args.num_epochs * data_loader.num_batches_train,
                    #               e + 1, train_loss, end - start))
                    # logger.info("{}/{} (epoch {}), train_loss = {:.3f}, time/batch = {:.3f}"
                    #              .format(e * data_loader.num_batches_train + b + 1,
                    #                      args.num_epochs * data_loader.num_batches_train,
                    #                      e + 1, train_loss, end - start))

                    sum_train_losses += train_loss

                stop_training_epoch = datetime.datetime.now()
                average_train_loss = sum_train_losses / data_loader.num_batches_train
                print("Epoch {}, Average train_loss = {:.5f}, Time trainings epoch = {}"
                      .format(e, average_train_loss, (stop_training_epoch - start_training_epoch)))
                logger.info("Epoch {}, Average train_loss = {:.5f}, Time trainings epoch = {}"
                            .format(e, average_train_loss, (stop_training_epoch - start_training_epoch)))

                start_validation_time = datetime.datetime.now()
                sum_valid_losses = 0
                nb_most_probable_predictions = 0
                nb_correct_predictions = 0
                nb_total_cases = 0
                nb_correct_change_predictions = 0
                nb_false_change_predictions = 0
                for b in range(data_loader.num_batches_validation):
                    # Let model run on validation set.
                    valid_verbs, valid_verb_seq_lens, \
                        valid_left_context_data, valid_left_context_seq_lengths, \
                        valid_right_context_data, valid_right_context_seq_lengths, \
                        valid_targets = data_loader.generate_batch(validation_verb_sentence_generator,
                                                                   validation_left_context_sentence_generator,
                                                                   validation_right_context_sentence_generator,
                                                                   validation_answers_sentence_generator)

                    feed = {model.input_data_verb: valid_verbs,
                            model.seq_length_verb: valid_verb_seq_lens,
                            model.input_data_left_context: valid_left_context_data,
                            model.seq_length_left_context: valid_left_context_seq_lengths,
                            model.input_data_right_context: valid_right_context_data,
                            model.seq_length_right_context: valid_right_context_seq_lengths,
                            model.target: valid_targets}

                    new_valid_loss, validation_predictions = sess.run([model.cost, model.prediction], feed)
                    predicted_rules = np.argmax(validation_predictions, axis=1)
                    true_rules = np.argmax(valid_targets, axis=1)

                    # print("Valid_loss = {:.3f}, time/batch = {:.3f}".format(new_valid_loss, end - start))
                    # logger.info("Valid_loss = {:.3f}, time/batch = {:.3f}".format(new_valid_loss, end - start))
                    sum_valid_losses += new_valid_loss

                    for prediction, correct_rule, i in \
                            zip(predicted_rules, true_rules, range(len(predicted_rules))):
                        nb_total_cases += 1
                        if correct_rule == Utils.AdaptRule.DO_NOTHING.value:
                            nb_most_probable_predictions += 1
                        if prediction == correct_rule:
                            nb_correct_predictions += 1
                        if prediction == correct_rule and not correct_rule == Utils.AdaptRule.DO_NOTHING.value:
                            nb_correct_change_predictions += 1
                        if not prediction == correct_rule and not prediction == Utils.AdaptRule.DO_NOTHING.value:
                            nb_false_change_predictions += 1

                stop_validation_time = datetime.datetime.now()
                average_valid_loss = sum_valid_losses/data_loader.num_batches_validation

                print("Epoch {}, Average valid_loss = {:.5f}, Time validation = {}"
                      .format(e, average_valid_loss, (stop_validation_time - start_validation_time)))
                logger.info("Epoch {}, Average valid_loss = {:.5f}, Time validation = {}"
                            .format(e, average_valid_loss, (stop_validation_time - start_validation_time)))

                print("Total cases = {}".format(nb_total_cases))
                logger.info("Total cases = {}".format(nb_total_cases))

                print("Number most probable predictions = {}".format(nb_most_probable_predictions))
                logger.info("Number most probable predictions = {}".format(nb_most_probable_predictions))

                print("Number correct predictions = {}".format(nb_correct_predictions))
                logger.info("Number correct predictions = {}".format(nb_correct_predictions))

                print("Number correct change predictions = {}".format(nb_correct_change_predictions))
                logger.info("Number correct change predictions = {}".format(nb_correct_change_predictions))

                print("Number correct no change predictions = {}".format(
                    nb_correct_predictions - nb_correct_change_predictions))
                logger.info("Number correct no change predictions = {}".format(
                    nb_correct_predictions - nb_correct_change_predictions))

                print("Number false predictions = {}".format(nb_total_cases - nb_correct_predictions))
                logger.info("Number false predictions = {}".format(nb_total_cases - nb_correct_predictions))

                print("Number false change predictions = {}".format(nb_false_change_predictions))
                logger.info("Number false change predictions = {}".format(nb_false_change_predictions))

                print("Number false no change predictions = {}".format(
                    nb_total_cases - nb_correct_predictions - nb_false_change_predictions))
                logger.info("Number false no change predictions = {}".format(
                    nb_total_cases - nb_correct_predictions - nb_false_change_predictions))

                accuracy_majority = nb_most_probable_predictions / nb_total_cases
                print("Accuracy majority class = {:.5f}".format(accuracy_majority))
                logger.info("Accuracy majority class = {:.5f}".format(accuracy_majority))

                accuracy_model = nb_correct_predictions / nb_total_cases
                print("Accuracy trained model = {:.5f}".format(accuracy_model))
                logger.info("Accuracy trained model = {:.5f}".format(accuracy_model))

                # Check loss on validation set. If loss is higher than previous loss, overfitting has taken place.
                if average_valid_loss > previous_average_valid_loss:
                    # New model is worse than previous model, so don't save it.
                    # Set parameter to prevent further training and associated overfitting.
                    overfitting = True
                    print("Best model was model of previous epoch (Epoch {}. Model saved to {}"
                          .format(e-1, checkpoint_path))
                    logger.info("Best model was model of previous epoch (Epoch {}. Model saved to {}"
                                .format(e-1, checkpoint_path))
                else:
                    # Save model as new best model.
                    saver.save(sess, checkpoint_path, global_step=e)
                    previous_average_valid_loss = average_valid_loss

        stop_training = datetime.datetime.now()
        print("Time training = {}, Number of epochs = {}".format((stop_training - start_training), e))
        logger.info("Time training = {}, Number of epochs = {}".format((stop_training - start_training), e))

        # test_verb_sentence_generator = Utils.read_sentences_generator_numbers(data_loader.test_verb_tensor_file)
        # test_left_context_sentence_generator = \
        #     Utils.read_sentences_generator_numbers(data_loader.test_left_context_tensor_file)
        # test_right_context_sentence_generator = \
        #     Utils.read_sentences_generator_numbers(data_loader.test_right_context_tensor_file)
        # test_answers_sentence_generator = Utils.read_sentences_generator_numbers(data_loader.test_answers_tensor_file)
        #
        # sum_test_losses = 0
        # start_testing = datetime.datetime.now()
        # for b in range(data_loader.num_batches_test):
        #     # Let model run on test set.
        #     test_verbs, test_verb_seq_lens, \
        #         test_left_context_data, test_left_context_seq_lengths, \
        #         test_right_context_data, test_right_context_seq_lengths, \
        #         test_targets = data_loader.generate_batch(test_verb_sentence_generator,
        #                                                   test_left_context_sentence_generator,
        #                                                   test_right_context_sentence_generator,
        #                                                   test_answers_sentence_generator)
        #
        #     feed = {model.input_data_verb: test_verbs,
        #             model.seq_length_verb: test_verb_seq_lens,
        #             model.input_data_left_context: test_left_context_data,
        #             model.seq_length_left_context: test_left_context_seq_lengths,
        #             model.input_data_right_context: test_right_context_data,
        #             model.seq_length_right_context: test_right_context_seq_lengths,
        #             model.target: test_targets}
        #
        #     test_loss = sess.run(model.cost, feed)
        #     sum_test_losses += test_loss
        #
        # average_test_loss = sum_test_losses / data_loader.num_batches_test
        # stop_testing = datetime.datetime.now()
        # print("Average test_loss = {:.5f}, Time testing = {}"
        #       .format(average_test_loss, (stop_testing - start_testing)))
        # logger.info("Average test_loss = {:.5f}, Time testing = {}"
        #             .format(average_test_loss, (stop_testing - start_testing)))

    stop_model_training_time = datetime.datetime.now()
    print("Time training model = {}".format(stop_model_training_time - start_model_training_time))
    logger.info("Time training model = {}".format(stop_model_training_time - start_model_training_time))

    logger.info(" ")
    logger.info("----------STOP TRAINING----------")
    logger.info(" ")

if __name__ == '__main__':
    main()
