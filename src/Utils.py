import os
import treetaggerwrapper
import collections
import datetime
import gensim
import logging
import random
from enum import Enum
import numpy as np
import pickle
import math

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
# logger.basicConfig(filename='dt_Corrector_Utils.log', filemode="w", level=logging.INFO)

fh = logging.FileHandler('dt_Corrector_Utils.log', mode='a')
fh.setLevel(logging.INFO)

logger.addHandler(fh)

tagger = treetaggerwrapper.TreeTagger(TAGLANG='nl')

unknown_token = "unknown"
unknown_character = "|"
start_sentence_token = "þ"


class TrainingPreProcessor:
    """This class preprocess texts (which is divided into a training, a validation and a data set)."""

    def __init__(self, train_path, train_save_path, batch_size, max_evaluation, max_verb_sequence_length,
                 max_context_sequence_length, min_appearances_word, embedding_size=100):
        logger.info(" ")
        logger.info("----------START TRAINING PREPROCESSING----------")
        logger.info(" ")

        self.batch_size = batch_size
        self.embedding_size = embedding_size

        if max_evaluation:
            self._max_evaluation = max_evaluation
            self.max_verb_sequence_length = math.inf
            self.max_context_sequence_length = math.inf
        else:
            self._max_evaluation = False
            self.max_verb_sequence_length = max_verb_sequence_length
            self.max_context_sequence_length = max_context_sequence_length

        train_original_file = os.path.join(train_path, "train_original.txt")
        train_file = os.path.join(train_path, "train.txt")
        self.train_verb_tensor_file = os.path.join(train_path, "train_verb_input_tensor.txt")
        self.train_left_context_tensor_file = os.path.join(train_path, "train_left_context_input_tensor.txt")
        self.train_right_context_tensor_file = os.path.join(train_path, "train_right_context_input_tensor.txt")
        train_answers_file = os.path.join(train_path, "train_answers.txt")
        self.train_answers_tensor_file = os.path.join(train_path, "train_output_tensor.txt")
        train_line_number_file = os.path.join(train_path, "train_line_number_file.txt")
        train_verb_file = os.path.join(train_path, "train_verb_input.txt")
        train_verb_times_file = os.path.join(train_path, "train_verb_times_file.txt")

        validation_original_file = os.path.join(train_path, "validation_original.txt")
        validation_file = os.path.join(train_path, "validation.txt")
        self.validation_verb_tensor_file = os.path.join(train_path, "validation_verb_input_tensor.txt")
        self.validation_left_context_tensor_file = os.path.join(train_path, "validation_left_context_input_tensor.txt")
        self.validation_right_context_tensor_file = os.path.join(train_path,
                                                                 "validation_right_context_input_tensor.txt")
        validation_answers_file = os.path.join(train_path, "validation_answers.txt")
        self.validation_answers_tensor_file = os.path.join(train_path, "validation_output_tensor.txt")
        validation_line_number_file = os.path.join(train_path, "validation_line_number_file.txt")
        validation_verb_file = os.path.join(train_path, "validation_verb_input.txt")
        validation_verb_times_file = os.path.join(train_path, "validation_verb_times_file.txt")

        word_vocab_file = os.path.join(train_save_path, "vocab_words.pkl")
        char_vocab_file = os.path.join(train_save_path, "vocab_chars.pkl")
        embedding_file = os.path.join(train_save_path, "embedding")

        max_length_file = os.path.join(train_path, "max_lengths")
        if max_evaluation:
            if os.path.isfile(max_length_file):
                with open(max_length_file, "r", encoding='utf-8') as f:
                    lines = f.read().splitlines()
                    self.max_verb_sequence_length = int(lines[0])
                    self.max_context_sequence_length = int(lines[1])

        # test_original_file = os.path.join(test_path, "test_original.txt")
        # temp_test_path, _ = os.path.split(test_path)
        # _, distribution = os.path.split(temp_test_path)
        # test_file = os.path.join(train_path, "test_" + distribution + ".txt")
        # self.test_verb_tensor_file = os.path.join(train_path, "test_verb_" + distribution + "_input_tensor.txt")
        # self.test_left_context_tensor_file = os.path.join(train_path, "test_left_context_" + distribution +
        #                                                   "_input_tensor.txt")
        # self.test_right_context_tensor_file = os.path.join(train_path, "test_right_context_" + distribution +
        #                                                    "_input_tensor.txt")
        # test_answers_file = os.path.join(test_path, "test_answers.txt")
        # self.test_answers_tensor_file = os.path.join(train_path, "test_" + distribution + "_output_tensor.txt")
        # self.test_line_number_file = os.path.join(train_path, "test_" + distribution + "_line_number_file.txt")
        # self.test_verb_file = os.path.join(train_path, "test_" + distribution + "_verb_input.txt")
        # self.test_verb_times_file = os.path.join(train_path, "test_" + distribution + "_verb_times_file.txt")

        # Edit (replace infrequent words by the unknown token and/or add start sentence token)
        # the input files if not already done.
        if not os.path.exists(train_file):
            start = datetime.datetime.now()
            print("Edit input files: Replace infrequent words by unknown token.")
            logger.info("Edit input files: Replace infrequent words by unknown token.")
            _replace_infrequent_words_text(train_original_file, train_file, min_appearances_word)
            _add_start_tokens_text(validation_original_file, validation_file)
            # _replace_infrequent_words_text(validation_original_file, validation_file, min_appearances_word)
            # _replace_infrequent_words_text(test_original_file, test_file, min_appearances_word)
            end = datetime.datetime.now()
            duration = end - start
            print("Time duration: {}".format(duration))
            logger.info("Time duration: {}".format(duration))
        # Make/load embedding
        if not (os.path.exists(embedding_file)):
            # If embedding file does not exist, build a new embedding.
            print("Building embedding")
            logger.info("Building embedding")
            start = datetime.datetime.now()
            self._embedding = _build_embedding_from_file(train_file, embedding_file, size=embedding_size,
                                                         min_count=min_appearances_word)
            end = datetime.datetime.now()
            duration = end - start
            print("Time duration: {}".format(duration))
            logger.info("Time duration: {}".format(duration))
        else:
            # If embedding file does exist, load it.
            print("Loading embedding")
            logger.info("Loading embedding")
            start = datetime.datetime.now()
            self._embedding = _load_embedding(embedding_file)
            end = datetime.datetime.now()
            duration = end - start
            print("Time duration: {}".format(duration))
            logger.info("Time duration: {}".format(duration))

        # Build/load word and character vocab files.
        if not (os.path.exists(word_vocab_file) and os.path.exists(char_vocab_file)):
            # If vocab files don't exist, build vocab files.
            print("Building vocabs.")
            logger.info("Building vocabs.")
            start = datetime.datetime.now()
            self._build_vocabs(train_file, word_vocab_file, char_vocab_file)
            end = datetime.datetime.now()
            duration = end - start
            print("Time duration: {}".format(duration))
            logger.info("Time duration: {}".format(duration))
        else:
            # If vocab files exist, load them.
            print("Loading word and character vocab.")
            logger.info("Loading word and character vocab.")
            start = datetime.datetime.now()
            self._load_vocabs(word_vocab_file, char_vocab_file)
            end = datetime.datetime.now()
            duration = end - start
            print("Time duration: {}".format(duration))
            logger.info("Time duration: {}".format(duration))

        # print(self.character_to_id)
        # print(self.word_to_id)
        # Create/load correct preprocessed files from training and validation files.
        if not (os.path.exists(self.train_verb_tensor_file) and os.path.exists(self.train_left_context_tensor_file) and
                os.path.exists(self.train_right_context_tensor_file) and
                os.path.exists(self.train_answers_tensor_file) and
                os.path.exists(self.validation_verb_tensor_file) and
                os.path.exists(self.validation_left_context_tensor_file) and
                os.path.exists(self.validation_right_context_tensor_file) and
                os.path.exists(self.validation_answers_tensor_file)):
            # Preprocess the training and validation files
            print("Preprocessing training, trainings answers, validation and validation answers files.")
            logger.info("Preprocessing training, trainings answers, validation and validation answers files.")
            start = datetime.datetime.now()
            self._preprocess(train_file, train_answers_file, self.train_verb_tensor_file,
                             self.train_left_context_tensor_file, self.train_right_context_tensor_file,
                             self.train_answers_tensor_file, train_line_number_file, train_verb_times_file,
                             train_verb_file, max_length_file)
            self._preprocess(validation_file, validation_answers_file, self.validation_verb_tensor_file,
                             self.validation_left_context_tensor_file, self.validation_right_context_tensor_file,
                             self.validation_answers_tensor_file, validation_line_number_file,
                             validation_verb_times_file, validation_verb_file, max_length_file)
            end = datetime.datetime.now()
            duration = end - start
            print("Time duration: {}".format(duration))
            logger.info("Time duration: {}".format(duration))

        # # Create/load correct preprocessed files from test files.
        # if not (os.path.exists(self.test_verb_tensor_file) and os.path.exists(self.test_left_context_tensor_file) and
        #         os.path.exists(self.test_right_context_tensor_file) and os.path.exists(self.test_answers_tensor_file)):
        #     # Preprocess the test files
        #     print("Preprocessing test and test answers files.")
        #     logger.info("Preprocessing test and test answers files.")
        #     start = datetime.datetime.now()
        #     self._preprocess(test_file, test_answers_file, self.test_verb_tensor_file,
        #                      self.test_left_context_tensor_file, self.test_right_context_tensor_file,
        #                      self.test_answers_tensor_file, self.test_line_number_file, self.test_verb_times_file,
        #                      self.test_verb_file)
        #     end = datetime.datetime.now()
        #     duration = end - start
        #     print("Time duration: {}".format(duration))
        #     logger.info("Time duration: {}".format(duration))

        with open(self.train_verb_tensor_file, 'r', encoding='utf-8') as f:
            i = 0
            for i, _ in enumerate(f):
                i += 1
                pass
            data_len = i
            self.num_batches_train = math.ceil(data_len / self.batch_size)
            print('Num Batches train: ' + str(self.num_batches_train))
            logger.info('Num Batches train: ' + str(self.num_batches_train))

        with open(self.validation_verb_tensor_file, 'r', encoding='utf-8') as f:
            i = 0
            for i, _ in enumerate(f):
                i += 1
                pass
            data_len = i
            self.num_batches_validation = math.ceil(data_len / self.batch_size)
            print('Num Batches validation: ' + str(self.num_batches_validation))
            logger.info('Num Batches validation: ' + str(self.num_batches_validation))

        # with open(self.test_verb_tensor_file, 'r', encoding='utf-8') as f:
        #     i = 0
        #     for i, _ in enumerate(f):
        #         i += 1
        #         pass
        #     data_len = i
        #     self.num_batches_test = math.ceil(data_len / self.batch_size)
        #     print('Num Batches test: ' + str(self.num_batches_test))

        # # Only take the amount of data specified
        # self._train_data = self._train_data[:data_size]
        # self._train_answers_data = self._train_answers_data[:data_size]

        logger.info(" ")
        logger.info("----------STOP TRAINING PREPROCESSING----------")
        logger.info(" ")

    def _build_vocabs(self, input_file, word_vocab_file, char_vocab_file):
        """Function to build the vocabs (word vocab and character vocab).

        Gets the words vocab and builds the character vocab.
        The words, characters and size of the vocabs are stored in object variables.
        The words and characters are also save in files.

        Args:
            input_file (str): The file used to build the vocabs.
            word_vocab_file (str): The file where the words are stored.
            word_vocab_file (str): The file where the characters are stored.

        Returns:
            None
        """
        self.word_to_id = self._get_embedding_sorted_word_to_id()
        self.character_to_id = _build_character_vocab(input_file)
        self.words, _ = zip(*self.word_to_id.items())
        self.word_vocab_size = len(self.words)
        self.chars, _ = zip(*self.character_to_id.items())
        self.char_vocab_size = len(self.chars)
        # with open(word_vocab_file, 'wb') as f:
        #     pickle.dump(self.words, f)
        # with open(char_vocab_file, 'wb') as f:
        #     pickle.dump(self.chars, f)
        with open(word_vocab_file, 'wb') as f:
            pickle.dump(self.word_to_id, f)
        with open(char_vocab_file, 'wb') as f:
            pickle.dump(self.character_to_id, f)

    def _load_vocabs(self, word_vocab_file, char_vocab_file):
        """Function to load created vocabs from vocab files."""
        # with open(word_vocab_file, 'rb') as f:
        #     self.words = pickle.load(f)
        # self.word_vocab_size = len(self.words)
        # self.word_to_id = dict(zip(self.words, range(len(self.words))))
        # with open(char_vocab_file, 'rb') as f:
        #     self.chars = pickle.load(f)
        # self.char_vocab_size = len(self.chars)
        # self.character_to_id = dict(zip(self.chars, range(len(self.chars))))
        with open(word_vocab_file, 'rb') as f:
            self.word_to_id = pickle.load(f)
        self.word_vocab_size = len(self.word_to_id)
        with open(char_vocab_file, 'rb') as f:
            self.character_to_id = pickle.load(f)
        self.char_vocab_size = len(self.character_to_id)

    def _get_embedding_sorted_vocab(self):
        """Function to get the sorted list of words in the vocab used in the embedding.

        Gets the sorted list of words in the vocab, from most used to less used, from the embedding.
        The result depends on the vocab used in the embedding, so also on the parameters used to build the embedding.

        Returns:
            Sorted list of words.
        """
        return self._embedding.wv.index2word

    def _get_embedding_sorted_word_to_id(self):
        """Function to get a sorted dictionary of words in the vocab used in the embedding."""
        words = self._embedding.wv.index2word
        word_to_id = dict(zip(words, range(len(words))))
        return word_to_id

    def get_embedding_model_repr(self):
        """Function to get the representation of the embedding."""
        return self._embedding.wv.syn0

    # def _preprocess(self, input_file, target_file, input_tensor_file, output_tensor_file):
    #     """Function to preprocess an input and target file in the correct format for the dt_Corrector."""
    #     with open(input_file, 'r', encoding='utf-8') as in_f:
    #         input_lines = in_f.read().splitlines()
    #     with open(target_file, 'r', encoding='utf-8') as out_f:
    #         target_lines = out_f.read().splitlines()
    #     # Gedaan in clean_and_shuffle.py op voorhand.
    #     # # Delete empty strings
    #     # input_lines = list(filter(None, input_lines))
    #     # target_lines = list(filter(None, target_lines))
    #     # # Shuffle both lists (relation between input and target is kept)
    #     # input_lines, target_lines = shuffle_two_lists(input_lines, target_lines)
    #
    #     # Convert the given data to the right format and id's
    #     converted_input_data, converted_target_data = convert_input_and_target(input_lines, target_lines,
    #                                                                            self.word_to_id,
    #                                                                            self.character_to_id,
    #                                                                            self.max_context_sequence_length,
    #                                                                            self.max_verb_sequence_length)
    #     # Save the created data to the given file names
    #     np.save(input_tensor_file, converted_input_data)
    #     np.save(output_tensor_file, converted_target_data)
    #     return converted_input_data, converted_target_data

    def _preprocess(self, input_file, target_file, verb_input_tensor_file, left_context_input_tensor_file,
                    right_context_input_tensor_file, target_input_tensor_file, line_number_file, verb_times_file,
                    verb_file, max_length_file):
        """Function to preprocess an input and target file in the correct format for the dt_Corrector."""
        input_sentence_generator = read_sentences_generator_text(input_file)
        target_sentence_generator = read_sentences_generator_text(target_file)

        line_number = 0
        print("Preprocessing.")
        logger.info("Preprocessing.")

        max_verb_length_found = 0
        max_context_length_found = 0

        for input_sentence, target_sentence in zip(input_sentence_generator, target_sentence_generator):
            # Reformat inputs and targets
            # print(input_sentence)
            # print(target_sentence)
            sentence_verbs, sentence_verbs_times, sentence_left_contexts, sentence_right_contexts, \
                sentence_targets, number_of_inputs = reformat_sentence(input_sentence, target_sentence,
                                                                       self.max_verb_sequence_length,
                                                                       self.max_context_sequence_length)
            # print(sentence_verbs)
            # print(sentence_left_contexts)
            # print(sentence_targets)
            # print(number_of_inputs)
            # Converting inputs in correct input format to ids in correct input format
            new_verb_input, new_left_context_input, new_right_context_input = \
                convert_correct_formatted_input_to_ids(sentence_verbs, sentence_left_contexts, sentence_right_contexts,
                                                       number_of_inputs, self.word_to_id, self.character_to_id)
            # Converting targets in correct target format to ids in correct target format
            new_target_input = convert_target_data_to_adaptation_rule_value(sentence_targets, number_of_inputs)

            # Save the created data to the given file names
            # if not new_target_input:
            #     save_reformatted_sentence(verb_input_tensor_file)
            #     save_reformatted_sentence(left_context_input_tensor_file)
            #     save_reformatted_sentence(right_context_input_tensor_file)
            #     save_int(target_input_tensor_file)
            #     save_int(line_number, line_number_file)
            # else:
            for new_verb_line, new_left_context_line, new_right_context_line, new_target_line, new_sentence_verb,\
                new_verb_time, in zip(new_verb_input, new_left_context_input, new_right_context_input,
                                      new_target_input, sentence_verbs, sentence_verbs_times):
                save_reformatted_sentence(new_verb_line, verb_input_tensor_file)
                verb_length = len(new_verb_line)
                if verb_length > max_verb_length_found:
                    max_verb_length_found = verb_length
                save_reformatted_sentence(new_left_context_line, left_context_input_tensor_file)
                context_length = len(new_left_context_line)
                if context_length > max_context_length_found:
                    max_context_length_found = context_length
                save_reformatted_sentence(new_right_context_line, right_context_input_tensor_file)
                context_length = len(new_right_context_line)
                if context_length > max_context_length_found:
                    max_context_length_found = context_length
                save_int(new_target_line, target_input_tensor_file)
                save_int(line_number, line_number_file)
                verb = ''.join(new_sentence_verb)
                save_word(verb, verb_file)
                save_int(new_verb_time, verb_times_file)

            line_number += 1

        if self._max_evaluation:
            self.max_verb_sequence_length = max_verb_length_found
            self.max_context_sequence_length = max_context_length_found
            with open(max_length_file, "w", encoding='utf-8') as f:
                f.write(str(self.max_verb_sequence_length) + '\n')
                f.write(str(self.max_context_sequence_length))

        print("Max verb length: " + str(self.max_verb_sequence_length))
        logger.info("Max verb length: " + str(self.max_verb_sequence_length))
        print("Max context length: " + str(self.max_context_sequence_length))
        logger.info("Max context length: " + str(self.max_context_sequence_length))
        print("Preprocessing done.")
        logger.info("Preprocessing done.")

    @staticmethod
    def _load_preprocessed(input_tensor_file, target_tensor_file):
        """Function to load an input and target tensor file."""
        converted_input_data = np.load(input_tensor_file)
        converted_target_data = np.load(target_tensor_file)
        return converted_input_data, converted_target_data

    def generate_batch(self, verb_sentence_generator, left_context_sentence_generator,
                       right_context_sentence_generator, answers_sentence_generator):
        batch_length = 0
        verb_data_batch = []
        verb_seq_length_batch = []
        left_context_data_batch = []
        left_context_seq_length_batch = []
        right_context_data_batch = []
        right_context_seq_length_batch = []
        target_batch = []
        # Total different adaptation rules
        total_cases = len(AdaptRule)

        try:
            while batch_length < self.batch_size:
                verb_input = []
                verb_id_input = next(verb_sentence_generator)
                for char_id in verb_id_input:
                    one_hot_verb_char = convert_number_to_one_hot_vector(char_id, len(self.character_to_id))
                    verb_input.append(one_hot_verb_char)
                verb_seq_length = len(verb_input)
                left_input = next(left_context_sentence_generator)
                left_seq_length = len(left_input)
                right_input = next(right_context_sentence_generator)
                right_seq_length = len(right_input)
                target_case = next(answers_sentence_generator)
                target_input = convert_number_to_one_hot_vector(target_case[0], total_cases)

                # 0-padding
                verb_input.extend([convert_number_to_one_hot_vector(0, self.char_vocab_size)] *
                                  (self.max_verb_sequence_length - len(verb_input)))
                left_input.extend([0] * (self.max_context_sequence_length - len(left_input)))
                right_input.extend([0] * (self.max_context_sequence_length - len(right_input)))

                verb_data_batch.append(verb_input)
                verb_seq_length_batch.append(verb_seq_length)
                left_context_data_batch.append(left_input)
                left_context_seq_length_batch.append(left_seq_length)
                right_context_data_batch.append(right_input)
                right_context_seq_length_batch.append(right_seq_length)
                target_batch.append(target_input)
                batch_length += 1

        except StopIteration:
            pass

        return verb_data_batch, verb_seq_length_batch, left_context_data_batch, left_context_seq_length_batch, \
            right_context_data_batch, right_context_seq_length_batch, target_batch


class EvaluatePreProcessor:
    """This class preprocess texts with solutions (only test set)."""

    def __init__(self, test_path, model_path, batch_size, max_evaluation, max_verb_sequence_length,
                 max_context_sequence_length,):

        logger.info(" ")
        logger.info("----------START EVALUATING PREPROCESSING----------")
        logger.info(" ")
        # print(max_evaluation)
        # print(max_context_sequence_length)
        self.batch_size = batch_size
        if max_evaluation:
            self._max_evaluation = max_evaluation
            self.max_verb_sequence_length = math.inf
            self.max_context_sequence_length = math.inf
        else:
            self._max_evaluation = False
            self.max_verb_sequence_length = max_verb_sequence_length
            self.max_context_sequence_length = max_context_sequence_length

        word_vocab_file = os.path.join(model_path, "vocab_words.pkl")
        char_vocab_file = os.path.join(model_path, "vocab_chars.pkl")
        embedding_file = os.path.join(model_path, "embedding")

        self.test_original_file = os.path.join(test_path, "test_original.txt")

        temp_test_path, _ = os.path.split(test_path)
        _, distribution = os.path.split(temp_test_path)
        temp_model_path, _ = os.path.split(model_path)
        temp_model_path, _ = os.path.split(temp_model_path)
        # if temp_model_path == test_path:
        #     save_dir = temp_model_path
        # else:
        first_save_dir = os.path.join(temp_model_path, "test_" + distribution + "/")
        if not os.path.isdir(first_save_dir):
            os.makedirs(first_save_dir)
        if max_evaluation:
            save_dir = os.path.join(first_save_dir, "maxverbmaximum_maxcontextmaximum/")
        else:
            save_dir = os.path.join(first_save_dir, "maxverb_" + str(max_verb_sequence_length) + "_maxcontext_" +
                                    str(max_context_sequence_length) + "/")
        if not os.path.isdir(save_dir):
            os.makedirs(save_dir)
        max_length_file = os.path.join(save_dir, "max_lengths")
        if max_evaluation:
            if os.path.isfile(max_length_file):
                with open(max_length_file, "r", encoding='utf-8') as f:
                    lines = f.read().splitlines()
                    self.max_verb_sequence_length = int(lines[0])
                    self.max_context_sequence_length = int(lines[1])

        self.test_file = os.path.join(test_path, "test_" + distribution + ".txt")
        self.test_verb_tensor_file = os.path.join(save_dir, "test_verb_" + distribution + "_input_tensor.txt")
        self.test_left_context_tensor_file = os.path.join(save_dir, "test_left_context_" + distribution +
                                                          "_input_tensor.txt")
        self.test_right_context_tensor_file = os.path.join(save_dir, "test_right_context_" + distribution +
                                                           "_input_tensor.txt")
        self.test_answers_file = os.path.join(test_path, "test_answers.txt")
        self.test_answers_tensor_file = os.path.join(save_dir, "test_" + distribution + "_output_tensor.txt")
        self.test_line_number_file = os.path.join(save_dir, "test_" + distribution + "_line_number_file.txt")
        self.test_verb_file = os.path.join(save_dir, "test_" + distribution + "_verb_input.txt")
        self.test_verb_times_file = os.path.join(save_dir, "test_" + distribution + "_verb_times_file.txt")

        # Edit (add the start sentence token) the input files if not already done.
        if not os.path.exists(self.test_file):
            start = datetime.datetime.now()
            print("Edit input files: Add start sentence token.")
            logger.info("Edit input files: Add start sentence token.")
            _add_start_tokens_text(self.test_original_file, self.test_file)
            end = datetime.datetime.now()
            duration = end - start
            print("Time duration: {}".format(duration))
            logger.info("Time duration: {}".format(duration))

        # Load embedding
        print("Loading embedding")
        logger.info("Loading embedding")
        start = datetime.datetime.now()
        self._embedding = _load_embedding(embedding_file)
        end = datetime.datetime.now()
        duration = end - start
        print("Time duration: {}".format(duration))
        logger.info("Time duration: {}".format(duration))

        # Load word and character vocab files.
        print("Loading word and character vocab.")
        logger.info("Loading word and character vocab.")
        start = datetime.datetime.now()
        self._load_vocabs(word_vocab_file, char_vocab_file)
        end = datetime.datetime.now()
        duration = end - start
        print("Time duration: {}".format(duration))
        logger.info("Time duration: {}".format(duration))

        # temp_text_vocab_file = os.path.join(save_dir, "vocab_temp.txt")
        # with open(temp_text_vocab_file, 'w') as tv:
        #     for i in self.words:
        #         tv.write(i + "\n")
        # print(self.chars)

        # Create correct preprocessed files from test files.
        if not (os.path.exists(self.test_verb_tensor_file) and os.path.exists(self.test_left_context_tensor_file) and
                os.path.exists(self.test_right_context_tensor_file) and os.path.exists(self.test_answers_tensor_file)):
            # Preprocess the test files
            print("Preprocessing test and test answers files.")
            logger.info("Preprocessing test and test answers files.")
            start = datetime.datetime.now()
            self._preprocess(self.test_file, self.test_answers_file, self.test_verb_tensor_file,
                             self.test_left_context_tensor_file, self.test_right_context_tensor_file,
                             self.test_answers_tensor_file, self.test_line_number_file, self.test_verb_times_file,
                             self.test_verb_file, max_length_file)
            end = datetime.datetime.now()
            duration = end - start
            print("Time duration: {}".format(duration))
            logger.info("Time duration: {}".format(duration))

        # print(self.character_to_id)

        with open(self.test_verb_tensor_file, 'r', encoding='utf-8') as f:
            i = 0
            for i, _ in enumerate(f):
                i += 1
                pass
            data_len = i
            self.num_batches_test = math.ceil(data_len / self.batch_size)
            print('Num Batches: ' + str(self.num_batches_test))
            logger.info('Num Batches: ' + str(self.num_batches_test))

        logger.info(" ")
        logger.info("----------STOP EVALUATING PREPROCESSING----------")
        logger.info(" ")

    def _load_vocabs(self, word_vocab_file, char_vocab_file):
        """Function to load created vocabs from vocab files."""
        # with open(word_vocab_file, 'rb') as f:
        #     self.words = pickle.load(f)
        # self.word_vocab_size = len(self.words)
        # self.word_to_id = dict(zip(self.words, range(len(self.words))))
        # with open(char_vocab_file, 'rb') as f:
        #     self.chars = pickle.load(f)
        # self.char_vocab_size = len(self.chars)
        # self.character_to_id = dict(zip(self.chars, range(len(self.chars))))
        with open(word_vocab_file, 'rb') as f:
            self.word_to_id = pickle.load(f)
        self.word_vocab_size = len(self.word_to_id)
        with open(char_vocab_file, 'rb') as f:
            self.character_to_id = pickle.load(f)
        self.char_vocab_size = len(self.character_to_id)

    def get_embedding_model_repr(self):
        """Function to get the representation of the embedding."""
        return self._embedding.wv.syn0

    def _preprocess(self, input_file, target_file, verb_input_tensor_file, left_context_input_tensor_file,
                    right_context_input_tensor_file, target_input_tensor_file, line_number_file, verb_times_file,
                    verb_file, max_length_file):
        """Function to preprocess an input and target file in the correct format for the dt_Corrector."""
        input_sentence_generator = read_sentences_generator_text(input_file)
        target_sentence_generator = read_sentences_generator_text(target_file)

        line_number = 0
        print("Preprocessing.")
        logger.info("Preprocessing.")

        max_verb_length_found = 0
        max_context_length_found = 0

        for input_sentence, target_sentence in zip(input_sentence_generator, target_sentence_generator):
            # Reformat inputs and targets
            # print(input_sentence)
            # print(target_sentence)
            # print(str(self.max_verb_sequence_length) + "," + str(self.max_context_sequence_length))
            sentence_verbs, sentence_verbs_times, sentence_left_contexts, sentence_right_contexts, \
                sentence_targets, number_of_inputs = reformat_sentence(input_sentence, target_sentence,
                                                                       self.max_verb_sequence_length,
                                                                       self.max_context_sequence_length)
            # Converting inputs in correct input format to ids in correct input format
            new_verb_input, new_left_context_input, new_right_context_input = \
                convert_correct_formatted_input_to_ids(sentence_verbs, sentence_left_contexts, sentence_right_contexts,
                                                       number_of_inputs, self.word_to_id, self.character_to_id)
            # Converting targets in correct target format to ids in correct target format
            # print(sentence_targets)
            # print(number_of_inputs)

            new_target_input = convert_target_data_to_adaptation_rule_value(sentence_targets, number_of_inputs)

            # Save the created data to the given file names
            # if not new_target_input:
            #     save_reformatted_sentence(verb_input_tensor_file)
            #     save_reformatted_sentence(left_context_input_tensor_file)
            #     save_reformatted_sentence(right_context_input_tensor_file)
            #     save_int(target_input_tensor_file)
            #     save_int(line_number, line_number_file)
            # else:
            # print(sentence_verbs)
            # print(new_target_input)

            for new_verb_line, new_left_context_line, new_right_context_line, new_target_line, new_sentence_verb,\
                new_verb_time, in zip(new_verb_input, new_left_context_input, new_right_context_input,
                                      new_target_input, sentence_verbs, sentence_verbs_times):
                save_reformatted_sentence(new_verb_line, verb_input_tensor_file)
                verb_length = len(new_verb_line)
                if verb_length > max_verb_length_found:
                    max_verb_length_found = verb_length
                save_reformatted_sentence(new_left_context_line, left_context_input_tensor_file)
                context_length = len(new_left_context_line)
                if context_length > max_context_length_found:
                    max_context_length_found = context_length
                save_reformatted_sentence(new_right_context_line, right_context_input_tensor_file)
                context_length = len(new_right_context_line)
                if context_length > max_context_length_found:
                    max_context_length_found = context_length
                save_int(new_target_line, target_input_tensor_file)
                # print(new_sentence_verb)
                # print("PREPROCESS " + str(new_target_line))
                save_int(line_number, line_number_file)
                verb = ''.join(new_sentence_verb)
                save_word(verb, verb_file)
                save_int(new_verb_time, verb_times_file)

            line_number += 1
        if self._max_evaluation:
            self.max_verb_sequence_length = max_verb_length_found
            self.max_context_sequence_length = max_context_length_found
            with open(max_length_file, "w", encoding='utf-8') as f:
                f.write(str(self.max_verb_sequence_length) + '\n')
                f.write(str(self.max_context_sequence_length))

        print("Max verb length: " + str(self.max_verb_sequence_length))
        logger.info("Max verb length: " + str(self.max_verb_sequence_length))
        print("Max context length: " + str(self.max_context_sequence_length))
        logger.info("Max context length: " + str(self.max_context_sequence_length))
        print("Preprocessing done.")
        logger.info("Preprocessing done.")

    def generate_batch(self, verb_sentence_generator, left_context_sentence_generator,
                       right_context_sentence_generator, answers_sentence_generator, line_number_sentence_generator,
                       verbs_generator, verb_time_sentence_generator):
        batch_length = 0
        verb_data_batch = []
        verb_seq_length_batch = []
        left_context_data_batch = []
        left_context_seq_length_batch = []
        right_context_data_batch = []
        right_context_seq_length_batch = []
        target_batch = []
        line_number_batch = []
        verbs_batch = []
        verb_time_batch = []
        # Total different adaptation rules
        total_cases = len(AdaptRule)

        try:
            while batch_length < self.batch_size:
                verb_input = []
                verb_id_input = next(verb_sentence_generator)
                for char_id in verb_id_input:
                    one_hot_verb_char = convert_number_to_one_hot_vector(char_id, len(self.character_to_id))
                    verb_input.append(one_hot_verb_char)
                verb_seq_length = len(verb_input)
                left_input = next(left_context_sentence_generator)
                left_seq_length = len(left_input)
                right_input = next(right_context_sentence_generator)
                right_seq_length = len(right_input)
                target_case = next(answers_sentence_generator)
                target_input = convert_number_to_one_hot_vector(target_case[0], total_cases)
                line_number = next(line_number_sentence_generator)
                verb = next(verbs_generator)
                verb_time = next(verb_time_sentence_generator)

                # 0-padding
                verb_input.extend([convert_number_to_one_hot_vector(0, self.char_vocab_size)] *
                                  (self.max_verb_sequence_length - len(verb_input)))
                left_input.extend([0] * (self.max_context_sequence_length - len(left_input)))
                right_input.extend([0] * (self.max_context_sequence_length - len(right_input)))

                verb_data_batch.append(verb_input)
                verb_seq_length_batch.append(verb_seq_length)
                left_context_data_batch.append(left_input)
                left_context_seq_length_batch.append(left_seq_length)
                right_context_data_batch.append(right_input)
                right_context_seq_length_batch.append(right_seq_length)
                target_batch.append(target_input)
                line_number_batch.append(line_number)
                verbs_batch.append(verb)
                verb_time_batch.append(verb_time)
                batch_length += 1

        except StopIteration:
            pass
        # verb_data = np.array(verb_input)
        # verb_data_seq_length = np.array(verb_seq_length)
        # left_context_data = np.array(left_input)
        # left_context_data_seq_length = np.array(left_seq_length)
        # right_context_data = np.array(right_input)
        # right_context_data_seq_length = np.array(right_seq_length)
        # target_data = np.array(target_input)
        # return verb_data, verb_data_seq_length, left_context_data, left_context_data_seq_length, \
        #            right_context_data, right_context_data_seq_length, target_data

        # print(target_batch)
        return verb_data_batch, verb_seq_length_batch, left_context_data_batch, left_context_seq_length_batch, \
            right_context_data_batch, right_context_seq_length_batch, target_batch, line_number_batch, verbs_batch, \
            verb_time_batch


class UsePreProcessor:
    """This class preprocess texts without solutions."""

    def __init__(self, use_path, model_path):

        logger.info(" ")
        logger.info("----------START CHECKER PREPROCESSING----------")
        logger.info(" ")

        self.max_verb_sequence_length = math.inf
        self.max_context_sequence_length = math.inf
        self.batch_size = math.inf
        self.max_verb_sequence_length = math.inf
        self.max_context_sequence_length = math.inf

        word_vocab_file = os.path.join(model_path, "vocab_words.pkl")
        char_vocab_file = os.path.join(model_path, "vocab_chars.pkl")
        embedding_file = os.path.join(model_path, "embedding")

        max_length_file = os.path.join(use_path, "max_lengths")
        if os.path.isfile(max_length_file):
            with open(max_length_file, "r", encoding='utf-8') as f:
                lines = f.read().splitlines()
                self.max_verb_sequence_length = int(lines[0])
                self.max_context_sequence_length = int(lines[1])

        self.use_original_file = os.path.join(use_path, "use_original.txt")
        self.use_file = os.path.join(use_path, "use.txt")
        self.use_verb_tensor_file = os.path.join(use_path, "use_verb_input_tensor.txt")
        self.use_left_context_tensor_file = os.path.join(use_path, "use_left_context_input_tensor.txt")
        self.use_right_context_tensor_file = os.path.join(use_path, "use_right_context_input_tensor.txt")
        self.use_verb_file = os.path.join(use_path, "use_verb_input.txt")
        self.use_line_number_file = os.path.join(use_path, "use_line_number_file.txt")
        self.use_verb_position_file = os.path.join(use_path, "use_verb_position_file.txt")

        # Edit (add the start sentence token) the input files if not already done.
        if not os.path.exists(self.use_file):
            start = datetime.datetime.now()
            print("Edit input files: Add start sentence token.")
            logger.info("Edit input files: Add start sentence token.")
            _add_start_tokens_text(self.use_original_file, self.use_file)
            end = datetime.datetime.now()
            duration = end - start
            print("Time duration: {}".format(duration))
            logger.info("Time duration: {}".format(duration))

        # Load embedding
        print("Loading embedding")
        logger.info("Loading embedding")
        start = datetime.datetime.now()
        self._embedding = _load_embedding(embedding_file)
        end = datetime.datetime.now()
        duration = end - start
        print("Time duration: {}".format(duration))
        logger.info("Time duration: {}".format(duration))

        # Load word and character vocab files.
        print("Loading word and character vocab.")
        logger.info("Loading word and character vocab.")
        start = datetime.datetime.now()
        self._load_vocabs(word_vocab_file, char_vocab_file)
        end = datetime.datetime.now()
        duration = end - start
        print("Time duration: {}".format(duration))
        logger.info("Time duration: {}".format(duration))

        # Create correct preprocessed files from test files.
        if not (os.path.exists(self.use_verb_tensor_file) and os.path.exists(self.use_left_context_tensor_file) and
                os.path.exists(self.use_right_context_tensor_file)):
            # Preprocess the test files
            print("Preprocessing the file.")
            logger.info("Preprocessing the file.")
            start = datetime.datetime.now()
            self._preprocess(self.use_file, self.use_verb_tensor_file, self.use_left_context_tensor_file,
                             self.use_right_context_tensor_file, self.use_verb_file, self.use_line_number_file,
                             self.use_verb_position_file, max_length_file)
            end = datetime.datetime.now()
            duration = end - start
            print("Time duration: {}".format(duration))
            logger.info("Time duration: {}".format(duration))

        logger.info(" ")
        logger.info("----------STOP CHECKER PREPROCESSING----------")
        logger.info(" ")

    def _load_vocabs(self, word_vocab_file, char_vocab_file):
        """Function to load created vocabs from vocab files."""
        # with open(word_vocab_file, 'rb') as f:
        #     self.words = pickle.load(f)
        # self.word_vocab_size = len(self.words)
        # self.word_to_id = dict(zip(self.words, range(len(self.words))))
        # with open(char_vocab_file, 'rb') as f:
        #     self.chars = pickle.load(f)
        # self.char_vocab_size = len(self.chars)
        # self.character_to_id = dict(zip(self.chars, range(len(self.chars))))
        with open(word_vocab_file, 'rb') as f:
            self.word_to_id = pickle.load(f)
        self.word_vocab_size = len(self.word_to_id)
        with open(char_vocab_file, 'rb') as f:
            self.character_to_id = pickle.load(f)
        self.char_vocab_size = len(self.character_to_id)

    def get_embedding_model_repr(self):
        """Function to get the representation of the embedding."""
        return self._embedding.wv.syn0

    def _preprocess(self, input_file, verb_input_tensor_file, left_context_input_tensor_file,
                    right_context_input_tensor_file, verb_input_file, line_number_file, verb_position_file,
                    max_length_file):
        """Function to preprocess an input and target file in the correct format for the dt_Corrector."""
        input_sentence_generator = read_sentences_generator_text(input_file)

        line_number = 0
        print("Preprocessing.")
        logger.info("Preprocessing.")

        max_verb_length_found = 0
        max_context_length_found = 0

        for input_sentence in input_sentence_generator:
            # Reformat inputs and targets
            sentence_verbs, sentence_left_contexts, sentence_right_contexts, \
                number_of_inputs = reformat_sentence_without_targets(input_sentence, self.max_verb_sequence_length,
                                                                     self.max_context_sequence_length)
            # Converting inputs in correct input format to ids in correct input format
            new_verb_input, new_left_context_input, new_right_context_input = \
                convert_correct_formatted_input_to_ids(sentence_verbs, sentence_left_contexts, sentence_right_contexts,
                                                       number_of_inputs, self.word_to_id, self.character_to_id)

            # Save the created data to the given file names
            for new_verb_line, new_left_context_line, new_right_context_line, new_sentence_verb, words_for_verb in \
                    zip(new_verb_input, new_left_context_input, new_right_context_input, sentence_verbs, sentence_left_contexts):
                save_reformatted_sentence(new_verb_line, verb_input_tensor_file)
                verb_length = len(new_verb_line)
                if verb_length > max_verb_length_found:
                    max_verb_length_found = verb_length
                save_reformatted_sentence(new_left_context_line, left_context_input_tensor_file)
                context_length = len(new_left_context_line)
                if context_length > max_context_length_found:
                    max_context_length_found = context_length
                save_reformatted_sentence(new_right_context_line, right_context_input_tensor_file)
                context_length = len(new_right_context_line)
                if context_length > max_context_length_found:
                    max_context_length_found = context_length
                verb = ''.join(new_sentence_verb)
                save_word(verb, verb_input_file)
                save_int(line_number, line_number_file)
                save_int(len(words_for_verb)-1, verb_position_file)

            line_number += 1

        self.max_verb_sequence_length = max_verb_length_found
        self.max_context_sequence_length = max_context_length_found
        with open(max_length_file, "w", encoding='utf-8') as f:
            f.write(str(self.max_verb_sequence_length) + '\n')
            f.write(str(self.max_context_sequence_length))

        print("Max verb length: " + str(self.max_verb_sequence_length))
        logger.info("Max verb length: " + str(self.max_verb_sequence_length))
        print("Max context length: " + str(self.max_context_sequence_length))
        logger.info("Max context length: " + str(self.max_context_sequence_length))
        print("Preprocessing done.")
        logger.info("Preprocessing done.")

    @staticmethod
    def _load_preprocessed(input_tensor_file, target_tensor_file):
        """Function to load an input and target tensor file."""
        converted_input_data = np.load(input_tensor_file)
        converted_target_data = np.load(target_tensor_file)
        return converted_input_data, converted_target_data

    def generate_batch(self, verb_sentence_generator, left_context_sentence_generator,
                       right_context_sentence_generator, verbs_generator, line_number_sentence_generator,
                       verb_position_sentence_generator):
        batch_length = 0
        verb_data_batch = []
        verb_seq_length_batch = []
        left_context_data_batch = []
        left_context_seq_length_batch = []
        right_context_data_batch = []
        right_context_seq_length_batch = []
        verbs_batch = []
        line_number_batch = []
        verb_position_batch = []
        # Total different adaptation rules
        total_cases = len(AdaptRule)

        try:
            while batch_length < self.batch_size:
                verb_input = []
                verb_id_input = next(verb_sentence_generator)
                for char_id in verb_id_input:
                    one_hot_verb_char = convert_number_to_one_hot_vector(char_id, len(self.character_to_id))
                    verb_input.append(one_hot_verb_char)
                verb_seq_length = len(verb_input)
                left_input = next(left_context_sentence_generator)
                left_seq_length = len(left_input)
                right_input = next(right_context_sentence_generator)
                right_seq_length = len(right_input)
                verb = next(verbs_generator)
                line_number = next(line_number_sentence_generator)
                verb_position = next(verb_position_sentence_generator)

                # 0-padding
                verb_input.extend([convert_number_to_one_hot_vector(0, self.char_vocab_size)] *
                                  (self.max_verb_sequence_length - len(verb_input)))
                left_input.extend([0] * (self.max_context_sequence_length - len(left_input)))
                right_input.extend([0] * (self.max_context_sequence_length - len(right_input)))

                verb_data_batch.append(verb_input)
                verb_seq_length_batch.append(verb_seq_length)
                left_context_data_batch.append(left_input)
                left_context_seq_length_batch.append(left_seq_length)
                right_context_data_batch.append(right_input)
                right_context_seq_length_batch.append(right_seq_length)
                verbs_batch.append(verb)
                line_number_batch.append(line_number)
                verb_position_batch.append(verb_position)
                batch_length += 1

        except StopIteration:
            self.batch_size = batch_length
        # verb_data = np.array(verb_input)
        # verb_data_seq_length = np.array(verb_seq_length)
        # left_context_data = np.array(left_input)
        # left_context_data_seq_length = np.array(left_seq_length)
        # right_context_data = np.array(right_input)
        # right_context_data_seq_length = np.array(right_seq_length)
        # target_data = np.array(target_input)
        # return verb_data, verb_data_seq_length, left_context_data, left_context_data_seq_length, \
        #            right_context_data, right_context_data_seq_length, target_data

        return verb_data_batch, verb_seq_length_batch, left_context_data_batch, left_context_seq_length_batch, \
            right_context_data_batch, right_context_seq_length_batch, verbs_batch, line_number_batch, \
            verb_position_batch


def save_empty_line(file):
    with open(file, 'a', encoding='utf-8') as f:
        f.write('\n')


def save_reformatted_sentence(list_ids, file):
    with open(file, 'a', encoding='utf-8') as f:
        for new_id in list_ids:
            f.write(str(new_id) + ' ')
        f.write('\n')


def save_word(word, file):
    with open(file, 'a', encoding='utf-8') as f:
        f.write(word + '\n')


def save_int(number, file):
    with open(file, 'a', encoding='utf-8') as f:
        f.write(str(number) + '\n')


def read_sentences_generator_text(file):
    with open(file, 'r', encoding='utf-8') as f:
        for sentence in f:
            yield sentence


def read_words_generator_text(file):
    with open(file, 'r', encoding='utf-8') as f:
        for word in f:
            new_word = word.strip(' \n')
            yield new_word


def read_sentences_generator_number(file):
    with open(file, 'r', encoding='utf-8') as f:
        for number in f:
            yield int(number)


def read_sentences_generator_numbers(file):
    with open(file, 'r', encoding='utf-8') as f:
        for sentence in f:
            lst_ids = []
            temp_sentence = sentence.strip(' \n')
            if not temp_sentence == "":
                lst = temp_sentence.split(' ')
                for i in lst:
                    if not i == "":
                        lst_ids.append(int(i))
            yield lst_ids


def tag_input_sentence(sentence):
    """Function to tag one sentence with POS-tags."""
    tags = tagger.tag_text(sentence)
    tags2 = treetaggerwrapper.make_tags(tags, exclude_nottags=True)
    return tags2


def shuffle_two_lists(list1, list2):
    """Function to shuffle two lists in the same way."""
    combined_list = list(zip(list1, list2))
    random.shuffle(combined_list)
    list1, list2 = zip(*combined_list)
    return list1, list2


def _replace_infrequent_words_text(original_filename, converted_filename, min_count):
    """Function to replace all words in a file that occur not frequent enough."""
    data = []
    verb_data = []
    with open(original_filename, 'r', encoding='utf-8') as f1:
        input_lines = f1.read().splitlines()
        # input_lines = [x.lower() for x in input_lines]
        for line_number in range(len(input_lines)):
            tags_sentence = tag_input_sentence(input_lines[line_number])
            for tag in tags_sentence:
                if not (type(tag).__name__ == "NotTag"):
                    if hasattr(tag, 'pos') and tag.pos.startswith('2') and len(tag.pos) >= 3:
                        verb_data.append(tag.word.lower())
                    else:
                        data.append(tag.word.lower())

    counter = collections.Counter(data)
    verb_counter = collections.Counter(verb_data)
    d = dict(counter)
    d2 = dict(verb_counter)
    nb_not_frequent_items = 0
    for key, value in d.items():
        if value < min_count:
            nb_not_frequent_items += 1
    d = {k: v for k, v in d.items() if v >= min_count}
    d[unknown_token] = nb_not_frequent_items
    d.update(d2)
    count_pairs = sorted(d.items(), key=lambda x: (-x[1], x[0]))

    words, _ = list(zip(*count_pairs))
    word_to_id = dict(zip(words, range(len(words))))

    with open(converted_filename, "w", encoding='utf-8') as out_f:
        for line_number in range(len(input_lines)):
            new_line = _replace_unknown_words_sentence(input_lines[line_number], unknown_token, word_to_id)
            # input_lines[line_number] = new_line
            new_new_line = _add_start_token(new_line, start_sentence_token)
            out_f.write(new_new_line + '\n')


def _add_start_tokens_text(original_filename, converted_filename):
    """Function to replace all words in a file that occur not frequent enough."""
    with open(original_filename, 'r', encoding='utf-8') as f1, open(converted_filename, "w", encoding='utf-8')\
            as out_f:
        input_lines = f1.read().splitlines()
        for line in input_lines:
            new_line = _add_start_token(line, start_sentence_token)
            out_f.write(new_line + '\n')


def _add_start_token(sentence, start_token):
    """Function to add a start token to a sentence."""
    new_sentence = ' '.join([start_token, sentence])
    return new_sentence


def _replace_unknown_words_sentence(sentence, replacement, vocab):
    """Function to replace words in a sentence that do not occur in a given vocab."""
    word_list = sentence.split()
    for i in range(len(word_list)):
        if word_list[i].lower() not in vocab:
            word_list[i] = replacement
    sentence = ' '.join(word_list)
    return sentence


def _build_character_vocab(filename):
    """Function to build a vocab of characters of a file."""
    with open(filename, 'r', encoding='utf-8') as f1:
        sentences = f1.read().splitlines()
    # text = _read_file(filename)
    # sentences = tokenizer.tokenize(text)
    data = []
    for sentence in sentences:
        tags_sentence = tag_input_sentence(sentence)
        for tag in tags_sentence:
            if not (type(tag).__name__ == "NotTag"):
                data.append(tag.word)
    for i in range(len(data)):
        data[i] = data[i].lower()
    data2 = "".join(data)

    new_data = list(data2)
    new_data.append(unknown_character)

    counter = collections.Counter(new_data)
    count_pairs = sorted(counter.items(), key=lambda x: (-x[1], x[0]))

    characters, _ = list(zip(*count_pairs))
    character_to_id = dict(zip(characters, range(len(characters))))

    return character_to_id


class MySentences(object):
    """A memory-friendly iterator of a text with sentences used for creating the embedding."""

    def __init__(self, data_path):
        self.data_path = data_path

    def __iter__(self):
        for line in open(self.data_path, 'r', encoding='utf-8'):
            yield line.lower().split()


def _build_embedding_from_file(filename, embedding_file, size=100, min_count=1, workers=4, sg=1):
    """Function to build an embedding model from a file."""
    sentences = MySentences(filename)  # a memory-friendly iterator
    model = gensim.models.Word2Vec(sentences, size=size, min_count=min_count, workers=workers, sg=sg)
    model.save(embedding_file)
    return model


def _load_embedding(embedding_file):
    """Function to load an embedding model."""
    model = gensim.models.Word2Vec.load(embedding_file)
    return model


def reformat_sentence(input_sentence, target_sentence, max_verb_sequence_length, max_context_sequence_length):
    """Function to reformat the given input lines.

    Transforms each sentence into a predefined format.
    If a sentence has multiple verbs with a possible d/t-fault, there are multiple inputs for this sentence.
    The multiple inputs of one sentence are considered as inputs of multiple sentences with only one verb with a
    possible d/t-fault.

    Args:
        list_input_lines (list): List with all the input lines.
        list_target_lines (list): List with the same order as the list_input_lines with for every input line all its
                                  target pairs (a target pair is the used verb in the sentence and the correct verb).

    Returns:
        reformatted_inputs (list): List of sentences with every sentence in the correct format.
        targets (list): List of targets.

    Example:
        Input:
            list_input_lines = ["Het wordt mooi weer .", "Ik antwoord correct ."]
            list_target_lines = ["wordt wordt", "antwoord antwoord"]
            max_context_sequence_length = 20
            max_verb_sequence_length = 20
        Output:
            ([[['w', 'o', 'r', 'd', 't'], ['Het'], ['.', 'weer', 'mooi']],
              [['a', 'n', 't', 'w', 'o', 'o', 'r', 'd'], ['Ik'], ['.', 'correct']]],
             [['wordt', 'wordt'], ['antwoord', 'antwoord']])
    """
    verb_inputs = []
    verb_times_inputs = []
    left_context_inputs = []
    right_context_inputs = []
    targets = []
    new_verbs, new_left_contexts, new_right_contexts, \
        new_targets, number_of_inputs, verb_times = convert_sentence_to_input_format(input_sentence, target_sentence,
                                                                                     max_verb_sequence_length,
                                                                                     max_context_sequence_length)
    # Convert every result of a sentence, so every input of a line with multiple verbs is considered as inputs of
    # multiple lines with one verb.
    for i in range(number_of_inputs):
        verb_inputs.append(new_verbs[i])
        verb_times_inputs.append(verb_times[i])
        left_context_inputs.append(new_left_contexts[i])
        right_context_inputs.append(new_right_contexts[i])
        targets.append(new_targets[i])

    return verb_inputs, verb_times_inputs, left_context_inputs, right_context_inputs, targets, number_of_inputs


def reformat_sentence_without_targets(input_sentence, max_verb_sequence_length, max_context_sequence_length):
    """Function to reformat the given input lines.

    Transforms each sentence into a predefined format.
    If a sentence has multiple verbs with a possible d/t-fault, there are multiple inputs for this sentence.
    The multiple inputs of one sentence are considered as inputs of multiple sentences with only one verb with a
    possible d/t-fault.

    Args:
        list_input_lines (list): List with all the input lines.
        list_target_lines (list): List with the same order as the list_input_lines with for every input line all its
                                  target pairs (a target pair is the used verb in the sentence and the correct verb).

    Returns:
        reformatted_inputs (list): List of sentences with every sentence in the correct format.
        targets (list): List of targets.

    Example:
        Input:
            list_input_lines = ["Het wordt mooi weer .", "Ik antwoord correct ."]
            list_target_lines = ["wordt wordt", "antwoord antwoord"]
            max_context_sequence_length = 20
            max_verb_sequence_length = 20
        Output:
            ([[['w', 'o', 'r', 'd', 't'], ['Het'], ['.', 'weer', 'mooi']],
              [['a', 'n', 't', 'w', 'o', 'o', 'r', 'd'], ['Ik'], ['.', 'correct']]],
             [['wordt', 'wordt'], ['antwoord', 'antwoord']])
    """
    verb_inputs = []
    left_context_inputs = []
    right_context_inputs = []
    new_verbs, new_left_contexts, new_right_contexts, \
        number_of_inputs = convert_sentence_to_input_format_without_targets(input_sentence, max_verb_sequence_length,
                                                                            max_context_sequence_length)
    # Convert every result of a sentence, so every input of a line with multiple verbs is considered as inputs of
    # multiple lines with one verb.
    for i in range(number_of_inputs):
        verb_inputs.append(new_verbs[i])
        left_context_inputs.append(new_left_contexts[i])
        right_context_inputs.append(new_right_contexts[i])

    return verb_inputs, left_context_inputs, right_context_inputs, number_of_inputs


def convert_sentence_to_input_format(sentence, line_targets_sentence, max_verb_sequence_length,
                                     max_context_sequence_length):
    """
    Function to reformat a sentence into the predefined input format.

    Transforms a sentence into a predefined format.
    The format is [list_character_verbs, list_words_left_context, list_words_right_context_reversed_order]
    The transformation of a sentence in the correct format can result in multiple lists in the created reformatted list.
    Every verb detected is considered, and if it is a possible d/t-fault, a result for this verb is formed.

    Args:
        sentence (str):
        line_targets_sentence ():

    Returns:
        reformatted_sentence (list<list<list<str>>>): List with lists of the sentence. A list of a sentence contains
            three lists. The first one with the characters of the verb concentrated on, the second one with the words
            of the left context, the third with the words of the right context.
        verbs (list): List of verbs used.
        targets (list<list<str>>): List with lists of target pairs. A target pair is the wrong (verb in sentence) and
            the correct verb

    Example:
        Input:
            list_input_lines = "Het wordt mooi weer ."
            line_target_lines = ["wordt wordt"]
        Output:
            ([[['w', 'o', 'r', 'd', 't'], ['Het'], ['.', 'weer', 'mooi']]],
             ['wordt'],
             [['wordt', 'wordt']])
    """
    tags_sentence = tag_input_sentence(sentence)
    sentence_length = len(tags_sentence)
    list_targets_sentence = line_targets_sentence.split()
    created_verbs = []
    verb_times = []
    created_left_contexts = []
    created_right_contexts = []
    number_created_inputs = 0
    created_targets = []
    number_of_seen_verbs = 0
    # print("_______________")
    # print(sentence)
    # print(list_targets_sentence)
    for i in range(sentence_length):
        # The word in the sentence needs a tag to be a possible verb.
        tag_word_i = tags_sentence[i]
        if not type(tag_word_i).__name__ == "NotTag":
            if hasattr(tag_word_i, 'pos'):
                pos_tag = tag_word_i.pos
                # print(tag_word_i.word)
                # print(tag_word_i)
                # print(list_targets_sentence)
                # Test if the tagged word is a verb (...\t2__\... means the tagged word is a verb).
                if pos_tag.startswith('2') and len(pos_tag) >= 3:
                    # Test present time and imperative
                    test_present_time = 4 <= int(pos_tag[1]) <= 8 and \
                                        (1 <= int(pos_tag[2]) <= 3 or 7 <= int(pos_tag[2]) <= 8)
                    # Test past time
                    test_past_time = 4 <= int(pos_tag[1]) <= 8 and 5 <= int(pos_tag[2]) <= 6
                    # Test past participle
                    test_past_participle = 0 <= int(pos_tag[1]) <= 3 and 6 <= int(pos_tag[2]) <= 9
                    # Test if the conjugation time of the verb is possible for d/t-faults.

                    verb = tag_word_i.word
                    left_context_len = i
                    possible_dt_fault = False
                    if test_past_time:
                        if verb.endswith('dde') or verb.endswith('de') or \
                                verb.endswith('dden') or verb.endswith('den') \
                                or verb.endswith('tte') or verb.endswith('te') \
                                or verb.endswith('tten') or verb.endswith('ten'):
                            possible_dt_fault = True
                            verb_time = VerbTime.PAST.value
                    elif test_present_time:
                        if verb.endswith('d') or verb.endswith('dt') or \
                                (verb.endswith('t') and not verb.endswith('dt')):
                            possible_dt_fault = True
                            verb_time = VerbTime.PRESENT.value
                    elif test_past_participle:
                        if verb.endswith('d') or (verb.endswith('t') and not verb.endswith('dt')):
                            possible_dt_fault = True
                            verb_time = VerbTime.PAST_PARTICIPLE.value

                    if possible_dt_fault:
                        # Test if there is a target pair for the found verb
                        if verb in list_targets_sentence[number_of_seen_verbs::2]:
                            # Make list of characters of verb
                            converted_verb = list(verb)
                            left_context = []
                            right_context = []
                            # Make list of all words (in order) coming before the verb in the sentence.
                            for j in range(left_context_len):
                                left_context.append(tags_sentence[j].word)
                            # Make list of all words (in reversed order) coming after the verb in the sentence.
                            for k in range(sentence_length - 1, left_context_len, -1):
                                right_context.append(tags_sentence[k].word)
                            # print(converted_verb)
                            # Test if the limitations (verb length, context lengths left and right) are fulfilled.
                            if len(converted_verb) <= max_verb_sequence_length and \
                                len(left_context) <= max_context_sequence_length \
                                    and len(right_context) <= max_context_sequence_length:
                                created_verbs.append(converted_verb)
                                verb_times.append(verb_time)
                                created_left_contexts.append(left_context)
                                created_right_contexts.append(right_context)
                                # TODO juiste target uit list meegeven
                                remaining_target_list_only_bad_verbs = list_targets_sentence[number_of_seen_verbs::2]
                                verb_bad_list_index = remaining_target_list_only_bad_verbs.index(verb)
                                verb_original_list_index = verb_bad_list_index * 2 + number_of_seen_verbs
                                target = [list_targets_sentence[verb_original_list_index],
                                          list_targets_sentence[verb_original_list_index + 1]]
                                number_of_seen_verbs = verb_original_list_index + 2
                                created_targets.append(target)
                                number_created_inputs += 1
                    # print(created_targets)
    return created_verbs, created_left_contexts, created_right_contexts, created_targets, number_created_inputs, verb_times


def convert_sentence_to_input_format_without_targets(sentence, max_verb_sequence_length, max_context_sequence_length):
    """
    Function to reformat a sentence into the predefined input format.

    Transforms a sentence into a predefined format.
    The format is [list_character_verbs, list_words_left_context, list_words_right_context_reversed_order]
    The transformation of a sentence in the correct format can result in multiple lists in the created reformatted list.
    Every verb detected is considered, and if it is a possible d/t-fault, a result for this verb is formed.

    Args:
        sentence (str):
        line_targets_sentence ():

    Returns:
        reformatted_sentence (list<list<list<str>>>): List with lists of the sentence. A list of a sentence contains
            three lists. The first one with the characters of the verb concentrated on, the second one with the words
            of the left context, the third with the words of the right context.
        verbs (list): List of verbs used.
        targets (list<list<str>>): List with lists of target pairs. A target pair is the wrong (verb in sentence) and
            the correct verb

    Example:
        Input:
            list_input_lines = "Het wordt mooi weer ."
            line_target_lines = ["wordt wordt"]
        Output:
            ([[['w', 'o', 'r', 'd', 't'], ['Het'], ['.', 'weer', 'mooi']]],
             ['wordt'],
             [['wordt', 'wordt']])
    """
    tags_sentence = tag_input_sentence(sentence)
    sentence_length = len(tags_sentence)
    created_verbs = []
    created_left_contexts = []
    created_right_contexts = []
    number_created_inputs = 0
    number_of_seen_verbs = 0
    # print("_______________")
    # print(sentence)
    # print(list_targets_sentence)
    for i in range(sentence_length):
        # The word in the sentence needs a tag to be a possible verb.
        tag_word_i = tags_sentence[i]
        if not type(tag_word_i).__name__ == "NotTag":
            if hasattr(tag_word_i, 'pos'):
                pos_tag = tag_word_i.pos
                # print(tag_word_i.word)
                # print(tag_word_i)
                # print(list_targets_sentence)
                # Test if the tagged word is a verb (...\t2__\... means the tagged word is a verb).
                if pos_tag.startswith('2') and len(pos_tag) >= 3:
                    # Test present time and imperative
                    test_present_time = 4 <= int(pos_tag[1]) <= 8 and \
                                        (1 <= int(pos_tag[2]) <= 3 or 7 <= int(pos_tag[2]) <= 8)
                    # Test past time
                    test_past_time = 4 <= int(pos_tag[1]) <= 8 and 5 <= int(pos_tag[2]) <= 6
                    # Test past participle
                    test_past_participle = 0 <= int(pos_tag[1]) <= 3 and 6 <= int(pos_tag[2]) <= 9
                    # Test if the conjugation time of the verb is possible for d/t-faults.

                    verb = tag_word_i.word
                    left_context_len = i
                    possible_dt_fault = False
                    if test_past_time:
                        if verb.endswith('dde') or verb.endswith('de') or \
                                verb.endswith('dden') or verb.endswith('den') \
                                or verb.endswith('tte') or verb.endswith('te') \
                                or verb.endswith('tten') or verb.endswith('ten'):
                            possible_dt_fault = True
                    elif test_present_time:
                        if verb.endswith('d') or verb.endswith('dt') or \
                                (verb.endswith('t') and not verb.endswith('dt')):
                            possible_dt_fault = True
                    elif test_past_participle:
                        if verb.endswith('d') or (verb.endswith('t') and not verb.endswith('dt')):
                            possible_dt_fault = True

                    if possible_dt_fault:
                        # Make list of characters of verb
                        converted_verb = list(verb)
                        left_context = []
                        right_context = []
                        # Make list of all words (in order) coming before the verb in the sentence.
                        for j in range(left_context_len):
                            left_context.append(tags_sentence[j].word)
                        # Make list of all words (in reversed order) coming after the verb in the sentence.
                        for k in range(sentence_length - 1, left_context_len, -1):
                            right_context.append(tags_sentence[k].word)

                        # Test if the limitations (verb length, context lengths left and right) are fulfilled.
                        if len(converted_verb) <= max_verb_sequence_length and \
                            len(left_context) <= max_context_sequence_length \
                                and len(right_context) <= max_context_sequence_length:
                            created_verbs.append(converted_verb)
                            created_left_contexts.append(left_context)
                            created_right_contexts.append(right_context)
                            number_of_seen_verbs += 1
                            number_created_inputs += 1
    return created_verbs, created_left_contexts, created_right_contexts, number_created_inputs


def convert_correct_formatted_input_to_ids(sentence_verbs, sentence_left_contexts, sentence_right_contexts,
                                           number_of_inputs, word_to_id_vocab, character_to_id_vocab):
    """Function to convert words and characters from input sentences (which are in correct format) to ids."""
    verb_input = []
    left_context_input = []
    right_context_input = []
    for i in range(number_of_inputs):
        verb = sentence_verbs[i]
        left_context = sentence_left_contexts[i]
        right_context = sentence_right_contexts[i]
        # Test if the limitations (verb length, context lengths left and right) are fulfilled.
        # if len(verb) <= max_verb_sequence_length and len(left_context) <= max_context_sequence_length \
        #         and len(right_context) <= max_context_sequence_length:
        new_verb_input = []
        new_left_context_input = []
        new_right_context_input = []
        for char_i in range(0, len(verb)):
            new_verb_input.append(get_voc_index_character(character_to_id_vocab, verb[char_i].lower()))
        # Every word of a context is converted in an id.
        for word in left_context:
            new_left_context_input.append(get_voc_index_word(word_to_id_vocab, word))
        for word in right_context:
            new_right_context_input.append(get_voc_index_word(word_to_id_vocab, word))

        verb_input.append(new_verb_input)
        left_context_input.append(new_left_context_input)
        right_context_input.append(new_right_context_input)

    return verb_input, left_context_input, right_context_input


def convert_number_to_one_hot_vector(n, length_vector):
    """Function to convert a number to a one hot vector with specified length."""
    lst = [0] * length_vector
    if n < length_vector:
        lst[n] = 1
    else:
        raise ValueError('Could not convert the number {} in a one-hot-vector of length {}.'.format(n, length_vector))
    return lst


def get_voc_index_word(vocab, word):
    """Function to get the index of a word from a word vocab."""
    word = word.lower()
    if word in vocab:
        return vocab[word]
    else:
        return vocab[unknown_token]


def get_voc_index_character(vocab, character):
    """Function to get the index of a character from a character vocab."""
    char = character.lower()
    if char in vocab:
        return vocab[char]
    else:
        return vocab[unknown_character]


class VerbTime(Enum):
    """This class is an enum of the different possible adaptation rules concerning d/t-faults in Dutch."""
    PAST = 0
    PRESENT = 1
    PAST_PARTICIPLE = 2


class AdaptRule(Enum):
    """This class is an enum of the different possible adaptation rules concerning d/t-faults in Dutch."""
    DO_NOTHING = 0
    REM_D_ADD_T = 1
    REM_D_ADD_DT = 2
    REM_DT_ADD_T = 3
    REM_DT_ADD_D = 4
    REM_T_ADD_D = 5
    REM_T_ADD_DT = 6
    REM_DDE_ADD_DE = 7
    REM_DE_ADD_DDE = 8
    REM_DDEN_ADD_DEN = 9
    REM_DEN_ADD_DDEN = 10
    REM_TTE_ADD_TE = 11
    REM_TE_ADD_TTE = 12
    REM_TTEN_ADD_TEN = 13
    REM_TEN_ADD_TTEN = 14


def convert_target_data_to_adaptation_rule_value(sentence_targets, number_of_targets):
    """Function to convert a list of target lines to a list of one hot vectors based on the class AdaptRule."""
    targets = []

    for i in range(number_of_targets):
        target = sentence_targets[i]
        wrong_verb = target[0]
        correct_verb = target[1]
        adapt_rule = convert_wrong_and_correct_verb_to_adapt_rule(wrong_verb, correct_verb)
        targets.append(adapt_rule.value)
    return targets


def convert_wrong_and_correct_verb_to_adapt_rule(wrong_verb, correct_verb):
    """Function to convert a pair with a wrong and correct verb to an adapt rule of the class AdaptRule."""
    adapt_rule = None
    if wrong_verb == correct_verb:
        adapt_rule = AdaptRule.DO_NOTHING
    elif wrong_verb.endswith("d") and correct_verb.endswith("t") and not correct_verb.endswith("dt"):
        adapt_rule = AdaptRule.REM_D_ADD_T
    elif wrong_verb.endswith("d") and correct_verb.endswith("dt"):
        adapt_rule = AdaptRule.REM_D_ADD_DT
    elif wrong_verb.endswith("dt") and correct_verb.endswith("t") and not correct_verb.endswith("dt"):
        adapt_rule = AdaptRule.REM_DT_ADD_T
    elif wrong_verb.endswith("dt") and correct_verb.endswith("d"):
        adapt_rule = AdaptRule.REM_DT_ADD_D
    elif wrong_verb.endswith("t") and not wrong_verb.endswith("dt") and correct_verb.endswith("d"):
        adapt_rule = AdaptRule.REM_T_ADD_D
    elif wrong_verb.endswith("t") and not wrong_verb.endswith("dt") and correct_verb.endswith("dt"):
        adapt_rule = AdaptRule.REM_T_ADD_DT
    elif wrong_verb.endswith("dde") and correct_verb.endswith("de"):
        adapt_rule = AdaptRule.REM_DDE_ADD_DE
    elif wrong_verb.endswith("de") and correct_verb.endswith("dde"):
        adapt_rule = AdaptRule.REM_DE_ADD_DDE
    elif wrong_verb.endswith("dden") and correct_verb.endswith("den"):
        adapt_rule = AdaptRule.REM_DDEN_ADD_DEN
    elif wrong_verb.endswith("den") and correct_verb.endswith("dden"):
        adapt_rule = AdaptRule.REM_DEN_ADD_DDEN
    elif wrong_verb.endswith("tte") and correct_verb.endswith("te"):
        adapt_rule = AdaptRule.REM_TTE_ADD_TE
    elif wrong_verb.endswith("te") and correct_verb.endswith("tte"):
        adapt_rule = AdaptRule.REM_TE_ADD_TTE
    elif wrong_verb.endswith("tten") and correct_verb.endswith("ten"):
        adapt_rule = AdaptRule.REM_TTEN_ADD_TEN
    elif wrong_verb.endswith("ten") and correct_verb.endswith("tten"):
        adapt_rule = AdaptRule.REM_TEN_ADD_TTEN
    else:
        print(wrong_verb + " " + correct_verb)
    return adapt_rule


def apply_predicted_rule(prediction, verb):
    """Function to apply a predicted rule on a verb to get the corrected verb."""
    adapt_rule = AdaptRule(prediction).name
    new_verb = None
    if adapt_rule == AdaptRule.DO_NOTHING.name:
        new_verb = verb
    elif adapt_rule == AdaptRule.REM_D_ADD_T.name:
        new_verb = verb[:-1] + 't'
    elif adapt_rule == AdaptRule.REM_D_ADD_DT.name:
        new_verb = verb[:-1] + 'dt'
    elif adapt_rule == AdaptRule.REM_DT_ADD_T.name:
        new_verb = verb[:-2] + 't'
    elif adapt_rule == AdaptRule.REM_DT_ADD_D.name:
        new_verb = verb[:-2] + 'd'
    elif adapt_rule == AdaptRule.REM_T_ADD_D.name:
        new_verb = verb[:-1] + 'd'
    elif adapt_rule == AdaptRule.REM_T_ADD_DT.name:
        new_verb = verb[:-1] + 'dt'
    elif adapt_rule == AdaptRule.REM_DDE_ADD_DE.name:
        new_verb = verb[:-3] + 'de'
    elif adapt_rule == AdaptRule.REM_DDEN_ADD_DEN.name:
        new_verb = verb[:-4] + 'den'
    elif adapt_rule == AdaptRule.REM_DE_ADD_DDE.name:
        new_verb = verb[:-2] + 'dde'
    elif adapt_rule == AdaptRule.REM_DEN_ADD_DDEN.name:
        new_verb = verb[:-3] + 'dden'
    elif adapt_rule == AdaptRule.REM_TTE_ADD_TE.name:
        new_verb = verb[:-3] + 'te'
    elif adapt_rule == AdaptRule.REM_TTEN_ADD_TEN.name:
        new_verb = verb[:-4] + 'ten'
    elif adapt_rule == AdaptRule.REM_TE_ADD_TTE.name:
        new_verb = verb[:-2] + 'tte'
    elif adapt_rule == AdaptRule.REM_TEN_ADD_TTEN.name:
        new_verb = verb[:-3] + 'tten'
    return new_verb


# print(convert_sentence_to_input_format("De vraag word beantwoord .", "word wordt beantwoord beantwoord"))
# sentence_verbs, sentence_left_contexts, sentence_right_contexts, sentence_targets, number_of_inputs = \
#     reformat_sentence("De vraag word beantwoord .", "word wordt beantwoord beantwoord")
# word_to_id = {'commissie': 27, 'genomen': 55, '.': 3, 'zijn': 22, 'de': 1, 'zou': 56, 'wij': 29, 'u': 46, 'europese': 40, 'moet': 57, 'ik': 18, 'die': 13, 'over': 36, 'maar': 41, 'om': 19, 'deze': 34, 'het': 4, 'is': 12, 'þ': 2, 'duidelijk': 47, 'niet': 28, 'land': 49, 'zich': 58, 'zo': 59, 'als': 23, ',': 6, 'tot': 31, '?': 60, 'met': 16, 'voorzitter': 48, 'verslag': 39, 'en': 7, 'veel': 61, 'heeft': 17, 'te': 11, 'zouden': 62, 'in': 10, 'unie': 42, 'door': 26, 'lidstaten': 50, 'dit': 20, 'of': 45, 'ook': 24, 'dan': 51, 'een': 9, 'moeten': 52, 'aan': 25, 'er': 53, 'we': 32, 'zoals': 63, 'op': 15, 'kunnen': 54, 'wat': 35, 'ons': 37, 'worden': 21, 'van': 5, 'wordt': 30, 'dat': 8, '"': 43, 'europees': 64, 'zeer': 65, 'unknown': 0, 'echter': 66, 'naar': 44, 'tussen': 67, 'voor': 14, 'parlement': 33, 'hebben': 38}
# character_to_id = {'g': 25, 'v': 11, 's': 13, 'p': 20, 'r': 10, '.': 16, 'k': 4, 'f': 22, 'l': 18, 'c': 23, ',': 17, 'u': 3, '|': 28, 'e': 5, '?': 27, 'w': 2, 'm': 15, 'þ': 14, 'n': 0, 'z': 19, 'b': 24, '"': 26, 'a': 8, 'o': 1, 'i': 9, 'h': 12, 'd': 6, 'j': 21, 't': 7}
# new_verb_input, new_left_context_input, new_right_context_input = convert_correct_formatted_input_to_ids(sentence_verbs, sentence_left_contexts, sentence_right_contexts, number_of_inputs, word_to_id, character_to_id, 20, 20)
# new_target_input = convert_target_data_to_adaptation_rule_value(sentence_targets, number_of_inputs)
# print(new_verb_input)
# print(new_left_context_input)
# print(new_right_context_input)
# print(new_target_input)
#
# i = 0
# for new_verb_line, new_left_context_line, new_right_context_line, new_target_line in \
#                     zip(new_verb_input, new_left_context_input, new_right_context_input, new_target_input):
#     print(new_verb_line)
#     print(new_left_context_line)
#     print(new_right_context_line)
#     print(new_target_line)
#     print(i)
#     i += 1


# abc = "þ Wanneer niet-terugvorderbare BTW beschouwd word als een uitgave die niet voor " \
#       "subsidie in aanmerking komt , dan wordt het voor veel potentiële begunstigden aanmerkelijk moeilijker subsidie te krijgen ."
#
# zyx = "beschouwd beschouwd word wordt komt komt wordt wordt"
# print(convert_sentence_to_input_format(abc, zyx, 20, 24))
#



