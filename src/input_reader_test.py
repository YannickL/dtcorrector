import unittest
import input_reader

# class convert_input_file_to_idsTests(unittest.TestCase):
#
#     def

class convert_sentence_to_inputsTests(unittest.TestCase):

    def test_correct(self):
        sentence = "Hij antwoordt correct op de vraag."
        wanted_result_inputs, wanted_result_verbs = input_reader.convert_sentence_to_inputs(sentence, 20, 20)
        real_result_inputs = [[["a", "n", "t", "w", "o", "o", "r", "d", "t"], ["hij"], [".", "vraag", "de", "op", "correct"]]]
        real_result_verbs = ["antwoordt"]
        self.failUnlessEqual(wanted_result_inputs, real_result_inputs)
        self.failUnlessEqual(wanted_result_verbs, real_result_verbs)


class convert_sentence_to_inputs2Tests(unittest.TestCase):

    def test_correct(self):
        sentence = "Hij antwoordt correct op de vraag."
        targets = "antwoordt antwoordt"
        wanted_result_inputs, wanted_result_verbs, wanted_result_targets = input_reader.convert_sentence_to_input_format(sentence, targets, 20, 20)
        real_result_inputs = [[["a", "n", "t", "w", "o", "o", "r", "d", "t"], ["hij"], [".", "vraag", "de", "op", "correct"]]]
        real_result_verbs = ["antwoordt"]
        real_result_targets = [["antwoordt", "antwoordt"]]
        self.failUnlessEqual(wanted_result_inputs, real_result_inputs)
        self.failUnlessEqual(wanted_result_verbs, real_result_verbs)
        self.failUnlessEqual(wanted_result_targets, real_result_targets)

    def test_correct2(self):
        sentence = "Het wordt correct geantwoord."
        targets = "wordt wordt geantwoord geantwoord"
        wanted_result_inputs, wanted_result_verbs, wanted_result_targets = input_reader.convert_sentence_to_input_format(sentence, targets, 20, 20)
        real_result_inputs = [[["w", "o", "r", "d", "t"], ["het"], [".", "geantwoord", "correct"]],
            [["g", "e", "a", "n", "t", "w", "o", "o", "r", "d"], ["het", "wordt", "correct"], ["."]]]
        real_result_verbs = ["wordt", "geantwoord"]
        real_result_targets = [["wordt", "wordt"], ["geantwoord", "geantwoord"]]

        self.failUnlessEqual(wanted_result_inputs, real_result_inputs)
        self.failUnlessEqual(wanted_result_verbs, real_result_verbs)
        self.failUnlessEqual(wanted_result_targets, real_result_targets)

class create_raw_inputsTests(unittest.TestCase):

    def test_correct(self):
        sentence1 = "Hij antwoordt correct op de vraag."
        sentence2 = "Zij wordt ouder."
        sentences = [sentence1, sentence2]
        wanted_result_inputs, wanted_result_verbs = input_reader.create_raw_inputs(sentences, 20, 20)
        real_result_inputs = [[["a", "n", "t", "w", "o", "o", "r", "d", "t"], ["hij"], [".", "vraag", "de", "op", "correct"]], [["w", "o", "r", "d", "t"], ["zij"], [".", "ouder"]]]
        real_result_verbs = ["antwoordt", "wordt"]
        self.failUnlessEqual(wanted_result_inputs, real_result_inputs)
        self.failUnlessEqual(wanted_result_verbs, real_result_verbs)

    def test_correct2(self):
        sentence1 = "Hij antwoordt correct op de vraag."
        sentence2 = "Het wordt correct geantwoord."
        sentences = [sentence1, sentence2]
        wanted_result_inputs, wanted_result_verbs = input_reader.create_raw_inputs(sentences, 20, 20)
        real_result_inputs = [
            [["a", "n", "t", "w", "o", "o", "r", "d", "t"], ["hij"], [".", "vraag", "de", "op", "correct"]],
            [["w", "o", "r", "d", "t"], ["het"], [".", "geantwoord", "correct"]],
            [["g", "e", "a", "n", "t", "w", "o", "o", "r", "d"], ["het", "wordt", "correct"], ["."]]]
        real_result_verbs = ["antwoordt", "wordt", "geantwoord"]
        self.failUnlessEqual(wanted_result_inputs, real_result_inputs)
        self.failUnlessEqual(wanted_result_verbs, real_result_verbs)

class create_raw_inputs2Tests(unittest.TestCase):

    def test_correct(self):
        sentence1 = "Hij antwoordt correct op de vraag."
        sentence2 = "Zij wordt ouder."
        sentences = [sentence1, sentence2]
        targets1 = "antwoordt antwoordt"
        targets2 = "wordt wordt"
        targets = [targets1, targets2]
        wanted_result_inputs, wanted_result_verbs, wanted_result_targets = input_reader.reformat_input(sentences, targets, 20, 20)
        real_result_inputs = [[["a", "n", "t", "w", "o", "o", "r", "d", "t"], ["hij"], [".", "vraag", "de", "op", "correct"]], [["w", "o", "r", "d", "t"], ["zij"], [".", "ouder"]]]
        real_result_verbs = ["antwoordt", "wordt"]
        real_result_targets = [["antwoordt", "antwoordt"], ["wordt", "wordt"]]
        self.failUnlessEqual(wanted_result_inputs, real_result_inputs)
        self.failUnlessEqual(wanted_result_verbs, real_result_verbs)
        self.failUnlessEqual(wanted_result_targets, real_result_targets)

    def test_correct2(self):
        sentence1 = "Hij antwoordt correct op de vraag."
        sentence2 = "Het wordt correct geantwoord."
        sentences = [sentence1, sentence2]
        targets1 = "antwoordt antwoordt"
        targets2 = "wordt wordt geantwoord geantwoord"
        targets = [targets1, targets2]
        wanted_result_inputs, wanted_result_verbs, wanted_result_targets = input_reader.reformat_input(sentences, targets, 20, 20)
        real_result_inputs = [
            [["a", "n", "t", "w", "o", "o", "r", "d", "t"], ["hij"], [".", "vraag", "de", "op", "correct"]],
            [["w", "o", "r", "d", "t"], ["het"], [".", "geantwoord", "correct"]],
            [["g", "e", "a", "n", "t", "w", "o", "o", "r", "d"], ["het", "wordt", "correct"], ["."]]]
        real_result_verbs = ["antwoordt", "wordt", "geantwoord"]
        real_result_targets = [["antwoordt", "antwoordt"], ["wordt", "wordt"], ["geantwoord", "geantwoord"]]
        self.failUnlessEqual(wanted_result_inputs, real_result_inputs)
        self.failUnlessEqual(wanted_result_verbs, real_result_verbs)
        self.failUnlessEqual(wanted_result_targets, real_result_targets)

class shuffle_two_listsTests(unittest.TestCase):

    def test_correct(self):
        list1 = [1, 2]
        list2 = [3, 4]
        shuffled_list_1, shuffled_list_2 = input_reader.shuffle_two_lists(list1, list2)
        real_result_inputs = [[["a", "n", "t", "w", "o", "o", "r", "d", "t"], ["hij"], [".", "vraag", "de", "op", "correct"]], [["w", "o", "r", "d", "t"], ["zij"], [".", "ouder"]]]
        real_result_verbs = ["antwoordt", "wordt"]
        self.failIf(shuffled_list_1[0] == 1 and shuffled_list_2[0] == 4)
        self.failIf(shuffled_list_1[0] == 2 and shuffled_list_2[0] == 3)

def main():
    unittest.main()

if __name__ == '__main__':
    main()