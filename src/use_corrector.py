from __future__ import print_function
import tensorflow as tf
import datetime
import numpy as np
import argparse
import os
import pickle
import re
from dt_corrector import CorrectorModel
import logging
import Utils


# logging.basicConfig(filename='dt_Corrector_use.log', level=logging.INFO)


logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

fh = logging.FileHandler('dt_Corrector_use.log', mode='a')
fh.setLevel(logging.INFO)

logger.addHandler(fh)


def main():
    parser = argparse.ArgumentParser(
                       formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--data_dir', type=str, default='/home/yakke/Bureaublad/preprocessed_data/',
                        help='data directory containing input.txt')
    parser.add_argument('--model_dir', type=str, default='/home/yakke/Bureaublad/preprocessed_data/',
                        help='data directory containing important model parameters')
    # parser.add_argument('--save_dir', type=str, default='/home/yakke/Bureaublad/preprocessed_data/save',
    #                     help='model directory to store checkpointed models')

    args = parser.parse_args()
    check_text(args)


def check_text(args):
    logger.info(" ")
    logger.info("----------START CHECKING----------")
    logger.info(" ")
    start_preprocessing = datetime.datetime.now()
    data_loader = Utils.UsePreProcessor(args.data_dir, args.model_dir)
    stop_preprocessing = datetime.datetime.now()
    print("Time preprocessing = {}".format(stop_preprocessing - start_preprocessing))
    logger.info("Time preprocessing = {}".format(stop_preprocessing - start_preprocessing))

    # check if all necessary files exist
    assert os.path.isdir(args.data_dir), " %s must be a a path" % args.data_dir
    assert os.path.isfile(
        os.path.join(args.model_dir, "config.pkl")), "config.pkl file does not exist in path %s" % args.model_dir
    assert os.path.isfile(os.path.join(args.model_dir,
                                       "vocab_words.pkl")), "vocab_words.pkl file does not exist in path %s" \
                                                            % args.model_dir
    assert os.path.isfile(os.path.join(args.model_dir,
                                       "vocab_chars.pkl")), "vocab_chars.pkl file does not exist in path %s" \
                                                            % args.model_dir
    ckpt = tf.train.get_checkpoint_state(args.model_dir)
    assert ckpt, "No checkpoint found"
    assert ckpt.model_checkpoint_path, "No model path found in checkpoint"

    learned_embedding = data_loader.get_embedding_model_repr()
    start_model_checking_time = datetime.datetime.now()

    with open(os.path.join(args.model_dir, 'config.pkl'), 'rb') as f:
        saved_args = pickle.load(f)

    model_args = vars(saved_args)
    model = CorrectorModel(data_loader.word_vocab_size, data_loader.char_vocab_size, model_args['embedding_size'],
                           data_loader.batch_size, model_args['rnn_size'], model_args['num_layers'],
                           data_loader.max_context_sequence_length, data_loader.max_verb_sequence_length)

    corrected_file = os.path.join(args.data_dir, "corrected.txt")

    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        sess.run(tf.assign(model.embedding_words, learned_embedding))
        saver = tf.train.Saver(tf.global_variables())
        ckpt = tf.train.get_checkpoint_state(args.model_dir)
        if ckpt and ckpt.model_checkpoint_path:
            saver.restore(sess, ckpt.model_checkpoint_path)

        verb_sentence_generator = Utils.read_sentences_generator_numbers(data_loader.use_verb_tensor_file)
        left_context_sentence_generator = \
            Utils.read_sentences_generator_numbers(data_loader.use_left_context_tensor_file)
        right_context_sentence_generator = \
            Utils.read_sentences_generator_numbers(data_loader.use_right_context_tensor_file)
        verbs_generator = Utils.read_words_generator_text(data_loader.use_verb_file)
        line_number_sentence_generator = Utils.read_sentences_generator_number(data_loader.use_line_number_file)
        verb_position_sentence_generator = Utils.read_sentences_generator_number(data_loader.use_verb_position_file)
        start_checking = datetime.datetime.now()

        verbs_input_data, verbs_seq_lens, \
            left_context_data, left_context_seq_lengths, \
            right_context_data, right_context_seq_lengths, verbs, line_numbers, verb_positions = \
            data_loader.generate_batch(verb_sentence_generator, left_context_sentence_generator,
                                       right_context_sentence_generator, verbs_generator,
                                       line_number_sentence_generator, verb_position_sentence_generator)

        feed = {model.input_data_verb: verbs_input_data,
                model.seq_length_verb: verbs_seq_lens,
                model.input_data_left_context: left_context_data,
                model.seq_length_left_context: left_context_seq_lengths,
                model.input_data_right_context: right_context_data,
                model.seq_length_right_context: right_context_seq_lengths}

        line_numbers_list = list(line_numbers)
        predictions = sess.run(model.prediction, feed)
        predicted_rules = np.argmax(predictions, axis=1)
        with open(data_loader.use_original_file) as original_f:
            original_lines = original_f.readlines()

        for i in range(len(original_lines)):
            if i in line_numbers_list:
                x = line_numbers_list.index(i)
                predicted_rule = predicted_rules[x]
                corrected_verb = Utils.apply_predicted_rule(predicted_rule, verbs[x])
                # print(corrected_verb)
                original_line = original_lines[i]
                # print("AA: " + original_line)
                list_corrected_line = []
                verb_position = verb_positions[x]
                tags_original_sentence = Utils.tag_input_sentence(original_line)
                # print(tags_original_sentence)
                for j in range(len(tags_original_sentence)):
                    if verb_position == j:
                        list_corrected_line.append(corrected_verb)
                    else:
                        list_corrected_line.append(tags_original_sentence[j].word)
                corrected_line = ' '.join(list_corrected_line)
                print("****")
                print(i)
                print(predicted_rule)
                print("_____")
            else:
                corrected_line = original_lines[i]
            with open(corrected_file, 'a', encoding='utf-8') as cf:
                if not corrected_line.endswith('\n'):
                    corrected_line = corrected_line + '\n'
                cf.write(corrected_line)
                # print("___: " + corrected_line)

        stop_checking = datetime.datetime.now()

        print("Time testing = {}".format(stop_checking - start_checking))
        logger.info("Time testing = {}".format(stop_checking - start_checking))

    stop_model_checking_time = datetime.datetime.now()
    print("Time checking text = {}".format(stop_model_checking_time - start_model_checking_time))
    logger.info("Time checking text = {}".format(stop_model_checking_time - start_model_checking_time))

    logger.info(" ")
    logger.info("----------STOP CHECKING----------")
    logger.info(" ")

if __name__ == '__main__':
    main()