import random
import sys
import re

arg1 = sys.argv[1]
arg2 = sys.argv[2]

with open(arg1, 'r', encoding='utf-8') as input_file, open(arg2, 'w', encoding='utf-8') as output_text_file:
    t = 0
    t2 = 0
    teller1 = 0
    teller2 = 0
    teller3 = 0
    count = 0
    for line in input_file:
        use_line = True
        t += 1
        # Strip all chars from the end of the string (default whitespace characters)
        new_line = line.strip()
        # Only work with non empty strings
        if not (len(new_line) == 0):
            if new_line.startswith('(') and new_line.endswith(')'):
                new_line = new_line[1:-1]
            if not(new_line.endswith('.') or new_line.endswith('?') or new_line.endswith('!')):
                new_line += '.'
            if new_line.startswith('.') or new_line.startswith('!') or new_line.startswith('?'):
                new_line = new_line[1:]
            if re.search(r'[^A-Z]\.[A-Za-z]+', new_line):
                use_line = False
                teller1 += 1
            if re.search(r'\s+\.[A-Za-z]+', new_line):
                use_line = False
                teller2 += 1
            if re.search(r'\S+\.[A-Za-z]+', new_line):
                use_line = False
                teller3 += 1

            new_line = new_line.strip()
            if len(new_line) == 0:
                use_line = False
            # TODO
            # new_line = re.sub(r'([0-9]+),([0-9]+)', r'\1.\2', new_line)
    #        if re.match(r"([a-zA-Z])+\.([A-Z])+", new_line):
    #            print(str(i) + ": " + new_line)
            if use_line:
                t2 += 1
                correct_line = new_line
                output_text_file.write(correct_line + "\n")
            print(t)
            print(t2)
    print(teller1)
    print(teller2)
    print(teller3)


with open(arg2, 'r') as text_file:
    input_lines = text_file.read().splitlines()
    # Shuffle list
    random.shuffle(input_lines)
with open(arg2, 'w') as text_file:
    for line in input_lines:
        text_file.write(line + "\n")
