#Anything in this file, followed by a period (and an upper-case word), does NOT indicate an end-of-sentence marker.
#Special cases are included for prefixes that ONLY appear before 0-9 numbers.

#any single upper case letter  followed by a period is not a sentence ender
#usually upper case letters are initials in a name
A
B
C
D
E
F
G
H
I
J
K
L
M
N
O
P
Q
R
S
T
U
V
W
X
Y
Z

#List of titles. These are often followed by upper-case names, but do not indicate sentence breaks
Dhr
dhr
M
m
Mr
mr
Mevr
mevr
Mw
mw
Dr
dr
Ir
ir
Drs
drs
Prof
prof
Ing
ing
Adj
adj
Dir
dir
Ds
ds
Fam
fam
Lt
lt
Mej
mej
Mgr
mgr
Secr
secr
v
Vz
vz
Z.M
Zr
zr

#misc - odd period-ending items that NEVER indicate breaks (p.m. does NOT fall into this category - it sometimes ends a sentence)
c.a
i.e
bijv
blz
bv
art
a.g.v
d.m.v
d.w.z
cf
cfr
conf
e.g
i.g.v
i.o.v
i.p.v
i.t.t
i.v.m
m.a.w
m.b.t
m.b.v
m.i.v
m.n
m.u.v
n.a.v
o.a
o.b.v
o.i.v
o.g.v
o.l.v
pag
resp
t.a.v
t.b.v
t.g.v
t.h.t
t.h.v
t.o.v
t.w.v
U.S.A
V.S
zgn
vs

#Numbers only. These should only induce breaks when followed by a numeric sequence
# add NUMERIC_ONLY after the word for this function
#This case is mostly for the english "No." which can either be a sentence of its own, or
#if followed by a number, a non-breaking prefix
No #NUMERIC_ONLY# 
Nos
Art #NUMERIC_ONLY#
Nr
pp #NUMERIC_ONLY#
