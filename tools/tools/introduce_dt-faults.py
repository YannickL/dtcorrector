import subprocess
import sys
import treetaggerwrapper
import random
import re
import pprint
import os
import time

# Stap0
# cat europarl/nl/* > raw.txt
# Stap 1
# perl tokenizer.perl -l nl <raw.txt > tokenized.txt
# Stap 2
# perl split-sentences.perl -l nl < tokenized.txt > tokenized_split.txt
# Stap 3
# perl de-xml.perl tokenized_split txt txt tokenized_split_dexml
# Stap 4
# python3 clean_and_shuffle.py tokenized_split_dexml.txt clean.txt
# Stap 5
# python3 introduce_dt-faults.py clean.txt input_distribution_n.txt answers_distribution_n.txt

tagger = treetaggerwrapper.TreeTagger(TAGLANG='nl')


def tag_input_sentence(sentence):
    tags = tagger.tag_text(sentence)
    tags2 = treetaggerwrapper.make_tags(tags)
    return tags2


arg1 = sys.argv[1]
directory = sys.argv[2]
if not os.path.exists(directory):
    os.makedirs(directory)
arg2 = os.path.join(directory, "train_original.txt")
arg3 = os.path.join(directory, "train_answers.txt")
arg4 = os.path.join(directory, "validation_original.txt")
arg5 = os.path.join(directory, "validation_answers.txt")
arg6 = os.path.join(directory, "test_original.txt")
arg7 = os.path.join(directory, "test_answers.txt")

per_never_correct_form = 0
per_sometimes_correct_form = 0
per_always_correct_form = 0.3

vt_dde = 0
vt_dde_to_de = 0
vt_dde_no_change = 0
vt_de = 0
vt_de_to_dde = 0
vt_de_no_change = 0
vt_dden = 0
vt_dden_to_den = 0
vt_dden_no_change = 0
vt_den = 0
vt_den_to_dden = 0
vt_den_no_change = 0
vt_tte = 0
vt_tte_to_te = 0
vt_tte_no_change = 0
vt_te = 0
vt_te_to_tte = 0
vt_te_no_change = 0
vt_tten = 0
vt_tten_to_ten = 0
vt_tten_no_change = 0
vt_ten = 0
vt_ten_to_tten = 0
vt_ten_no_change = 0

tt_d = 0
tt_d_to_t = 0
tt_d_to_dt = 0
tt_d_no_change = 0
tt_dt = 0
tt_dt_to_t = 0
tt_dt_to_d = 0
tt_dt_no_change = 0
tt_t = 0
tt_t_to_dt = 0
tt_t_to_d = 0
tt_t_no_change = 0

vd_d = 0
vd_d_to_dt = 0
vd_d_to_t = 0
vd_d_no_change = 0
vd_t = 0
vd_t_to_dt = 0
vd_t_to_d = 0
vd_t_no_change = 0

with open(arg1, 'r', encoding='utf-8') as input_file, open(arg2, 'w', encoding='utf-8') as output_train_file, \
        open(arg3, 'w', encoding='utf-8') as output_train_answers_file, \
        open(arg4, 'w', encoding='utf-8') as output_validation_file, \
        open(arg5, 'w', encoding='utf-8') as output_validation_answers_file, \
        open(arg6, 'w', encoding='utf-8') as output_test_file, \
        open(arg7, 'w', encoding='utf-8') as output_test_answers_file:
    # t =
    count = 0
    separator = 0
    start = time.time()
    count_lines = 0
    count_possible_dt_faults = 0
    for line in input_file:
#         t += 1
#         # Strip all chars from the end of the string (default whitespace characters)
#         new_line = line.rstrip()
#         verb = ''
#         if new_line.startswith('(') and new_line.endswith(')'):
#             new_line = new_line[1:-1]
#         if not(new_line.endswith('.') or new_line.endswith('?') or new_line.endswith('!')):
#             new_line += '.'
#         # TODO
#         # new_line = re.sub(r'([0-9]+),([0-9]+)', r'\1.\2', new_line)
# #        if re.match(r"([a-zA-Z])+\.([A-Z])+", new_line):
# #            print(str(i) + ": " + new_line)
#         correct_line = new_line
#         print(t)

        count += 1
        if count % 1000 == 0:
            print(count)
        adapted_line = line
        tags_sentence = tag_input_sentence(line)
        bad_verbs = []
        correct_verbs = []
        no_use = False
        # pprint.pprint(tags_sentence)
        for i in range(len(tags_sentence)):
            if type(tags_sentence[i]).__name__ == "NotTag":
                no_use = True
                break
            else:
                if hasattr(tags_sentence[i], 'pos'):
                    pos_tag = tags_sentence[i].pos
                    # print(tags_sentence[i].word)
                    if pos_tag.startswith('2') and len(pos_tag) >= 3:
                        # Test present time and imperative
                        test_present_time = 4 <= int(pos_tag[1]) <= 8 and \
                                            (1 <= int(pos_tag[2]) <= 3 or 7 <= int(pos_tag[2]) <= 8)
                        # Test past time
                        test_past_time = 4 <= int(pos_tag[1]) <= 8 and 5 <= int(pos_tag[2]) <= 6
                        # Test past participle
                        test_past_participle = 0 <= int(pos_tag[1]) <= 3 and 6 <= int(pos_tag[2]) <= 9
                        verb = tags_sentence[i].word
                        bad_verb = ''
                        random_percentage = random.random()
                        possible_dt_fault = True
                        if test_past_time:
                            if verb.endswith('dde'):
                                vt_dde += 1
                                if random_percentage < per_never_correct_form:
                                    # Introduced fault is a never occuring form
                                    bad_verb = verb[:-3] + 'de'
                                    vt_dde_to_de += 1
                                else:
                                    # if per_never_correct_form <= random < 1
                                    bad_verb = verb
                                    vt_dde_no_change += 1
                            elif verb.endswith('de'):
                                vt_de += 1
                                if random_percentage < per_never_correct_form:
                                    # Introduced fault is a never occuring form
                                    bad_verb = verb[:-2] + 'dde'
                                    vt_de_to_dde += 1
                                else:
                                    # if per_never_correct_form <= random < 1
                                    bad_verb = verb
                                    vt_de_no_change += 1
                            elif verb.endswith('dden'):
                                vt_dden += 1
                                if random_percentage < per_sometimes_correct_form:
                                    # Introduced fault can be a correct or wrong form
                                    bad_verb = verb[:-4] + 'den'
                                    vt_dden_to_den += 1
                                else:
                                    # if per_sometimes_correct_form <= random < 1
                                    bad_verb = verb
                                    vt_dden_no_change += 1
                            elif verb.endswith('den'):
                                vt_den += 1
                                if random_percentage < per_never_correct_form:
                                    # Introduced fault is a never occuring form
                                    bad_verb = verb[:-3] + 'dden'
                                    vt_den_to_dden += 1
                                else:
                                    # if per_never_correct_form <= random < 1
                                    bad_verb = verb
                                    vt_den_no_change += 1
                            elif verb.endswith('tte'):
                                vt_tte += 1
                                if random_percentage < per_never_correct_form:
                                    # Introduced fault is a never occuring form
                                    bad_verb = verb[:-3] + 'te'
                                    vt_tte_to_te += 1
                                else:
                                    # if per_never_correct_form <= random < 1
                                    bad_verb = verb
                                    vt_tte_no_change += 1
                            elif verb.endswith('te'):
                                vt_te += 1
                                if random_percentage < per_never_correct_form:
                                    # Introduced fault is a never occuring form
                                    bad_verb = verb[:-2] + 'tte'
                                    vt_te_to_tte += 1
                                else:
                                    # if per_never_correct_form <= random < 1
                                    bad_verb = verb
                                    vt_te_no_change += 1
                            elif verb.endswith('tten'):
                                vt_tten += 1
                                if random_percentage < per_sometimes_correct_form:
                                    # Introduced fault can be a correct or wrong form
                                    bad_verb = verb[:-4] + 'ten'
                                    vt_tten_to_ten += 1
                                else:
                                    # if per_sometimes_correct_form <= random < 1
                                    bad_verb = verb
                                    vt_tten_no_change += 1
                            elif verb.endswith('ten'):
                                vt_ten += 1
                                if random_percentage < per_never_correct_form:
                                    # Introduced fault is a never occuring form
                                    bad_verb = verb[:-3] + 'tten'
                                    vt_ten_to_tten += 1
                                else:
                                    # if per_never_correct_form <= random < 1
                                    bad_verb = verb
                                    vt_ten_no_change += 1
                            else:
                                # If the verbs are strong verbs -> no d/t-faults
                                possible_dt_fault = False

                        elif test_present_time:
                            if verb.endswith('d'):
                                tt_d += 1
                                if random_percentage < per_never_correct_form:
                                    # Introduced fault is a never occuring form
                                    bad_verb = verb[:-1] + 't'
                                    tt_d_to_t += 1
                                elif per_never_correct_form <= random_percentage < per_never_correct_form + \
                                        per_always_correct_form:
                                    # Introduced fault is always a correct form
                                    bad_verb = verb[:-1] + 'dt'
                                    tt_d_to_dt += 1
                                else:
                                    # if per_never_correct_form + per_always_correct_form <= random < 1
                                    bad_verb = verb
                                    tt_d_no_change += 1
                            elif verb.endswith('dt'):
                                tt_dt += 1
                                if random_percentage < per_never_correct_form:
                                    # Introduced fault is a never occuring form
                                    bad_verb = verb[:-2] + 't'
                                    tt_dt_to_t += 1
                                elif per_never_correct_form <= random_percentage < per_never_correct_form + \
                                        per_always_correct_form:
                                    # Introduced fault is always a correct form
                                    bad_verb = verb[:-2] + 'd'
                                    tt_dt_to_d += 1
                                else:
                                    # if per_never_correct_form + per_always_correct_form <= random < 1
                                    bad_verb = verb
                                    tt_dt_no_change += 1
                            elif verb.endswith('t') and not verb.endswith('dt'):
                                tt_t += 1
                                if random_percentage < per_never_correct_form:
                                    # Introduced fault is a never occuring form
                                    bad_verb = verb[:-1] + 'dt'
                                    tt_t_to_dt += 1
                                elif per_never_correct_form <= random_percentage < per_never_correct_form + \
                                        per_sometimes_correct_form:
                                    # Introduced fault can be a correct or wrong form
                                    bad_verb = verb[:-1] + 'd'
                                    tt_t_to_d += 1
                                else:
                                    # if per_never_correct_form + per_sometimes_correct_form <= random < 1
                                    bad_verb = verb
                                    tt_t_no_change += 1
                            else:
                                # If the verbs are in imperative or first person and not ending in d or t or dt
                                possible_dt_fault = False

                        elif test_past_participle:
                            if verb.endswith('d'):
                                vd_d += 1
                                if random_percentage < per_never_correct_form:
                                    # Introduced fault is a never occuring form
                                    bad_verb = verb[:-1] + 'dt'
                                    vd_d_to_dt += 1
                                elif per_never_correct_form <= random_percentage < per_never_correct_form + \
                                        per_sometimes_correct_form:
                                    # Introduced fault can be a correct or wrong form
                                    bad_verb = verb[:-1] + 't'
                                    vd_d_to_t += 1
                                else:
                                    # if per_never_correct_form + per_sometimes_correct_form <= random < 1
                                    bad_verb = verb
                                    vd_d_no_change += 1
                            elif verb.endswith('t') and not verb.endswith('dt'):
                                vd_t += 1
                                if random_percentage < per_never_correct_form:
                                    # Introduced fault is a never occuring form
                                    bad_verb = verb[:-1] + 'dt'
                                    vd_t_to_dt += 1
                                elif per_never_correct_form <= random_percentage < 2 * per_never_correct_form:
                                    # Introduced fault is a never occuring form
                                    bad_verb = verb[:-1] + 'd'
                                    vd_t_to_d += 1
                                else:
                                    # if 2*per_never_correct_form <= random < 1
                                    bad_verb = verb
                                    vd_t_no_change += 1
                            else:
                                # If the past participle is not ending with a d or t
                                possible_dt_fault = False

                        else:
                            # If the verb is not in a time tested for
                            possible_dt_fault = False

                        if possible_dt_fault:
                            if not bad_verb == verb:
                                words = adapted_line.split()
                                old_words = words[:i]
                                new_words = words[i:]
                                first_part_adapted_line = ' '.join(old_words)
                                second_part_adapted_line = ' '.join(new_words)
                                second_part_adapted_line = re.sub(r"\b%s\b" % verb, bad_verb, second_part_adapted_line,
                                                                  count=1)
                                adapted_line = first_part_adapted_line + ' ' + second_part_adapted_line
                            bad_verbs.append(bad_verb)
                            correct_verbs.append(verb)
        if len(bad_verbs) >= 1 and not no_use:
            count_lines += 1

            x = ""
            for j in range(len(bad_verbs)):
                count_possible_dt_faults += 1
                x += bad_verbs[j] + " " + correct_verbs[j] + " "
                separator += 1
            x += "\n"
            if not adapted_line.endswith('\n'):
                adapted_line = adapted_line + "\n"
            if separator < 1992486:
                output_train_file.write(adapted_line)
                output_train_answers_file.write(x)
            elif 1992486 <= separator < 2241547:
                output_validation_file.write(adapted_line)
                output_validation_answers_file.write(x)
            else:
                output_test_file.write(adapted_line)
                output_test_answers_file.write(x)

    end = time.time()
    duration = end - start
    print("Time: " + str(duration))
    print("Number of lines: " + str(count_lines))
    print("Number of possible d/t-faults: " + str(count_possible_dt_faults))
    print("vt_dde :" + str(vt_dde))
    print("vt_dde_to_de :" + str(vt_dde_to_de))
    print("vt_dde_no_change :" + str(vt_dde_no_change))
    print("vt_de :" + str(vt_de))
    print("vt_de_to_dde :" + str(vt_de_to_dde))
    print("vt_de_no_change :" + str(vt_de_no_change))
    print("vt_dden :" + str(vt_dden))
    print("vt_dden_to_den :" + str(vt_dden_to_den))
    print("vt_dden_no_change :" + str(vt_dden_no_change))
    print("vt_den :" + str(vt_den))
    print("vt_den_to_dden :" + str(vt_den_to_dden))
    print("vt_den_no_change :" + str(vt_den_no_change))
    print("vt_tte :" + str(vt_tte))
    print("vt_tte_to_te :" + str(vt_tte_to_te))
    print("vt_tte_no_change :" + str(vt_tte_no_change))
    print("vt_te :" + str(vt_te))
    print("vt_te_to_tte :" + str(vt_te_to_tte))
    print("vt_te_no_change :" + str(vt_te_no_change))
    print("vt_tten :" + str(vt_tten))
    print("vt_tten_to_ten :" + str(vt_tten_to_ten))
    print("vt_tten_no_change :" + str(vt_tten_no_change))
    print("vt_ten :" + str(vt_ten))
    print("vt_ten_to_tten :" + str(vt_ten_to_tten))
    print("vt_ten_no_change :" + str(vt_ten_no_change))

    print("tt_d :" + str(tt_d))
    print("tt_d_to_t :" + str(tt_d_to_t))
    print("tt_d_to_dt :" + str(tt_d_to_dt))
    print("tt_d_no_change :" + str(tt_d_no_change))
    print("tt_dt :" + str(tt_dt))
    print("tt_dt_to_t :" + str(tt_dt_to_t))
    print("tt_dt_to_d :" + str(tt_dt_to_d))
    print("tt_dt_no_change :" + str(tt_dt_no_change))
    print("tt_t :" + str(tt_t))
    print("tt_t_to_dt :" + str(tt_t_to_dt))
    print("tt_t_to_d :" + str(tt_t_to_d))
    print("tt_t_no_change :" + str(tt_t_no_change))

    print("vd_d :" + str(vd_d))
    print("vd_d_to_dt :" + str(vd_d_to_dt))
    print("vd_d_to_t :" + str(vd_d_to_t))
    print("vd_d_no_change :" + str(vd_d_no_change))
    print("vd_t :" + str(vd_t))
    print("vd_t_to_dt :" + str(vd_t_to_dt))
    print("vd_t_to_d :" + str(vd_t_to_d))
    print("vd_t_no_change :" + str(vd_t_no_change))


# with open(arg1, 'r') as input_file, open(arg2, 'w') as output_text_file, open(arg3, 'w') as output_answer_file:
#     # t = 0
#     count = 0
#     for line in input_file:
# #         t += 1
# #         # Strip all chars from the end of the string (default whitespace characters)
# #         new_line = line.rstrip()
# #         verb = ''
# #         if new_line.startswith('(') and new_line.endswith(')'):
# #             new_line = new_line[1:-1]
# #         if not(new_line.endswith('.') or new_line.endswith('?') or new_line.endswith('!')):
# #             new_line += '.'
# #         # TODO
# #         # new_line = re.sub(r'([0-9]+),([0-9]+)', r'\1.\2', new_line)
# # #        if re.match(r"([a-zA-Z])+\.([A-Z])+", new_line):
# # #            print(str(i) + ": " + new_line)
# #         correct_line = new_line
# #         print(t)
#
#         # correct_line is nu line geworden
#         adapted_line = line
#         tags_sentence = tag_input_sentence(line)
#         bad_verbs = []
#         correct_verbs = []
#         # print(line)
#         pprint.pprint(tags_sentence)
#         for i in range(len(tags_sentence)):
#             if type(tags_sentence[i]).__name__ == "NotTag":
#                 count += 1
#                 break
#             else:
#                 if hasattr(tags_sentence[i], 'pos'):
#                     pos_tag = tags_sentence[i].pos
#                     # print(tags_sentence[i].word)
#                     if pos_tag.startswith('2') and len(pos_tag) >= 3:
#                         test_present_time = 4 <= int(pos_tag[1]) <= 8 and 1 <= int(pos_tag[2]) <= 3
#                         test_past_time = 0 <= int(pos_tag[1]) <= 3 and 6 <= int(pos_tag[2]) <= 9
#                         if test_present_time or test_past_time:
#                             verb = tags_sentence[i].word
#                             if verb.endswith('d') or verb.endswith('dt'):
#                             # TODO if verb.endswith('d') or verb.endswith('dt') or verb.endswith('t'):
#                                 bad_verb = ''
#                                 random_percentage = random.random()
#                                 # Verdeling: 70% correct, 27% homofone vorm (d of dt), 3% naar de t-vorm
#                                 if verb.endswith('d'):
#                                     if random_percentage <= 0.7:
#                                         bad_verb = verb
#                                     elif 0.7 < random_percentage <= 0.97:
#                                         bad_verb = verb + 't'
#                                     elif 0.97 < random_percentage <= 1:
#                                         bad_verb = verb[:-1] + 't'
#                                 elif verb.endswith('dt'):
#                                     if random_percentage <= 0.7:
#                                         bad_verb = verb
#                                     elif 0.7 < random_percentage <= 0.97:
#                                         bad_verb = verb[:-1]
#                                     elif 0.97 < random_percentage <= 1:
#                                         bad_verb = verb[:-2] + 't'
#                                 adapted_line = re.sub(r"\b%s\b" % verb, bad_verb, adapted_line)
#                                 bad_verbs.append(bad_verb)
#                                 correct_verbs.append(verb)
#         if len(bad_verbs) >= 1:
#             output_text_file.write(adapted_line)
#             x = ""
#             for j in range(len(bad_verbs)):
#                 x += bad_verbs[j] + " " + correct_verbs[j] + " "
#             x += "\n"
#             output_answer_file.write(x)
#     print(count)
