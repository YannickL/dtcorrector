import treetaggerwrapper
import sys

tagger = treetaggerwrapper.TreeTagger(TAGLANG='nl')


def tag_input_sentence(sentence):
    tags = tagger.tag_text(sentence)
    tags2 = treetaggerwrapper.make_tags(tags)
    return tags2

arg1 = sys.argv[1]

vt_dde = 0
vt_de = 0
vt_dden = 0
vt_den = 0
vt_tte = 0
vt_te = 0
vt_tten = 0
vt_ten = 0

tt_d = 0
tt_dt = 0
tt_t = 0

vd_d = 0
vd_t = 0
count = 0
count_lines = 0
count_possible_dt_faults = 0
separator = 0

with open(arg1, 'r', encoding='utf-8') as input_file:
    for line in input_file:
        tags_sentence = tag_input_sentence(line)

        bad_verbs = []
        correct_verbs = []
        no_use = False
        count += 1
        if count % 1000 == 0:
            print(count)
        for i in range(len(tags_sentence)):
            if type(tags_sentence[i]).__name__ == "NotTag":
                no_use = True
                break
            else:
                if hasattr(tags_sentence[i], 'pos'):
                    pos_tag = tags_sentence[i].pos
                    # print(tags_sentence[i].word)
                    if pos_tag.startswith('2') and len(pos_tag) >= 3:
                        # Test present time and imperative
                        test_present_time = 4 <= int(pos_tag[1]) <= 8 and \
                                            (1 <= int(pos_tag[2]) <= 3 or 7 <= int(pos_tag[2]) <= 8)
                        # Test past time
                        test_past_time = 4 <= int(pos_tag[1]) <= 8 and 5 <= int(pos_tag[2]) <= 6
                        # Test past participle
                        test_past_participle = 0 <= int(pos_tag[1]) <= 3 and 6 <= int(pos_tag[2]) <= 9
                        verb = tags_sentence[i].word
                        bad_verb = ''
                        possible_dt_fault = True
                        if test_past_time:
                            if verb.endswith('dde'):
                                vt_dde += 1
                            elif verb.endswith('de'):
                                vt_de += 1
                            elif verb.endswith('dden'):
                                vt_dden += 1
                            elif verb.endswith('den'):
                                vt_den += 1
                            elif verb.endswith('tte'):
                                vt_tte += 1
                            elif verb.endswith('te'):
                                vt_te += 1
                            elif verb.endswith('tten'):
                                vt_tten += 1
                            elif verb.endswith('ten'):
                                vt_ten += 1
                            else:
                                # If the verbs are strong verbs -> no d/t-faults
                                possible_dt_fault = False

                        elif test_present_time:
                            if verb.endswith('d'):
                                tt_d += 1
                            elif verb.endswith('dt'):
                                tt_dt += 1
                            elif verb.endswith('t') and not verb.endswith('dt'):
                                tt_t += 1
                            else:
                                # If the verbs are in imperative or first person and not ending in d or t or dt
                                possible_dt_fault = False

                        elif test_past_participle:
                            if verb.endswith('d'):
                                vd_d += 1
                            elif verb.endswith('t') and not verb.endswith('dt'):
                                vd_t += 1
                            else:
                                # If the past participle is not ending with a d or t
                                possible_dt_fault = False

                        else:
                            # If the verb is not in a time tested for
                            possible_dt_fault = False

                        if possible_dt_fault:
                            correct_verbs.append(verb)
        if len(correct_verbs) >= 1 and not no_use:
            count_lines += 1

            x = ""
            for j in range(len(correct_verbs)):
                separator += 1
                count_possible_dt_faults += 1
            x += "\n"
            if separator == 1992486:
                print("Number of lines: " + str(count_lines))
                print("Number of possible d/t-faults: " + str(count_possible_dt_faults))
                print("vt_dde :" + str(vt_dde))
                print("vt_de :" + str(vt_de))
                print("vt_dden :" + str(vt_dden))
                print("vt_den :" + str(vt_den))
                print("vt_tte :" + str(vt_tte))
                print("vt_te :" + str(vt_te))
                print("vt_tten :" + str(vt_tten))
                print("vt_ten :" + str(vt_ten))

                print("tt_d :" + str(tt_d))
                print("tt_dt :" + str(tt_dt))
                print("tt_t :" + str(tt_t))

                print("vd_d :" + str(vd_d))
                print("vd_t :" + str(vd_t))
                vt_dde = 0
                vt_de = 0
                vt_dden = 0
                vt_den = 0
                vt_tte = 0
                vt_te = 0
                vt_tten = 0
                vt_ten = 0

                tt_d = 0
                tt_dt = 0
                tt_t = 0

                vd_d = 0
                vd_t = 0

                count_lines = 0

            elif separator == 2241547:
                print("Number of lines: " + str(count_lines))
                print("Number of possible d/t-faults: " + str(count_possible_dt_faults))                
                print("vt_dde :" + str(vt_dde))
                print("vt_de :" + str(vt_de))
                print("vt_dden :" + str(vt_dden))
                print("vt_den :" + str(vt_den))
                print("vt_tte :" + str(vt_tte))
                print("vt_te :" + str(vt_te))
                print("vt_tten :" + str(vt_tten))
                print("vt_ten :" + str(vt_ten))

                print("tt_d :" + str(tt_d))
                print("tt_dt :" + str(tt_dt))
                print("tt_t :" + str(tt_t))

                print("vd_d :" + str(vd_d))
                print("vd_t :" + str(vd_t))
                vt_dde = 0
                vt_de = 0
                vt_dden = 0
                vt_den = 0
                vt_tte = 0
                vt_te = 0
                vt_tten = 0
                vt_ten = 0

                tt_d = 0
                tt_dt = 0
                tt_t = 0

                vd_d = 0
                vd_t = 0

                count_lines = 0


print("Number of lines: " + str(count_lines))
print("Number of possible d/t-faults: " + str(count_possible_dt_faults))
print("vt_dde :" + str(vt_dde))
print("vt_de :" + str(vt_de))
print("vt_dden :" + str(vt_dden))
print("vt_den :" + str(vt_den))
print("vt_tte :" + str(vt_tte))
print("vt_te :" + str(vt_te))
print("vt_tten :" + str(vt_tten))
print("vt_ten :" + str(vt_ten))

print("tt_d :" + str(tt_d))
print("tt_dt :" + str(tt_dt))
print("tt_t :" + str(tt_t))

print("vd_d :" + str(vd_d))
print("vd_t :" + str(vd_t))
