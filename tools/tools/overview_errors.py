import sys

arg1 = sys.argv[1]

count = 0
number_of_real_errors = 0
number_of_not_errors = 0
with open(arg1, 'r', encoding='utf-8') as input_file:
	for line in input_file:
		#print(line)
		count += 1
		if count % 1000 == 0:
			print(count)
		lst = line.split(' ')
		#print(lst)
		for i in range(0, len(lst)-1, 2):
			bad_verb = lst[i]
			correct_verb = lst[i+1]
			#print(bad_verb)
			#print(correct_verb)
			if bad_verb == correct_verb:
				number_of_not_errors += 1
			else:
				number_of_real_errors += 1
print("Errors: " + str(number_of_real_errors))
print("Not Errors: " + str(number_of_not_errors))
